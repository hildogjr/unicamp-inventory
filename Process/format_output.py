#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas classified information.
#
# Format definitions for the report output.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import os
import re
from datetime import datetime, date
import platform
import getpass

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
import numpy as np

# Internal libraries.
from general_definitions import (configs, path_temporary, _,
                                 SPREADSHEET_PAGE_INFO_NAME, GENERAL_SEP, print)


# %%
# Definitions of shortcut methods.

def temporary_path(f):
    return os.path.join(path_temporary, f)


# General definitions for this part of the software.
OUTPUT_FLOAT_FORMAT = configs.get('report').get('output float format')
if not OUTPUT_FLOAT_FORMAT:
    OUTPUT_FLOAT_FORMAT = '{:,.2f}'
OUTPUT_INT_FORMAT = configs.get('report').get('output int format')
if not OUTPUT_FLOAT_FORMAT:
    OUTPUT_FLOAT_FORMAT = '{:,}'
CURRENCY_UNITS = ['R$', 'BRL', 'USD', 'US$', '$']


# General definitions.
# Subnames (translations) used for the sub-statistics.
# Those values given the order to sort and save the statistics and
# consequently the order into the report file pages.
STATISTIC_SUBNAMES = {
    'total': _('Quant. items'),  # Total of items.
    'qty': _('Quant. sistemas'),  # Total of equipment, in case of some are composed by multiple items.
    'missing': _('Dados faltando'),  # Missing information for some
    'min': _('Menor valor'),  # Minimum value
    'max': _('Maior valor'),  # Maximum value.
    'mode': _('Mais frequente'),  # Statistic mode.
    'median': _('Mediana'),  # Median value.
    'avg': _('Média'),  # Average value of the statistic.
    'std': _('Desvio'),  # Standard deviation of the statistic.
    'sum': _('Total'),  # Sum of all valid elements in the specific statistic.
    # Projected total using the most common value for the missing data equipments.
    'projected mode': _('Proj. frequente'),
    'projected': _('Proj. média'),  # Projected total in case of missing info for some equipment.
    'score': _('Pontuação'),  # Score used for classification statistics (dictionary).
}


# %%
# General functions for output definitions.

RE_PYTHON_FUNC_VAR_STR = r'[a-z\_]+[\w\_]'
RE_PYTHON_EXPRESSION = re.compile(r'((?P<func_call>(?P<func>{t})*\((?P<args>.*)\))'
                                  r'|(?P<list_call>(?P<list>{t})\[(?P<element>.*)\]))'.format(
                                      t=RE_PYTHON_FUNC_VAR_STR), re.IGNORECASE)


def evaluate_variables(globals_handle: dict,
                       old_text: str,
                       convert_to_str: bool = False) -> str:
    '''Evaluate variable by string between double braces, e.g.: `'{{foo}}'`.

    Parameters
        globals_handle: global dictionary of handles.
        old_text: old text to evaluate.
        convert_to_str: (default False) force the conversion of value of
                        the variable  to string.

    Return: new text.

    Usage (keep `dict(globals())` as parameter):
    >>> new_text = evaluate_variables(dict(globals()), old_text)
    '''
    vars_to_eval = re.findall(r'\{\{(.+?)\}\}', old_text)
    for var_to_eval in vars_to_eval:
        try:
            # Check if needed a interpretation of especial function.
            python_expression = RE_PYTHON_EXPRESSION.match(var_to_eval)
            if python_expression:
                if python_expression.group('func_call'):
                    # Convert the arguments, if needed.
                    args = []
                    # TODO Not now the arguments does not allow the use of comma, even if is is a
                    # string, to fix is necessary to allow the de re-search for `{{}}`` and the not
                    # split when `,` is in a string limited by `[\'\"]`. This generate the balanced
                    # search problem that have no code to fix in this software.
                    for arg in python_expression.group('args').split(','):
                        arg = arg.strip()
                        if arg[0] in ('\'', '"') and arg[0] in ('\'', '"'):
                            # It a force string, not to be interpreted, just remove the quotation mark.
                            args.append(arg[1:-1])
                        # elif arg[:1] == '{{' and arg[-2:] == '}}':
                        #    args.append(evaluate_variables(globals_handle, arg))
                        else:
                            # The string may correspond to a variable.
                            args.append(globals_handle.get(arg, arg))
                    args = tuple(args)
                    # Check the known functions.
                    if python_expression.group('func') == 'report_link':
                        var_value = report_link(*args)
                        old_text = old_text.replace('{{%s}}' % var_to_eval, var_value)
                        continue
                elif python_expression.group('list_call'):
                    list_element = re.findall('[\'\"](.+)[\"\']', python_expression.group('element'))[0]
                    var_value = globals_handle[python_expression.group('list')][list_element]
                    old_text = old_text.replace('{{%s}}' % var_to_eval, var_value)
                    continue
            # If not match with any `global()` or `local()` variable list passed,
            # just use the current text. This will mark the report text as not converted
            # and make possible to track the error.
            var_value = globals_handle[var_to_eval]
            if convert_to_str:
                # Check against variable types that need conversion to be presented.
                if isinstance(var_value, (pd.Timestamp, np.datetime64, datetime)):
                    var_value = var_value.strftime(r'%d/%m/%Y')
                elif isinstance(var_value, list) and \
                        all([isinstance(v, str) for v in var_value]):
                    var_value = join_list_str(var_value)
                else:
                    var_value = str(var_value)
                old_text = old_text.replace('{{%s}}' % var_to_eval, var_value)
            else:
                old_text = var_value
        except KeyError:
            pass
    return old_text


HTML_SECTION_ID_NAME_CHAPTER_START = 'chapter-id'
HTML_SECTION_ID_NAME_SECTION_START = 'section-id'
HTML_SECTION_ID_NAME_SEP = '_'


def html_section_id_name(*strings: tuple[str]) -> str:
    '''Compose the HTML section name by a list of arguments.
    This function is may be also executed by the `evaluate_variables` when
    pointed by some `configuration.yml` comment string.
    '''
    strings = [s for s in strings if s]
    if not strings:
        return ''
    # Check if it will be a chapter or section in the report, just for
    # better HTML strings.
    if len(strings) == 1:
        string_list = [HTML_SECTION_ID_NAME_CHAPTER_START] + list(strings)
    else:
        string_list = [HTML_SECTION_ID_NAME_SECTION_START] + list(strings)
    return HTML_SECTION_ID_NAME_SEP.join(string_list).replace(' ', '').lower()


def report_link(*strings: tuple[str], text: str | None = None) -> str:
    '''Create the hyperlink for the report.
    If not provide the text string, it assumed the first on the list of strings.
    '''
    id_link = html_section_id_name(*strings)
    if len(strings) == 1 and not id_link:
        return strings[0]
    elif not id_link:
        return ''
    text = text or strings[0]
    return '<a href="#%s">%s</a>' % (id_link, text)


def compose_chart_file_name(list_parameters: list[str] | str,
                            folder_path: str | None = None,
                            check_exist: bool = False) -> str:
    '''Check if a string/list of string compose a valid file name for
    for the OS. Replace the invalid characters by "_" and join a list
    by use of the `GENERAL_SEP` definition.
    The default extension added is ".png".

    Parameters:
        List of string for a file name.
        Folder path to the file, important to check procedure.
        Check if the file exist (default False).

    Return a valid file name or None.
    '''
    name_part = []
    if isinstance(list_parameters, list):
        for p in list_parameters:
            name_part.append(re.sub(r'[^\w\-_\.]', '_', p))
    elif isinstance(list_parameters, str):
        name_part = re.sub(r'[^\w\-_\.]', '_', list_parameters)
    else:
        return None
    file_name = GENERAL_SEP.join(name_part)
    # Add the extension if it is not present.
    if not os.path.splitext(file_name)[1]:
        file_extension = configs['charts'].get('file extension', '.png')
        if file_extension[0] != '.':
            file_extension = '.' + file_extension
        file_name += file_extension
    if not check_exist:
        return file_name
    else:
        if folder_path:
            file_name = os.path.join(folder_path, file_name)
        if os.path.isfile(file_name):
            return file_name
    return None


def join_list_str(list_str: list[str],
                  msg_none: str = '',
                  msg_singular: str = '',
                  msg_plural: str = '',
                  and_particule: str = ' e ',
                  comma_particule: str = ', ',
                  pre_mark: str = '',
                  post_mark: str = '',
                  remove_none: bool = True) -> str:
    '''Join a list of string using comma ', ' and ' e ' particle.
    Allow to use singular/plural previous/post message, use `%s`
    to identify the point of replace into the previous/post message,
    if not use the message will be used before the joined list.

    Parameters:
        List of strings to join.
        Message to be used the strings joined in none case, singular or plural.
        Message for singular, use `%s` for insertion point.
        Message for plural, use `%s` for insertion point.
        And separator.
        Comma separators.
        Pre and post mark add to each element.
        Ignore or not the not `None` elements.


    Return the text to be used in a paragraph.
    '''
    if isinstance(list_str, str):
        list_str = [list_str]
    # elif isinstance(list_str, callable):
    #    list_str = list(list_str)
    if not msg_none:
        msg_none = msg_singular
    # Add the pre and post terminator to each strings and remove `None`
    list_print = []
    for element in list_str:
        if element is None:
            if remove_none:
                continue
            else:
                element = str(element)
        list_print.append(pre_mark + element + post_mark)
    # Check the kind of message to give.
    if len(list_print) == 0:
        # Message for empty.
        if msg_none:
            message = msg_none
        else:
            message = ''
    elif len(list_print) == 1:
        # Unique element message.
        message = list_print[0]
        try:
            message = msg_singular % message
        except TypeError:
            message = msg_singular + message
    else:
        # Join the list of elements.
        message = comma_particule.join(list_print[:-1]) + and_particule + list_print[-1]
        try:
            message = msg_plural % message
        except TypeError:
            message = msg_plural + message
    return message


def get_link_from_string(string: str) -> list[str]:
    '''Extract all the URLs from a texts.
    It may be used as parameter to replace for functional
    links on the report/page construction.
    '''
    RE_URL = (r'(?P<link>(http|ftp|https)://'
              r'([\w_-]+(?:(?:\.[\w_-]+)+))'
              r'([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?)')
    match = re.search(RE_URL, string)
    if not match:
        return None
    return match.group('link')


# %%
# Spreadsheet specific output definitions by functions.

def spreadsheet_adjust_columns(spreadsheet_writer: pd.ExcelWriter,
                               table: pd.Series, *,
                               sheet_names: list | None = None,
                               columns: list | dict | None = None) -> None:
    '''Adjust the columns width of the spreadsheet to fit the content.

    Parameters:
        spreadsheet_writer (pd.ExcelWriter): The writer object.
        table (pd.Series): Data table, used to catch the column title.
        sheets (list): Sheets to check the columns.
        columns (list or dict[list]): Columns to adjust (adjust all if not
                                      specified). Can be use a dictionary
                                      to specify columns of each sheet.

    Return the new `spreadsheet_writer`.

    Usage:
    >>> with pd.ExcelWriter(file_name_analysis, engine='xlsxwriter') as spreadsheet_writer:
    >>>     ...  # User code.
    >>>     spreadsheet_adjust_columns(spreadsheet_writer, data_frame)
    '''
    # Iterate writing each sheet of the spreadsheet with the found.
    sheet_names = sheet_names or list(spreadsheet_writer.sheets.keys())
    if not isinstance(sheet_names, list):
        sheet_names = [sheet_names]

    def as_text(cell_value) -> str:
        return str(cell_value) if cell_value else ''

    for sheet_name in sheet_names:
        worksheet = spreadsheet_writer.sheets[sheet_name]

        # Iterate through each column and set the width equal the max
        # length in that column. A padding length of 2 is also added.
        if not columns:
            columns = list(table.columns)
        elif isinstance(columns, list):
            columns = columns
        elif isinstance(columns, dict):
            columns = columns[sheet_name]
        else:
            columns = [columns]

        for idx, col_name in enumerate(table.columns):
            col_title = str(col_name[0] if isinstance(col_name, tuple) else col_name)
            column_len = max(table[col_name].astype(str).str.len().max(), len(col_title) + 2)
            worksheet.set_column(idx, idx, column_len)


def spreadsheet_format_columns(spreadsheet_writer: pd.ExcelWriter,
                               table: pd.Series, *,
                               columns: list | dict | None = None,
                               sheet_names: list | None = None,
                               format_header: bool | None = False) -> None:
    '''Format the columns type of the spreadsheet for better content visualization.

    Parameters:
        spreadsheet_writer (pd.ExcelWriter): The writer object.
        table (pd.Series): Data table, used to catch the column title.
        sheet_names (list): Sheets to check the columns.
        columns (list or dict[list]): Columns to format (format all if not
                                      specified). Can be use a dictionary
                                      to specify columns of each sheet.
        format_header (bool): Default False to format the header row applying
                              bold and some highlight. The `pandas` save
                              method already apply bold to the header.

    Return the new `spreadsheet_writer`.

    Usage:
    >>> with pd.ExcelWriter(file_name_analysis, engine='xlsxwriter') as spreadsheet_writer:
    >>>     ...  # User code.
    >>>     spreadsheet_format_columns(spreadsheet_writer)
    '''
    return  # TODO
    # Iterate writing each sheet of the spreadsheet with the found.
    if columns is None:
        columns = table.keys()
    sheet_names = sheet_names or list(spreadsheet_writer.sheet_names)
    if not isinstance(sheet_names, list):
        sheet_names = [sheet_names]
    for sheet in sheet_names:
        # Workaround to autofit the width of the column:
        # https://stackoverflow.com/questions/17326973/is-there-a-way-to-auto-adjust-excel-column-widths-with-pandas-excelwriter/32679806#32679806

        # Indicate workbook and worksheet for formatting.
        # workbook = spreadsheet_writer.book
        try:
            workbook = spreadsheet_writer.workbook.sheets[sheet]
        except Exception:
            # Try a sheet name that does not exist.
            continue

        # Iterate through each column and set the width equal the max
        # length in that column. A padding length of 2 is also added.
        if columns:
            pass
            # TODO
            # Reference: https://xlsxwriter.readthedocs.io/example_pandas_column_formats.html
            column_name = 'lklklk'

            def infer_name(name: str, group_list: list[str]) -> bool:
                '''Return True if is found any element of the list in
                part of the name.
                '''
                return any([n in name.lower() for n in group_list])

            is_column_to_format = True
            if infer_name(column_name, ['data', ]):
                num_format = 'DD-MM-YYYY'  # Date
            elif infer_name(column_name, ['ano', ]):
                num_format = 'YYYY'  # Date
            elif infer_name(column_name, ['%', ]):
                num_format = '0%'  # Percentage.
            elif infer_name(column_name, ['valor', 'saldo', ]):
                num_format = '#,##0.00'  # Currency.
            else:
                # Keep the current default format.
                is_column_to_format = False
            # If it is a "data missing" information about the statistic,
            # keep the default numeric format for just counting.
            if any([n in column_name.lower() for n in ['dados faltanto', ]]):
                is_column_to_format = False

            # Format the column with the correspondent format inferred the
            # title name in the header.
            if is_column_to_format:
                column_letter = 'A'
                column_format = workbook.add_format({'num_format': num_format})
                workbook.set_column('%s:%s' % (column_letter, column_letter),
                                    None, column_format)

        # Format the header
        if format_header:
            pass
            # TODO
    # spreadsheet_writer.save()


def get_default_unit(statistic_name: str) -> str | None:
    '''Get the default statistic unit expected by it name.
    It is used as default unit for report visualization.
    '''
    # Force some units of the statistics in case that is not
    # present by default.
    if any([n in statistic_name.lower() for n in ['saldo', 'valor', ]]):
        return 'R$'
    return None


def spreadsheet_write_info(spreadsheet_writer: pd.ExcelWriter,
                           page_name: str = SPREADSHEET_PAGE_INFO_NAME,
                           date_db: date | datetime | None = None) -> None:
    '''Write a information page for control into the spreadsheet.
    Parameters:
        spreadsheet_writer (pd.ExcelWriter): The writer object.
        page_name (optional str): Page name for the information, default
                                  defined by `SPREADSHEET_PAGE_INFO_NAME`.
        date_db (optional datetime.datetime): Datetime of database.
    Return the new `spreadsheet_writer`.'''
    date_now = datetime.now()
    try:
        memory_size = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')

        def format_memory_size(bytes_size, suffix="B") -> str:
            '''Scale bytes to its proper format.
            e.g:
                1253656 => '1.20MB'
                1253656678 => '1.17GB'
            '''
            FACTOR = 1024
            for unit in ["", "K", "M", "G", "T", "P"]:
                if bytes_size < FACTOR:
                    return f'{bytes_size:.2f}{unit}{suffix}'
                bytes_size /= FACTOR

        memory_size = format_memory_size(memory_size)
    except ValueError:
        memory_size = 'Valor não encontrado'

    wks = spreadsheet_writer.book.add_worksheet(page_name)
    row_idx = 0

    wks.write(row_idx, 0, 'Sistema de identificação de patrimônio da UNICAMP')
    row_idx += 1
    wks.write(row_idx, 0, 'Autor')
    wks.write(row_idx, 1, __author__)
    row_idx += 1
    wks.write(row_idx, 0, 'Website')
    wks.write(row_idx, 1, 'https://hildogjr.gitlab.io/unicamp-inventory/')
    row_idx += 2

    wks.write(row_idx, 0, 'Agradecimento obrigatório em publicações:')
    row_idx += 1
    wks.write(row_idx, 1, 'Esse trabalho usa os dados de patrimônios da UNICAMP processados por Dr. Hildo Guillardi Júnior.')
    row_idx += 1
    wks.write(row_idx, 1, 'This paper contains data of patrimony from UNICAMP processed by Dr. Hildo Guillardi Júnior.')
    row_idx += 2

    # Add the list of published articles using this tool.
    # The list is line-by-line into "articles.txt" file.
    wks.write(row_idx, 0, 'Referências')
    with open('articles.txt') as f_articles:
        articles = f_articles.read().splitlines()
        for article in articles:
            if article:
                row_idx += 1
                url = get_link_from_string(article)
                if url:
                    wks.write_url(row_idx, 1, url, string=article)
                else:
                    wks.write(row_idx, 1, article)
    row_idx += 2

    if date_db:
        wks.write(row_idx, 0, 'Data do DB')
        wks.write(row_idx, 2, str(date_db))

    wks.write(row_idx, 0, 'Execução')
    row_idx += 1
    wks.write(row_idx, 1, 'Data')
    wks.write(row_idx, 2, str(date_now))
    row_idx += 1
    wks.write(row_idx, 1, 'Computador')
    wks.write(row_idx, 2, platform.node())
    row_idx += 1
    wks.write(row_idx, 1, 'Usuário')
    wks.write(row_idx, 2, getpass.getuser())
    row_idx += 1
    wks.write(row_idx, 1, 'CPU')
    wks.write(row_idx, 2, platform.processor())
    row_idx += 1
    wks.write(row_idx, 1, 'Memória')
    wks.write(row_idx, 2, memory_size)
    row_idx += 1
    wks.write(row_idx, 1, 'OS')
    wks.write(row_idx, 2, platform.platform())
    row_idx += 1
    wks.write(row_idx, 1, 'Python')
    wks.write(row_idx, 2, ' '.join([platform.python_implementation(),
                                    platform.python_version()]))

    # Set the page info to active to be that one visible hen open the file.
    # It was workaround by creating this sheet/page before any other data pages.
    # spreadsheet_writer.sheets[page_name].active()

    return spreadsheet_writer


# %%
# HTML (report) specific output definitions by functions.

def number_to_html(value: int | float,
                   unit: str | None = None,
                   statistic_name: str | None = None,
                   extra_data: dict | None = None) -> str:
    '''Format the number for a better visualization of
    the reported file.

    Parameters:
        value (int, float, ...): Numeric value or other.
        statistic_name (str): Name of the statistic, used
                              for formatting checking.
        unit (str): Value unit.
        extra_data (dict): Extra statistics used for internal
                           calculation here such percentage.

    Return the HTML converted string.
    '''
    SI_PREFIX = ['', 'k', 'M', 'G']  # , 'T', 'P', 'E', 'Z', 'Y', 'R', 'Q']
    DIRECT_CONVERSION = [
        STATISTIC_SUBNAMES['total'],
        STATISTIC_SUBNAMES['qty'],
        STATISTIC_SUBNAMES['missing'],
        # STATISTIC_SUBNAMES['mode']
    ]
    # If present unit may be used a prefix.
    if unit and statistic_name not in DIRECT_CONVERSION and \
            (isinstance(value, int) or isinstance(value, float)):
        # Do not use SI prefix/suffix if it is a currency unity.
        if unit not in CURRENCY_UNITS:
            si_prefix_idx = 0
            while value >= 1e3 and (si_prefix_idx + 1) < len(SI_PREFIX):
                value /= 1e3
                si_prefix_idx += 1
            unit = SI_PREFIX[si_prefix_idx] + unit
    # Format the number.
    if statistic_name in DIRECT_CONVERSION:
        # Force the value be integer due some conversion caused
        # on save procedure between `performe_analysis` and
        # `generate_report` execution.
        value = int(value)
    if isinstance(value, int):
        string = OUTPUT_INT_FORMAT.format(value)
        # Replace the dot-comma as decimal-thousand indicator.
        string = string.replace(",", ".")
    elif isinstance(value, float):
        string = OUTPUT_FLOAT_FORMAT.format(value)
        # Replace the dot-comma as decimal-thousand indicator.
        string = string.replace(".", "#").replace(",", ".").replace("#", ",")
    else:
        string = str(value)

    # Add the unit.
    if unit and statistic_name and \
            statistic_name not in DIRECT_CONVERSION:
        if unit in CURRENCY_UNITS:
            string = '<em>' + unit + '</em> ' + string
        else:
            string += ' <em>' + unit + '</em>'
    # Add the percentage value of the statistic.
    if (extra_data and statistic_name == STATISTIC_SUBNAMES['missing']
            and (STATISTIC_SUBNAMES['total'] in extra_data.keys()
                 or STATISTIC_SUBNAMES['total'] in extra_data['Tipo'].keys())):
        if value > 0:
            total = extra_data.get(STATISTIC_SUBNAMES['total'])
            if not isinstance(total, (int, float)):
                total = extra_data['Tipo'].get(STATISTIC_SUBNAMES['total'])
            percentage = value / total
            string_percentage = ' (%.2f%%)' % (percentage * 100)
            string_percentage = string_percentage.replace('.', ',')
            string += string_percentage
    return string


def image_to_html(image_path: str,
                  image_size: str | float | None = None) -> str:
    '''Add a image do HTML from the temporary folder
    configuring it for better visualization of the
    reported file. The default extension is PNG.

    Parameters:
        image_path (str): Image path.
        image_size (str): Image size on the HTML.

    Return the HTML converted string.
    '''
    # Check the standard folder.
    if not os.path.split(image_path)[0]:
        image_path = os.path.join(path_temporary, image_path)
    # Check the file name extension.
    if os.path.splitext(image_path)[1] == '':
        image_path += '.png'
    # Create the image HTML tag.
    if os.path.isfile(image_path):
        # image_path = os.path.relpath(
        #        os.path.dirname(__file__),
        #        image_path)
        if not image_size:
            image_size = '33%'
        else:
            if isinstance(image_size, (float, int)):
                if image_size <= 1.0:
                    image_size = image_size * 100
                image_size = '%g%%' % image_size
        # It is necessary to go one level up on the directory tree after
        # I move all this code to the `Process` folder. This is necessary
        # to the `weasyprint` final command on `generate_report.py` script
        # understand the correct image `%PROJECT/Tempory` path.
        return '<img src="%s" width=%s>' % (os.path.join('..', image_path), image_size)
    else:
        # Some image may be not created because of missing data.
        return ''


def table_to_html(values: dict | pd.DataFrame,
                  title: str | None = None) -> str:
    '''Format a table of data in a HTML table for better
    visualization of the reported file.

    Parameters:
        values (dict, pd.DataFrame): Values used to table
                                     generation.
        title (str): Table tile.

    Return the HTML converted string.
    '''
    # Reference for implementation:
    # https://stackoverflow.com/questions/41671436/how-convert-python-dictionary-to-html-table
    if not isinstance(values, pd.DataFrame):
        values = pd.DataFrame(data=values)
    values = values.fillna(' ').T
    html_string = values.to_html(float_format=OUTPUT_FLOAT_FORMAT,
                                 decimal=',',
                                 na_rep='<em>indisponível</em>',
                                 justify='center',  # Centralize header.
                                 render_links=True)  # Make HTML hyperlinks.
    print(values.to_latex(float_format=OUTPUT_FLOAT_FORMAT,
                          decimal=',',
                          na_rep='<em>indisponível</em>'))
    return html_string


# %%
# Main entry, used just for debug.
if __name__ == "__main__":

    # Test numeric output.
    assert number_to_html(13335) == '13.335'
    assert number_to_html(85613335) == '85.613.335'
    assert number_to_html(133.35) == '133,35'
    assert number_to_html(856133.35) == '856.133,35'
    assert number_to_html(7856133.35) == '7.856.133,35'

    # Test string output.
    assert join_list_str('a') == 'a'
    assert join_list_str('a') == 'a'
    assert join_list_str(['a']) == 'a'
    assert join_list_str(['a', 'b']) == 'a e b'
    assert join_list_str(['a', 'b', 'c']) == 'a, b e c'
    assert join_list_str(['a', 'b', None, 'c']) == 'a, b e c'

    # Test currency output.
    assert get_default_unit('... valor ...') == 'R$'
    assert get_default_unit('... saldo ...') == 'R$'
    assert get_default_unit('... ...') is None

    # Test link output.
    link = 'https://stackoverflow.com/questions'
    assert get_link_from_string('0d0sd0' + link + ' jkjkjk') == link
    assert get_link_from_string('0d0sd0<' + link + '>jkjkjk') == link

    # Test HTML formatting.
    assert html_section_id_name() == ''
    assert html_section_id_name('') == ''
    assert html_section_id_name('escola da faculdade') == \
        HTML_SECTION_ID_NAME_SEP.join([HTML_SECTION_ID_NAME_CHAPTER_START, 'escoladafaculdade'])
    assert html_section_id_name('escola da faculdade', 'categoria do item') == \
        HTML_SECTION_ID_NAME_SEP.join([HTML_SECTION_ID_NAME_SECTION_START, 'escoladafaculdade', 'categoriadoitem'])

    # Test conversion to HTML.
    extra_data = {'Quant. items': 12903,
                  'Tipo': {'Quant. items': 12903, 'Dados faltando': 0, 'Mais frequente': 'estabilizador'},
                  'Data de compra': {'Dados faltando': 0, 'Menor valor': 1963.0, 'Maior valor': 2024.0,
                                     'Mais frequente': 2009.0, 'Média': 2005.281019917849},
                  'Potência elétrica (VA)': {'Dados faltando': 4507, 'Menor valor': 1.4, 'Maior valor': 1200273921200.0,
                                             'Mais frequente': 1000.0, 'Média': 146007862.328061, 'Total': 1225882012106.4,
                                             'Proj. frequente': 1225886519106.4, 'Proj. média': 1883939447618.971}}
    print(number_to_html(1200273921200.0, 'VA', 'Maior valor', extra_data))
