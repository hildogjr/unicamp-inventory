#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide all the all functions for general analysis and chart
# generation from statistics of all items.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from datetime import datetime
from collections import Counter
import re

# Required but not default libraries.
import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import seaborn as sns
# from scipy import stats
from statistics import mode

# Specific application libraries.
from general_definitions import (UNIDENTIFIED_TYPE, COLUMN_UNIT_NAME, configs, _, print)
from format_output import STATISTIC_SUBNAMES
from copyright import imagefile_add_watermark


# Setup the chart dimensions, font and size.
# Decrease the size of the image and increase all font sizes for
# better dimension on the PDF report and decrease the DPI from 100
# to minimum, for small file size.
# The labels and ticks are changed from `'medium'`.
if configs['charts']['reduced']:
    plt.rcParams['figure.dpi'] = 75  # plt.rcParams['figure.dpi'] * 0.9
    plt.rcParams['figure.figsize'] = [plt.rcParams['figure.figsize'][0] * 0.95,
                                      plt.rcParams['figure.figsize'][1] * 0.95]
plt.rcParams['font.size'] = plt.rcParams['font.size'] * 1.1
plt.rcParams['axes.labelsize'] = 'large'
plt.rcParams['xtick.labelsize'] = 'large'
plt.rcParams['ytick.labelsize'] = 'large'


# This definitions are used for the above level of the code and filed
# with definitions and callable references to the bellow functions
# and others.
if 'items_definitions' not in globals():
    items_definitions = {}


def sort_statistics(statistics: dict) -> dict:
    '''Sort the statistic dict by the order given by
    `STATISTIC_SUBNAMES` values.'''
    stat = {}
    for name in STATISTIC_SUBNAMES.values():
        if name in statistics.keys():
            stat[name] = statistics[name]
    return stat


# %%
# General functions to analyse and plot data.

def data_to_chart(data: list | pd.Series,
                  consider_zero_invalid: bool = False,
                  consider_negative_invalid: bool = False) -> list | pd.DataFrame:
    ''''Convert the data to type need for the chart analysis
    or for extract some characteristic.

    Parameters:
        data: Data to check.
        consider_zero_invalid (Default False): Consider 0.0 (zero) as
                    invalid data, this is used to eliminate "invalid"
                    numeric values on mandatory original database fields.
                    The data must be numeric (float or int) for this option.
        consider_negative_invalid (Default False): Consider negative value
                    as invalid as the above description.

    Returns: validated data, number of not valid values.'''
    data_original_len = len(data)

    # Remove non valid or absent data (`None` or `pandas.nan` of Excel).
    data = data.dropna()
    if data.isnull().values.any():
        print('-> Error: it was not removed a NaN Pandas value.')
    data_invalid_len = data_original_len - data.size

    # If all data are valid, it is possible that was used
    # `UNIDENTIFIED_TYPE` to marked not classified or information
    # missing for such identification. In this case, do not
    # remove the values identified as `UNIDENTIFIED_TYPE` but
    # count them as invalid data.
    if data_invalid_len == 0:
        # and type(data[0]) == type(UNIDENTIFIED_TYPE):
        # The above test not necessary and also `pandas` library uses
        # a `dtype('O')` instead of `str` object for strings.
        # data = data[~(data == UNIDENTIFIED_TYPE)]
        data_invalid_len = sum(data == UNIDENTIFIED_TYPE)

    # Evaluate the return values.
    if data_invalid_len == data_original_len:
        # Empty data.
        return [], data_invalid_len
    if isinstance(data.iloc[0], (pd.Timestamp, np.datetime64, datetime)):
        # Extract only the year for analysis in case of `datetime` / `pd.datetime64`.
        data = data.apply(lambda x: x.year)
        return data, data_invalid_len
    elif all(isinstance(d, str) and re.search(r'^[\-\+\ ]*[\,\.\d]+$', d) for d in data):
        # The UNICAMP database export procedure force all data be
        # string, this is wrong, so the above code is a workaround
        # to reconvert it into numeric value.
        def db_str_to_num(d):
            d = d.replace('.', '').replace(' ', '')
            if re.search(r'^[\-\+]*\d+\,\d+', d):
                return float(d.replace(',', '.'))
            else:
                return int(d)
        data = data.apply(db_str_to_num)
    # If asked, consider the zero value as invalid and remove
    # them from the data to analyze.
    if consider_zero_invalid and data.dtypes in (int, float):
        data_invalid_len += sum(data == 0)
        data = data[data != 0]
    # If asked, consider the negative value as invalid and remove
    # them from the data to analyze.
    if consider_negative_invalid and data.dtypes in (int, float):
        data_invalid_len += sum(data < 0)
        data = data[data >= 0]
    return data, data_invalid_len


def chart_save(plot_handle: plt, file_name: str):
    '''General function with parameters to save the charts.

    Parameters:
        Plot handle object.
        File name string.
    '''
    if file_name:
        # Use the default (better resolution) in case of English language
        # used to generate chart images for articles and a lower resolution
        # to the report for better compression.
        plot_handle.savefig(file_name, bbox_inches='tight', pad_inches=0.05,
                            )  # dpi=(75 if configs.get('translate language') == 'en_US' else 100))
        plot_handle.close()

        if configs.get('charts') and configs['charts'].get('add watermark'):
            imagefile_add_watermark(file_name)  # Add my personal logo as watermark.
    else:
        plot_handle.show()


# %%
# Specific functions to analyze and plot data.

def analyze_continuous_chart(data: pd.DataFrame,
                             column: str,
                             file_name: str | None = None,
                             title: str | None = '',
                             xlabel: str | None = '',
                             ylabel: str | None = None,
                             evaluate_sum: bool | None = True) -> dict:
    '''Plot and analyze continuous number list creating a histogram and
    the conventional statistics.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        file_name (str): Path and file name to save the chart
                         (for future use into the report).
        title (str): Chart title.
        xlabel (str): Chart X label name.
        ylabel (str): Chart Y label name.
        evaluate_sum (bool): Evaluate the total statistic.

    Return statistics in a dictionary.
    '''
    if 'data' in column.lower():
        # Do not evaluate sum or projected sum of dates.
        evaluate_sum = False
    data_to_analyze, data_invalid_len = data_to_chart(data[column])

    statistics = {STATISTIC_SUBNAMES['missing']: data_invalid_len}
    # If empty data, the only statistic is the total missing.
    if data_invalid_len == len(data[column]):
        return statistics

    statistics.update({
        STATISTIC_SUBNAMES['min']: np.min(data_to_analyze),
        STATISTIC_SUBNAMES['max']: np.max(data_to_analyze),
        STATISTIC_SUBNAMES['avg']: np.mean(data_to_analyze),
        # STATISTIC_SUBNAMES['std']: np.std(data_to_analyze),
        # STATISTIC_SUBNAMES['median']: np.median(data_to_analyze),
        STATISTIC_SUBNAMES['mode']: mode(data_to_analyze)  # stats.mode(data_to_analyze).mode[0],
    })
    if evaluate_sum:
        statistics[STATISTIC_SUBNAMES['sum']] = np.sum(data_to_analyze)
        if data_invalid_len > 0:
            # Projected total.
            statistics[STATISTIC_SUBNAMES['projected mode']] = (  # noqa
                statistics[STATISTIC_SUBNAMES['sum']]
                + statistics[STATISTIC_SUBNAMES['mode']] * data_invalid_len)
            statistics[STATISTIC_SUBNAMES['projected']] = \
                statistics[STATISTIC_SUBNAMES['avg']] * len(data[column])  # noqa
    if len(data) == 1:
        # Do not create a chart with a unique category/element.
        return sort_statistics(statistics)

    density = False
    if not ylabel:
        if density:
            ylabel = _('Probabilidade')
        else:
            ylabel = _('Quantidade')
    # hist, bins = np.histogram(data_to_analyze, density=density)#, bins='auto')  # Compute histogram.
    # bins_w = np.diff(bins)  # Width of each bin.
    # hist_p = hist * bins_w  # Compute proportion of sample in each bin.
    # plt.bar(bins[:-1], hist_p, width=bins_w, align='edge')
    plt.hist(data_to_analyze, bins=int(np.ceil(np.sqrt(len(data_to_analyze)))))
    plt.axvline(statistics[STATISTIC_SUBNAMES['avg']], color='k', linestyle='dashed', linewidth=1)

    # Add a 'best fit' normal line.
    # mu, sigma = stats.norm.fit(data_to_analyze)
    # y = mlab.normpdf(bins, mu, sigma)
    # plt.plot(bins, y, 'r--')
    if not xlabel:
        if configs.get('translate language') == 'en_US':
            # I use this particular configuration to generate charts
            # for the article that I am writing.
            if column == 'Data de compra':
                xlabel = 'Purchase date (year)'
            else:
                xlabel = ''
        else:
            xlabel = column
    title_str = ''
    if title:
        title_str = '%s: ' % title
    # title_str += '$\overline{x}=%.2f$, $\sigma=%.2f$' % \
    #             (statistics[STATISTIC_SUBNAMES['avg']],
    #              statistics[STATISTIC_SUBNAMES['std']])
    plt.title(title_str)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # Tweak spacing to prevent clipping of ylabel
    # plt.subplots_adjust(left=0.15)
    plt.autoscale()
    plt.grid()
    chart_save(plt, file_name)

    return sort_statistics(statistics)


def analyze_discrete_chart(data: pd.DataFrame,
                           column: str,
                           file_name: str | None = None,
                           title: str | None = '',
                           xlabel: str | None = '',
                           ylabel: str | None = _('Quantidade'),
                           evaluate_sum: bool | None = True) -> dict:
    '''Plot and analyze discrete number list bar graph and
    the conventional statistics.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        file_name (str): Path and file name to save the chart
                         (for future use into the report).
        title (str): Chart title.
        xlabel (str): Chart X label name.
        ylabel (str): Chart Y label name.
        evaluate_sum (bool): Evaluate the total statistic.

    Return statistics in a dictionary.
    '''
    if 'data' in column.lower():
        # Do not evaluate sum or projected sum of dates.
        evaluate_sum = False
    data_to_analyze, data_invalid_len = data_to_chart(data[column])

    statistics = {STATISTIC_SUBNAMES['missing']: data_invalid_len}
    # If empty data, the only statistic is the total missing.
    if data_invalid_len == len(data[column]):
        return statistics

    average = np.mean(data_to_analyze)
    statistics.update({
        STATISTIC_SUBNAMES['min']: np.min(data_to_analyze),
        STATISTIC_SUBNAMES['max']: np.max(data_to_analyze),
        STATISTIC_SUBNAMES['avg']: (int(average) if isinstance(data_to_analyze.iloc[0], int) else average),
        # STATISTIC_SUBNAMES['median']: int(np.median(data_to_analyze)),
        STATISTIC_SUBNAMES['mode']: mode(data_to_analyze)  # stats.mode(data_to_analyze).mode[0],
    })
    if evaluate_sum:
        statistics[STATISTIC_SUBNAMES['sum']] = np.sum(data_to_analyze)
        if data_invalid_len > 0:
            # Projected total.
            statistics[STATISTIC_SUBNAMES['projected mode']] = (
                statistics[STATISTIC_SUBNAMES['sum']]
                + statistics[STATISTIC_SUBNAMES['mode']] * data_invalid_len)
            statistics[STATISTIC_SUBNAMES['projected']] = \
                statistics[STATISTIC_SUBNAMES['avg']] * len(data[column])

    scores = Counter(data_to_analyze)
    if len(data) == 1:
        # Do not create a chart with a unique category/element.
        return sort_statistics(statistics)
    if 'data' not in column.lower() or 'data' in column.lower():
        # Common bar chart.
        plt.bar(scores.keys(), scores.values())
        # statistics[STATISTIC_SUBNAMES['score']] = scores
    else:
        # Stacked bar chart with type as division.
        # Documentation in https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/bar_stacked.html

        # Get the list of general type frequency.
        kind_frequent = sorted(scores,
                               key=scores.__getitem__,
                               reverse=True)
        # TODO
        handle_bar_chart = []
        for kind in kind_frequent:
            data_to_analyze_kind = data_to_analyze
            scores = Counter(data_to_analyze_kind)
            handle_bar_chart.append(
                plt.bar(scores.keys(), scores.values())
            )
        plt.legend(handle_bar_chart, kind_frequent)

    if not xlabel:
        if configs.get('translate language') == 'en_US':
            # I use this particular configuration to generate charts
            # for the article that I am writing.
            if column == 'Data de compra':
                xlabel = 'Purchase date (year)'
            else:
                xlabel = ''
        else:
            xlabel = column
    title_str = ''
    if title:
        title_str = '%s: ' % title
    # title_str += '$\overline{x}=%i$' % statistics[STATISTIC_SUBNAMES['avg']]
    plt.title(title_str)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # Tweak spacing to prevent clipping of ylabel
    # plt.subplots_adjust(left=0.15)
    plt.autoscale()
    plt.grid()
    chart_save(plt, file_name)

    return sort_statistics(statistics)


def analyze_percentage_chart(data: pd.DataFrame,
                             column: str,
                             file_name: str | None = None,
                             title: str | None = '') -> dict:
    '''Plot and analyze percentage groups list creating a pie chart and
    the conventional statistics.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        file_name (str): Path and file name to save the chart
                         (for future use into the report).
        title (str): Chart title.

    Return statistics in a dictionary.
    '''
    data_to_analyze, data_invalid_len = data_to_chart(data[column])

    statistics = {}
    if 'tipo' in column.lower():
        statistics[STATISTIC_SUBNAMES['total']] = len(data)
    statistics[STATISTIC_SUBNAMES['missing']] = data_invalid_len
    # If empty data, the only statistic is the total missing.
    if data_invalid_len == len(data[column]):
        return statistics

    # The most frequent value considers only the valid value,
    # not take into account `UNIDENTIFIED_TYPE` if it was the
    # bigger occurrence.
    # most_frequent = stats.mode(data_to_analyze).mode
    most_frequent = Counter(data_to_analyze)
    most_frequent = sorted(most_frequent,
                           key=most_frequent.__getitem__,
                           reverse=True)
    if most_frequent[0] == UNIDENTIFIED_TYPE and len(most_frequent) >= 2:
        most_frequent = most_frequent[1]
    else:
        most_frequent = most_frequent[0]

    statistics.update({
        # STATISTIC_SUBNAMES['avg']: np.mean(data_to_analyze),
        # STATISTIC_SUBNAMES['median']: np.median(data_to_analyze),
        STATISTIC_SUBNAMES['mode']: most_frequent,
    })

    scores = Counter(data_to_analyze)
    if len(data) == 1:
        # Do not create a chart with a unique category/element.
        return sort_statistics(statistics)
    # most_common = scores.most_common(1)[0][0]
    # most_common = statistics[STATISTIC_SUBNAMES['mode']]
    # idx_big = list(scores).index(most_common)
    # explode = [0] * len(scores)
    # explode[idx_big] = 0.1  # Explode just the big one.
    explode = [0.1] * len(scores)  # Explode all.
    # plt.pie(scores.values(), explode=explode, labels=scores.keys(),
    #        autopct='%1.1f%%', pctdistance=1.1, labeldistance=1.2)
    # statistics[STATISTIC_SUBNAMES['score']] = scores

    # Use a better legend method, the inside `plt.pie` function method
    # does not allow long text description or are to compressed getting
    # overlaped when the perceptual is too small.
    # https://matplotlib.org/3.1.1/gallery/pie_and_polar_charts/pie_and_donut_labels.html
    def perceptual_absolute(pct, allvals):
        absolute = round(pct / 100.0 * np.sum(allvals))
        return '{:.2f}%\n({:d})'.format(pct, absolute)

    # Using some value to rotate clockwise the chart start
    # (startangle=-40) produces a better space for the legend.
    patches, _ = plt.pie(scores.values(), explode=explode,
                         # autopct=lambda pct: perceptual_absolute(pct, list(scores.values())),
                         pctdistance=1.1, labeldistance=1.2, startangle=-40)
    '''
    groups = list(scores.keys())
    values = list(scores.values())
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    kw = dict(arrowprops=dict(arrowstyle="-"),
                bbox=bbox_props, zorder=0, va="center")
    for i, p in enumerate(patches):
        annotation = '%s\n%.2f%% (%d)' % \
                        (groups[i], values[i]/np.sum(values)*100, np.sum(values))
        ang = (p.theta2 - p.theta1)/2. + p.theta1
        if ang == 180.:
            ang = 90.
        y = np.sin(np.deg2rad(ang))
        x = np.cos(np.deg2rad(ang))
        #horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
        horizontalalignment = 'center'
        connectionstyle = "angle,angleA=0,angleB={}".format(ang)
        kw["arrowprops"].update({"connectionstyle": connectionstyle})
        plt.annotate(annotation, xy=(x, y), xytext=(1.35*np.sign(x), 1.4*y),
                        horizontalalignment=horizontalalignment, **kw)
    '''
    legend_order = list(np.argsort(list(scores.values())))[::-1]
    legend_labels = ['%s: %.2f%% (%d)' %
                     ((label.capitalize() if label != UNIDENTIFIED_TYPE else label.upper()),
                      value / np.sum(list(scores.values())) * 100,
                      value)
                     for label, value in scores.items()]
    plt.legend([patches[o] for o in legend_order],
               [legend_labels[o] for o in legend_order],
               loc='best')

    title_str = ''
    if title:
        title_str = '%s: ' % title
    # title_str += '$\overline{x}=%.2f$' % statistics['average']
    plt.title(title_str)

    # Tweak spacing to prevent clipping of ylabel
    # plt.subplots_adjust(left=0.15)
    plt.autoscale()
    plt.axis('equal')
    chart_save(plt, file_name)

    return sort_statistics(statistics)


def analyze_scatter_chart(data: pd.DataFrame,
                          column: str,
                          file_name: str | None = None,
                          title: str | None = '',
                          xlabel: str | None = '',
                          ylabel: str | None = '') -> dict:
    '''Plot and analyze dispersal values chart and
    the conventional statistics.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        file_name (str): Path and file name to save the chart
                         (for future use into the report).
        title (str): Chart title.
        xlabel (str): Chart X label name.
        ylabel (str): Chart Y label name.

    Return statistics in a dictionary.
    '''
    data_to_analyze, data_invalid_len = data_to_chart(data[column],
                                                      consider_zero_invalid=True,
                                                      consider_negative_invalid=True)
    statistics = {}
    statistics[STATISTIC_SUBNAMES['missing']] = data_invalid_len
    # If empty data, the only statistic is the total missing.
    if data_invalid_len == len(data[column]):
        return statistics
    statistics.update({
        STATISTIC_SUBNAMES['avg']: np.mean(data_to_analyze),
        STATISTIC_SUBNAMES['min']: np.min(data_to_analyze),
        STATISTIC_SUBNAMES['max']: np.max(data_to_analyze),
        STATISTIC_SUBNAMES['sum']: np.sum(data_to_analyze),
    })

    sns.stripplot(y=data_to_analyze)
    # if 'Tipo' in data_clean.columns:
    #    group_type = data_clean['Tipo'] or fnc_type(foo)
    #    sns.stripplot(y=data_to_analyze, x=group_type)

    title_str = ''
    if title:
        title_str = '%s: ' % title
    # title_str += '$\overline{x}=%.2f$' % statistics['average']
    plt.title(title_str)

    # Tweak spacing to prevent clipping of ylabel
    # plt.subplots_adjust(left=0.15)
    plt.autoscale()
    plt.axis('equal')
    plt.grid()
    chart_save(plt, file_name)

    return sort_statistics(statistics)


# %%
# Specific functions to classify and plot data.

def classify_type(data: pd.DataFrame,
                  column: str,
                  file_name: str | None = None,
                  title: str | None = '',
                  xlabel: str | None = _('Unidades'),
                  yabel: str | None = _('Quantidade')):
    '''Plot and analyze classification list creating a bar chart and
    the conventional statistics.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        file_name (str): Path and file name to save the chart
                         (for future use into the report).
        title (str): Chart title.
        xlabel (str): Chart X label name.
        ylabel (str): Chart Y label name.

    Return statistics in a dictionary.
    '''
    return
    data_to_analyze, data_invalid_len = data_to_chart(data[column])
    COLUMN_UNIT_NAME

    title_str = ''
    if title:
        title_str = '%s: ' % title
    # title_str += '$\overline{x}=%.2f$' % statistics['average']
    plt.title(title_str)

    plt.autoscale()
    # This chart will be full page width.
    # plt.axis('equal')
    chart_save(plt, file_name)


def classify_density(data: pd.DataFrame,
                     column: str,
                     file_name: str | None = None,
                     title: str | None = '',
                     xlabel: str | None = _('Unidades'),
                     yabel: str | None = _('Quantidade')):
    '''Plot and analyze classification list creating a bar chart and
    the conventional statistics.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        file_name (str): Path and file name to save the chart
                         (for future use into the report).
        title (str): Chart title.
        xlabel (str): Chart X label name.
        ylabel (str): Chart Y label name.

    Return statistics in a dictionary.
    '''
    return
    data_to_analyze, data_invalid_len = data_to_chart(data[column])
    COLUMN_UNIT_NAME

    title_str = ''
    if title:
        title_str = '%s: ' % title
    # title_str += '$\overline{x}=%.2f$' % statistics['average']
    plt.title(title_str)

    plt.autoscale()
    # This chart will be full page width.
    # plt.axis('equal')
    chart_save(plt, file_name)
