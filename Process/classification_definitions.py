#!python
# This file is covered tby GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide all the unit classification analysis definitions.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
# import re
import glob
import os
import importlib

# Required but not default libraries.
# import numpy as np
# try:
#     import modin.pandas as pd
# except ImportError:
#     import pandas as pd

# Specific application libraries.
from analysis_chart import *
# from general_definitions import UNIDENTIFIED_TYPE, COLUMN_UNIT_NAME, _

# Load the specific scrape/search definitions for each item type. They
# are defined into files named as `items_*.py` (excluding some special
# ones). The `items_definitions`is are inside each bellow submodule.
# items_definitions = {}
from items_airconditioners import *
from items_ups import *
from items_computers import *
from items_routers import *
from items_fridges import *
from items_transformers import *
'''
items_module_names = glob.glob('items_*.py')
for item_module_name in items_module_names:
    # Do not import the test, low level and current file.
    if item_module_name in ('items_general_info.py', 'items_testcase.py', 'items_definitions.py'):
        continue
    # Import all the equipment items definitions.
    item_module_name = os.path.splitext(item_module_name)[0]
    item_module = importlib.import_module(item_module_name)
    # The bellow code was borrow from:
    # https://stackoverflow.com/questions/21221358/python-how-to-import-all-methods-and-attributes-from-a-module-dynamically
    module_dict = item_module.__dict__
    try:
        to_import = item_module.__all__
    except AttributeError:
        to_import = [name for name in module_dict if not name.startswith('_')]
    globals().update({name: module_dict[name] for name in to_import})
'''


# %%
# Auxiliary functions to filter data or get specific analisis/charts based
# on the `analysis_chart.py` functions.

def chart_type(data, item_category, *args, **kargs):
    '''
    '''
    if item_category.lower() == 'ar condicionados':
        unified_data = unify_airconditioners_systems(data)
        classify_type(data=[unified_data[COLUMN_UNIT_NAME].value_counts(), data], *args, **kargs)
    else:
        classify_type(data=data, *args, **kargs)


# %%
# References to functions of classification charts.

classification_definitions = {
    'Tipo': None,  # chart_type,
    # 'Data de compra': None,
    'Potência elétrica (kWh)': None,  # classify_density,
    'Potência frigorífica (BTU/h)': None,  # classify_density,
    # 'Selo PROCEL': None,
    # 'Localização': None,
    # 'Modo de aquisição': None,
}
