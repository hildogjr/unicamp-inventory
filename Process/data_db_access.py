#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas NOT classified
# information, it is covered by Federal Brazilian Law 12527
# from 18/Nov./2011 (public data access):
# http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm.
#
# Extract the original databases from the UNICAMP patrimony system.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import os
from datetime import datetime


# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
import psycopg  # psycopg-pool, psycopg-binary, psycopg
# import sqlalchemy  # sqlalchemy psycopg2-binary greenlet

# Internal libraries.


print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Extraindo banco de dados em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)


# %%
# Main class

# Check the application variables and fill the login form.
if not os.getenv('DB_CONN'):
    try:
        from dotenv import load_dotenv
        print('Trying to force environment load (used on debug mode)...', end='')
        load_dotenv()
        print(' loaded.')
    except ImportError:
        pass
    if not os.getenv('DB_CONN'):
        exit('Missed user and password registry on application.')

# db_conn = sqlalchemy.create_engine(os.getenv('DB_CONN'))
# data = pd.read_sql_query('SELECT column_name FROM information_schema.columns WHERE table_schema=\'public\''
#                          ' AND table_name=\'VPTR30\' ORDER BY column_name ASC', db_conn.raw_connection())
# #data = pd.read_sql_query('select * where descr_are_pat==CEL', db_conn.raw_connection())
# print(data.size)

with psycopg.connect(os.getenv('DB_CONN'), connect_timeout=3.0) as db_conn:
    data = pd.read_sql_query('SELECT column_name FROM information_schema.columns WHERE table_schema=\'public\''
                             ' AND table_name=\'VPTR30\' ORDER BY column_name ASC', db_conn)
    # data = pd.read_sql_query('select * where descr_are_pat==CEL', db_conn.raw_connection())
    # db_cursor = db_conn.cursor()
    # result = db_cursor.execute()
    print(data.size)
    pass

if __name__ == "__main__":
    pass
