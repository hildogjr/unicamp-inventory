#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide all the general aspects of search and scrape definitions
# used by all classes of items.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
from datetime import date, datetime
import re

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
import numpy as np
try:
    from numba import jit
except ImportError:
    print('--->>> Not using JIT compilation.')

    def jit(x):
        return x

# Internal libraries.
from general_definitions import GENERAL_SEP, UNIDENTIFIED_TYPE, _, configs, print

# This definitions are used for the above level of the code and filed
# with definitions and callable references to the bellow functions
# and others.
if 'items_definitions' not in globals():
    items_definitions: dict[str, Any] = {}


# %%
# General auxiliary functions.

@jit
def text_warning_missing(row: pd.DataFrame,
                         missing: str,
                         category: str | None = None,
                         message: str | None = None) -> str:
    '''Create the text message of missing some characteristic.

    Parameters:
        row (pd.DataFrame): Data row of a Pandas DataFrame object.
        base_msg (str): Base message of the warning.
        Initial "missing message".

    Return (str) text for warning.
    '''
    if message is None:
        message = '\tSem '
    message += '"%s" para %i' % (missing, row['Identificador'])
    if category:
        message += '(%s)' % category
    message += f': ({row["Marca"]} : {row["Modelo"]}) {row["Descrição"].strip()}'
    return message


def is_subtype_equipment(equipment_subtype: str | None, type_group: tuple[str]) -> bool:
    '''Check if a equipment sub type belong to a group.
    '''
    if not equipment_subtype or equipment_subtype is np.nan:
        return False
    return any(t in equipment_subtype for t in type_group)


# %%
# General functions to get data from the original databases.

INFO_TO_SAVE = configs['identify'].get('info to save')


def get_location(item_idx: int,
                 category: str,
                 row: pd.DataFrame,
                 found_statistics: dict[str, Any] = {}) -> str:
    '''Get the item location.

    Parameters:
        item_idx (int): Identification number.
        category (str): Item description matched.
        row (pd.DataFrame): Data row of a Pandas DataFrame object.
        Statistics already evaluated.

    Returns (str) the purchase data or incorporation date.
    '''
    try:
        # List of possible columns with location information.
        LOCATION_COLUMN_LIST = []
        # Add build and sector information only if is not saved
        # the specific column with such code.
        if 'Imóvel' not in INFO_TO_SAVE:
            LOCATION_COLUMN_LIST.append('Desc. Imóvel')
        if 'Órgão' not in INFO_TO_SAVE:
            LOCATION_COLUMN_LIST.append('Desc. Órgão')
        # Add others address information.
        LOCATION_COLUMN_LIST += ['Localização', 'Complemento Endereço',
                                 'Local N1', 'Local N2',
                                 'Local N3', 'Local N4', 'Local N5', ]
        location = []
        for column_name in LOCATION_COLUMN_LIST:
            # Check if the columns exist into the database and
            # it have a not null value. It is necessary due the
            # UNICAMP exportation procedure to spreadsheet that
            # do not give me access to all information.
            if column_name in row.keys():
                value = row[column_name]
                if not pd.isnull(value):
                    location.append(value)

        for idx in range(len(location) - 1, -1, -1):
            if pd.isnull(location[idx]):
                del location[idx]
            else:
                # Remove additional start and end spaces, newlines
                # and multiple internal spaces.
                location[idx] = location[idx].strip()
                location[idx] = re.sub(r'\s+', ' ', location[idx])
        return GENERAL_SEP.join(location)
    except TypeError:
        # Usually old items come here.
        print(text_warning_missing(row, 'Localização'))
        return ''


def get_purchase_data(item_idx: int,
                      category: str,
                      row: pd.DataFrame,
                      found_statistics: dict[str, Any] = {}) -> date | None:
    '''Get the purchase data if not present or invalid use the
    incorporation data.

    Parameters:
        item_idx (int): Identification number.
        category (str): Item description matched.
        row (pd.DataFrame): Data row of a Pandas DataFrame object.
        Statistics already evaluated.

    Return (datetime.date) Purchase data or incorporation date.
    '''
    try:
        if 'Data de compra' in row.keys():
            value = row['Data de compra']  # Used on the test cases.
        else:
            if 'Data Doc. Fiscal' in row.keys():
                value = row['Data Doc. Fiscal']
            else:
                value = row['Data Incorporação']
        try:
            value = datetime.strptime(value, r'%d/%m/%Y')
        except ValueError:
            try:
                value = datetime.strptime(value, r'%Y-%m-%d')
            except ValueError:
                value = datetime.strptime(value, r'%Y-%m-%d %H:%M:%S.%f')
        finally:
            return date(value.year, value.month, value.day)
    except (TypeError, KeyError):
        # Usually old items come here.
        print('\tData de compra não consta em %i (%s), assumido '
              'data de incorporação.' % (row['Identificador'], category))
        if 'Data de compra' not in row.keys():
            return None  # Used on the test cases.
        return None


@jit
def get_incorporation_method(item_idx: int,
                             category: str,
                             row: pd.DataFrame,
                             found_statistics: dict[str, Any] = {}) -> str:
    '''Get the incorporation method of the one item.

    Parameters:
        item_idx (int): Identification number.
        category (str): Item description matched.
        row (pd.DataFrame): Data row of a Pandas DataFrame object.
        Statistics already evaluated.

    Return (str) the type of purchase/donation/lending for the item.
    '''

    '''
    Second e-mail from Giovani Hermogenes Pereira.

    *Compra*: é a aquisição realizada pela Universidade, com recursos próprios ou por
    ela administrados, no mercado nacional ou estrangeiro, através de processo estabelecido
    conforme a Lei de Licitações e Contratos.
    *Doação*: é a entrega gratuita, por liberalidade, do direito de propriedade de bens ou
    vantagens em favor da Universidade. É a modalidade em que bens são recebidos gratuitamente,
    provenientes de qualquer pessoa ou instituição e aceitos pela instância competente.
    *Comodato ou cessão*: é o empréstimo gratuito de bem tangível, que deve ser restituído,
    prorrogado ou doado ao término do tempo convencionado.
    *Permuta*: é a contratação pelo qual as partes transferem e recebem, uma da outra, bens de
    igual natureza e/ou valor econômico, ou ainda, bens de diferente natureza e/ou valor econômico,
    mediante complementação ou contrapartida.
    *Produção Própria*: são os bens fabricados ou montados em oficinas, órgãos técnicos ou
    laboratórios da Universidade a partir de insumos, componentes ou peças anteriormente adquiridas.
    *Transformação*: consiste na alteração das características e funções de um bem pertencente à
    Universidade, provenientes da necessidade de divisão, aumento ou redução de medidas ou supressão
    de partes ou componentes, resultando em bem diferente do original.
    *Reposição*: ocorre quando um bem é oferecido para restituir outro bem anteriormente incorporado.
    *Avaliação (Apropriação)*: é a modalidade de registro, de caráter precário, que decorre da
    identificação de um bem reconhecidamente pertencente e de uso da Universidade que não dispõe de
    documentação de origem.
    *Adjudicação*: é o recebimento de bens decorrente de decisão judicial emanada a partir de
    ação movida pela Fazenda Pública, órgãos de controle e fiscalização ou pela própria Universidade.
    .
    Relação Modalidade X Fornecedor/Cedente:
    --------------------------------------
    Modalidade          Campo
    --------------------------------------
    Compra              Fornecedor
    Doação              Cedente
    Comodato            Cedente
    Permuta             Fornecedor
    Produção Própria    não se aplica
    Transformação       não se aplica
    Reposição           Fornecedor
    Avaliação           não se aplica
    Adjudicação         Cedente/Fornecedor
    --------------------------------------
    '''
    method = row['Modalidade']

    # On the Unicamp's DB event migration of end of May/2021 same data was
    # lost, the main reflect of this was some empty incorporation method.
    if pd.isna(method):
        method = ''

    incorporation_translate = {
        'compra': _('Compra'),
        'doacao': _('Doação'),
        'producao propria': _('Produção própria'),
        'reposicao': _('Reposição'),
        'transformacao': _('Doação'),
        'adjudcacao': _('Adjudicação'),  # Same as "avaliação".
        'comodato': _('Comodato'),
        'permuta': _('Permuta'),
    }
    return incorporation_translate.get(method.strip().lower(), UNIDENTIFIED_TYPE)
    '''
    if pd.isnan(row['Convênio']):
        return _('Convênio')
    #or pd.isna(row['Fim de Cessão']) \
    if pd.isna(row['Doc. Cedente']) or pd.isna(row['Cedente']) \
            or pd.isna(row['Doc. Benef./Cess.']) or pd.isna(row['Beneficiário/Cessionário']):
        return _('Cedido')
    if pd.isna(row['Fim de Comodato']):
        return _('Comodato')
    if pd.isna(row['Doc. Fornec.']) or pd.isna(row['Fornecedor']):
        return _('Fornecido')
    if pd.isna(row['Tipo Doc. Fiscal']) or pd.isna(row['Doc. Fiscal']):
        return _('Comprado')
    return UNIDENTIFIED_TYPE
    '''


def get_electric_power(item_idx: int,
                       category: str,
                       row: pd.DataFrame,
                       found_statistics: dict[str, Any] = {},
                       column_search: str = 'Descrição',
                       units_re: list[str] = ['VA', 'W'],
                       search_on_model: bool = True,
                       print_warning: bool = True,
                       re_remove: str | None = None) -> float | None:
    '''Get the electrical power.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.v
        Column name to search the information. The default is
            'Descrição', some user filled into 'Modelo'.
        Units for search in regular expression format in the priority order.
        Search on 'Modelo' column if did not found at 'Descrição'.
        Print message if not found value.

    Return watt or volt-ampere power (float).
    '''
    # Search definitions are inspired on `get_refrigeration_power_from_string(foo)`
    # at `items_airconditioners.py` identification definition file.
    RE_DECIMAL_SEP = r'[\.,; ]'  # r'[\.,]'
    RE_STARTER = r'((?!\d)|^|-|[a-z]\.)'  # '(?![a-z])'
    RE_TERMINATOR = r'(\s|\.|\;|\,|$|-|\/\d)'
    RE_NUM = r'(?P<result>\s*(\d+\s)*\d+' + RE_DECIMAL_SEP + r'*[o\d]*)'

    # To catch a specific error by a condition debug, use `idx == ID_INT_NUMBER` to pause
    # it is equivalent to `row['id'] == ID_INT_NUMBER` but `row['id']` is used as index
    # by my definition on the `pandas.dataframe` object.

    def desc_to_power(pattern: str,
                      idx_result: int | None = None) -> int | None:
        '''Get a power number from the `row[column_search]`.

        Parameters:
            Pattern to search in the `row[column_search]`.
            Optional index expected by the match answer. If
                `None`, use the group 'result'.

        Return:
            Numeric value.
        '''
        string_to_search = row[column_search]
        if pd.isna(string_to_search):
            return None
        if re_remove:
            string_to_search = re.sub(re_remove, '', string_to_search, flags=re.IGNORECASE)
        match = re.search(pattern, string_to_search, re.IGNORECASE)
        if match:
            if idx_result is None:
                power_string = match.group('result')
            else:
                power_string = match[idx_result]
            if not power_string and idx_result > 0:
                power_string = match[idx_result - 1]
            # Some typing error using '[oO]' instead '0' (zero).
            power_string = re.sub('o', '0', power_string, flags=re.IGNORECASE)

            power_string = power_string.strip()  # Necessary to get the correct center.
            # If the description text have, in sequence, the model finished
            # with number that is the same of the power plus the power text,
            # the two could have concatenated. Remove this (example, ID 575779).
            # This need to be done before the decimal mark remotion to allow
            # espaces.
            idx_half_string = int((len(power_string) - 1) / 2)
            if len(power_string) % 2 == 1 and power_string[idx_half_string] == ' ':
                if power_string[0:idx_half_string] == power_string[idx_half_string + 1:]:
                    power_string = power_string[idx_half_string + 1:]

            # Check if the decimal separator as "possible" just used as
            # thousand separator replacing by '.' or just removing it.
            # (examples: IDs 505604 and 505605 of FCM, 450326 and 450327
            # of FEM).
            idx_separator = re.search(RE_DECIMAL_SEP, power_string)
            if idx_separator and idx_separator.regs[0][1] == len(power_string) - 3:
                if power_string[idx_separator.regs[0][0]:idx_separator.regs[0][1]] == '.':
                    # If there is exactly there numbers after the possible decimal
                    # marker, it is because it is only a thousand marker.
                    power_string = power_string.replace('.', '')
                elif power_string[-1] == '0':
                    # Replace the decimal markers.
                    power_string = re.sub(r'(' + RE_DECIMAL_SEP + r')', '.', power_string)
            else:
                power_string = re.sub(r'[\s]', '', power_string)
                power_string = re.sub(r'[\,]', '.', power_string)

            try:
                return float(power_string)  # Return the integer.
            except ValueError:
                print('\tPotência elétrica "(%s)" não interpretada para %i (%s): %s' %
                      (power_string, item_idx, category, row['Descrição'].strip()))
                return None
        else:
            return None

    for unit_re in units_re:
        power_value = desc_to_power(RE_STARTER + RE_NUM + r'\ *k' + unit_re + RE_TERMINATOR)
        if power_value:
            return power_value * 1e3
        power_value = desc_to_power(RE_STARTER + RE_NUM + r'\ *' + unit_re + RE_TERMINATOR)
        if power_value:
            return power_value

    # If not found at the 'Descrição' column, try to found at the
    # 'Modelo' column by recursive use of the function.
    if search_on_model and column_search == 'Descrição':
        return get_electric_power(item_idx, category, row, found_statistics,
                                  column_search='Modelo', print_warning=False)

    if print_warning:
        print(text_warning_missing(row, 'Potência elétrica'))
    return None


# Global definitions used into the bellow function and in some other parts of the
# main code (charts and analysis).
# http://www.procelinfo.com.br/main.asp?TeamID=%7B88A19AD9-04C6-43FC-BA2E-99B27EF54632%7D
# http://www.inmetro.gov.br/consumidor/pbe/condicionadores.asp
PROCEL_STAMP_START_DATE = date(1993, 12, 8)  # Do not check equipments older.
# String to identify equipments produced before the "Selo PROCEL" obligation.
PROCEL_STAMP_BEFORE_TAG = _('Antes PROCEL')
PROCEL_STAMP_MISSING = UNIDENTIFIED_TYPE  # Item should but do not have a "Selo PROCEL".


def get_procel_stamp(item_idx: int,
                     category: str,
                     row: pd.DataFrame,
                     found_statistics: dict[str, Any] = {},
                     column_search: str = 'Descrição') -> str:
    '''Get the "Selo PROCEL" "A" to "E" classification of equipment
    efficiency in the power use.

    Parameters:
        item_idx (int): Identification number.
        category (str): Item description matched.
        row (pd.DataFrame): Data row of a Pandas DataFrame object.
        Column name to search the information. The default is
            'Descrição', some user filled into 'Modelo'.

    Return (str) the PROCEL A to E stamp.
    '''
    # General search definitions.
    PROCEL_STAMP_STRING = r'[\"\']*(?P<result>[A-E])[\"\']*'  # Stamp string in Regular Expression format.
    PROCEL_STAMP_END = r'([\s;\.,]|$)'  # Mark for a end match string.

    # If before the launch of the PROCEL program, do not search by the PROCEL
    # tag/stamp and mark the equipment as old for this attribute.
    purchase_date = get_purchase_data(item_idx, category, row)
    if purchase_date and purchase_date < PROCEL_STAMP_START_DATE:
        return PROCEL_STAMP_BEFORE_TAG

    def desc_to_procel_stamp(pattern: str,
                             idx_result: int | None = None) -> str:
        '''Get a 'selo PROCEL' from the `row[column_search]`.

        Parameters:
            Pattern to search in the `row[column_search]`.
            Optional index expected by the match answer. If
                `None`, use the group 'result'.

        Return (str) value A to E of "Selo PROCEL".
        '''
        match = re.search(pattern, row[column_search], re.IGNORECASE)
        if match:
            if idx_result is None:
                matched = match.group('result')
            else:
                matched = match[idx_result]
            # Usually the information on the UNICAMP database is all upper cased
            # but some new items (see "ar condicionados" of "CCUEC" on and after
            # 2020) have upper and lower characters on the descriptions.
            return matched.upper()
        else:
            return None

    procel_tag = desc_to_procel_stamp(r'classifica[cç][a~a]o +'
                                      + r'(do |no )*inmetro +'
                                      + PROCEL_STAMP_STRING + PROCEL_STAMP_END)
    if procel_tag:
        return procel_tag

    procel_tag = desc_to_procel_stamp(r'(selo )*procel[,\.]*.* *'
                                      + r'(classifica[cç][aã]o|efici[êe\?]ncia)* +'
                                      + r'(energ[eé\?]tica|n[íi\?]vel)* +'
                                      + r'(no m[ií]nimo|inmetro|letra)* *'
                                      + PROCEL_STAMP_STRING + PROCEL_STAMP_END)
    if procel_tag:
        return procel_tag

    procel_tag = desc_to_procel_stamp(r'(selo )*procel.* *'
                                      + r'classe '
                                      + PROCEL_STAMP_STRING + PROCEL_STAMP_END)
    if procel_tag:
        return procel_tag

    procel_tag = desc_to_procel_stamp(r'(selo )*procel\.* *'
                                      + PROCEL_STAMP_STRING + PROCEL_STAMP_END)
    if procel_tag:
        return procel_tag

    procel_tag = desc_to_procel_stamp(r'classifica[cç\?][aã\?]o +'
                                      + PROCEL_STAMP_STRING
                                      + r'((do +)*in*metro)*'
                                      + PROCEL_STAMP_END)
    if procel_tag:
        return procel_tag

    print(text_warning_missing(row, 'Selo PROCEL'))
    return PROCEL_STAMP_MISSING


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    test_data = pd.DataFrame.from_dict({
        'Identificador': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        'Descrição': [
            'CONDICIONADOR DE AR; DO TIPO JANELA/PAREDE. DESLIZANTE; COM CAPACIDADE DE NO MIN. 21.000 BTUS;'
            ' OPERACAO: CICLO FRIO; VAZAO DE AR DE NO MINIMO 940 M3H;VOLTAGEM: 220 VOLTS; COM CONSUMO APROXIMADO DE 21 80 W;'
            ' COM SELO PROCEL. CLASSIFICACAO A ; FUNCOES:TURBO/SLEEP/TIMER/AJUSTE AUTOMATICO DE TEMPERATUR',
            'CONDICIONADOR DE AR; SPLIT (HI WALL) - PAREDE; COM CAPACI DADE DE NO MIN. 12.000 BTU/H; POTENCIA MIN. DE 1.175 W.'
            ' OPERACAO A FRIO; FUNCOES: REFRIGERACAO. VENTILACAO E DESUMIDIFICACAO INDEPENDENTE; COMPOSTO DE 1 UNIDADE'
            ' CONDENSADORA EXTERNA E 1 UNIDADE EVAPORADORA INTERNA; SISTEMA DUPLO DE FILTRAGEM',
            'CONDICIONADOR DE AR; DO TIPO SPLIT PISO TETO; COM CAPACIDADE DE 36.000 BTUS/H; OPERACAO: FRIO; VAZAO DE AR DE'
            ' 1.150 M3/H; VOLTAGEM: 220 VOLTS; COM CONSUMO APROXIMADO DE 3.720 W; COM BAIXO RUIDO; EQUIPAMENTO COMPOSTO DE'
            ' 1 UNIDADE CONDENSADORA EXTERNA E 01 UNIDADE EVAPORADORA INTERNA; COM UTILIZACAO D',
            'UNIDADE EVAPORADORA dutada 14kw',
            'UNIDADE EVAPORADORA dutada 11, 2kw',
            'UNIDADE EVAPORADORA dutada 7kw',
            'TRANSFORMADOR TRIFASICO DE 112.5 KVA',
            'TRANSFORMADOR -112.5 KVA - 60 HZ',
            'TRANSFORMADOR QL-02 10 KVA-440V-220/127V',
            'TRANSFORMADOR TRIFASICO 500 KVA',
        ],
    })
    expected_values = {
        'power': [2180, 1175, 3720, 14e3, 11.2e3, 7e3, 112.5e3, 112.5e3, 10e3, 500e3],
        'procel': ['A', None, None, None, None, None, None, None, None, None, None],
    }
    for idx, db_row in test_data.iterrows():
        expected = expected_values['power'][idx]
        if expected:
            evaluated = get_electric_power(idx, '--nenhuma--', db_row,
                                           search_on_model=False, print_warning=False)
            if evaluated != expected:
                print(get_electric_power(idx, '--nenhuma--', db_row,
                                         search_on_model=False, print_warning=False))  # NOTE: debug exactly here.
            assert expected == evaluated
        expected = expected_values['procel'][idx]
        if expected:
            try:
                evaluated = get_procel_stamp(idx, '--nenhuma--', db_row)
                if evaluated != expected:
                    print(get_procel_stamp(idx, '--nenhuma--', db_row,
                                           search_on_model=False, print_warning=False))  # NOTE: debug exactly here.
                assert expected == evaluated
            except KeyError:
                pass
