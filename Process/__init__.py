#!python
# This file is covered tby GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# The databases are University of Campinas classified information.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

# This import is just for the hierarchical module image generation.
# The software execution is made by individually executing, in the
# sequency, each of the bellow python modules.
import download_data
import identify_items
import performe_analysis
import generate_report
import send_email
