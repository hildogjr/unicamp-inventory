#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Copyright definitions, used for attach information of my
# authorship to some specific file extensions.
# Allow the use of installation of python-pillow (already used
# by weasyprint v52.5 and v53.0, the HTML to PDF report generator
# that I use) and python-opencv for better compatibility with
# other codes.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['imagefile_add_watermark']


# Import one of the possible implementations.
try:
    from copyright_opencv import imagefile_add_watermark
except ImportError:
    try:
        from copyright_pillow import imagefile_add_watermark
    except ImportError:
        exit('Manipulation image packages not present on Python installation.')


# Definitions of shortcut methods.


# %%
# Main entry, used for debug. In this case also used to prompt use the watermark function.
if __name__ == "__main__":
    import argparse
    import os
    from glob import glob

    arg_parser = argparse.ArgumentParser(description='Add watermark to images.')
    arg_parser.add_argument('-i', '--images', '--input',
                            nargs='+',
                            type=str, default=None,
                            help='Images to receive the watermark.')
    arg_parser.add_argument('-l', '--logo',
                            type=str, default=None,
                            help='Logo image.')
    arg_parser.add_argument('-o', '--opacity',
                            type=float, default=None,
                            help='Opacity level for the logo image.')
    arg_parser.add_argument('-r', '--rotation',
                            type=float, default=None,
                            help='Rotation of the logo image.')
    args = arg_parser.parse_args()
    if args.images:
        # Define allowed file extensions for default search in folder paths.
        ALLOWED_FILE_EXTENSIONS = ('.png', '.jpg', '.jgep', '.gif')

        # If passed a path or file pattern, catch all the files present.
        if any([not os.path.isfile(file_input) for file_input in args.images]):
            file_input = []
            for path_input in args.images:
                if os.path.splitext(path_input)[1].lower() not in ALLOWED_FILE_EXTENSIONS:
                    for file_extension in ALLOWED_FILE_EXTENSIONS:
                        file_input.append(glob(path_input + f'{file_extension}'))
                else:
                    file_input.append(glob(path_input))
            args.images = file_input
        # Include the watermark to each image.
        for file_input in args.images:
            base_file_name = os.path.splitext(file_input)
            file_output = base_file_name[0] + ' (logo)' + base_file_name[1]
            imagefile_add_watermark(file_input, file_output,
                                    logo_file_name=args.logo,
                                    watermark_rotation=args.roration)
        exit()

    # Test image output.
    print('Using the debugging tests...')
    import shutil
    base_image_path = os.path.join('Test_data', 'chart_image.png')
    image_path1 = os.path.join('public', 'image1.png')
    shutil.copyfile(base_image_path, image_path1)
    imagefile_add_watermark(image_path1)
    assert os.path.isfile(image_path1), f'Failed {image_path1}'
    # Test a second time, because the test of logo already loaded.
    image_path2 = os.path.join('public', 'image2.png')
    shutil.copyfile(base_image_path, image_path2)
    imagefile_add_watermark(image_path2)
    assert os.path.isfile(image_path2), f'Failed {image_path2}'
    # Test a thirty time with other file extension.
    image_path3 = os.path.join('public', 'image3.jpg')
    shutil.copyfile(base_image_path, image_path3)
    imagefile_add_watermark(image_path3)
    assert os.path.isfile(image_path3), f'Failed {image_path3}'
