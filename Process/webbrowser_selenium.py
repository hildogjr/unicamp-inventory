# This file is covered tby GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Web browser utils.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['SeleniumBrowser']

# Base libraries.
import os

# Required but not default libraries.
# from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver import Firefox, FirefoxOptions, FirefoxService
from selenium.common.exceptions import WebDriverException
# from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# %%
# Auxiliary functions.

if os.name == 'nt':
    import ctypes
    from ctypes import windll, wintypes
    from uuid import UUID

    # ctypes GUID copied from MSDN sample code.
    class GUID(ctypes.Structure):
        _fields_ = [('Data1', wintypes.DWORD), ('Data2', wintypes.WORD),
                    ('Data3', wintypes.WORD), ('Data4', wintypes.BYTE * 8)]

        def __init__(self, uuidstr):
            uuid = UUID(uuidstr)
            ctypes.Structure.__init__(self)
            self.Data1, self.Data2, self.Data3, \
                self.Data4[0], self.Data4[1], rest = uuid.fields
            for i in range(2, 8):
                self.Data4[i] = rest >> (8 - i - 1) * 8 & 0xff

    SHGetKnownFolderPath = windll.shell32.SHGetKnownFolderPath
    SHGetKnownFolderPath.argtypes = [ctypes.POINTER(GUID), wintypes.DWORD,
                                     wintypes.HANDLE, ctypes.POINTER(ctypes.c_wchar_p)]

    def _get_known_folder_path(uuidstr):
        pathptr = ctypes.c_wchar_p()
        guid = GUID(uuidstr)
        if SHGetKnownFolderPath(ctypes.byref(guid), 0, 0, ctypes.byref(pathptr)):
            raise ctypes.WinError()
        return pathptr.value

    FOLDERID_Download = '{374DE290-123F-4565-9164-39C4925E467B}'

    def get_download_folder():
        return _get_known_folder_path(FOLDERID_Download)
else:
    def get_download_folder():
        return os.path.join(os.path.expanduser('~'), 'Downloads')


# %%
# Main class and functions.

class SeleniumBrowser():

    def __init__(self, download_path: str | None = None) -> None:
        ''' Selenium web browser puppeteer.
        Parameters:
            Keep the browser open util the software finish.
            Path to download folder.
        '''
        self.initialize_browser(download_path)

    def initialize_browser(self, download_path: str | None = None) -> None:
        '''Instantiate a web browser.'''
        opts = FirefoxOptions()

        # Check the presence of displays (if is on CI/CD environment or debug machines).
        try:
            display_response = os.popen('xrandr').read().lower()
            # if display_response:
            #    print(f'Answer to the display command query:\n{display_response}')
            # else:
            #    print('No answer do the display query command.')
            if not display_response or 'can\'t open display' in display_response or 'command not found' in display_response:
                opts.add_argument('--headless')  # Necessary on no-display machines.
                print('Assuming a not display machine...', end='')
        except Exception:
            opts.add_argument('--headless')
            print('Assuming a not display machine...', end='')

        # opts.add_argument("--no-sandbox")
        # opts.add_argument("--disable-gpu")
        # Active a deep log for debugging. When active, the username and
        # password login will be printed into the geckodriver.log file.
        # opts.log.level = "trace"

        if download_path:
            # As reported by me in https://github.com/SeleniumHQ/selenium/issues/9697,
            # Selenium/geckodriver for Mozilla Firefox doesn't recognize accented
            # characters being passed as `DOWNLOAD_PATH` but accept when manually
            # configured. This may cause issues in my local project folders.
            opts.set_preference('browser.download.folderList', 2)
            opts.set_preference('browser.download.dir', download_path)
            opts.set_preference('browser.download.lastDir', download_path)
            opts.set_preference('browser.download.manager.showWhenStarting', False)
            # For not annoying me when running in background.
            opts.set_preference('browser.download.alwaysOpenPanel', False)
            opts.set_preference('browser.download.panel.shown', False)
            # Default print to PDF.
            opts.set_preference('print.always_print_silent', True)
            opts.set_preference('print.printer_Mozilla_Save_to_PDF.print_to_file', True)
            opts.set_preference('print_printer', 'Mozilla Save to PDF')
            # Configurations found at https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types.
            opts.set_preference('browser.helperApps.neverAsk.saveToDisk',
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                                ';application/vnd.ms-excel'
                                ';application/octet-stream')  # Any extension #NOTE bellow.
            # Because the file is generated through a JavaScript action, Firefox appear to not
            # respect the MIME extension passed at the configuration profile to auto download.
            # It is used the global MIME type to by pass this. This is issue is possible caused
            # because initially the JavaScript create some link that is solved in the download
            # initialization time and doesn't match with XLSX extension.

        # opts.set_preference("browser.download.manager.focusWhenStarting", False)
        # opts.set_preference("browser.download.useDownloadDir", True)
        # opts.set_preference("browser.helperApps.alwaysAsk.force", False)
        # opts.set_preference("browser.download.manager.alertOnEXEOpen", False)
        # opts.set_preference("browser.download.manager.closeWhenDone", True)
        # opts.set_preference("browser.download.manager.showAlertOnComplete", False)
        # opts.set_preference("browser.download.manager.useWindow", False)
        # opts.set_preference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", False)
        # opts.set_preference("pdfjs.disabled", True)

        # opts.headless = True
        # Mozilla Firefox v90 doesn't correct start in some Ubuntu 20.04 installation
        # without a display https://bugzilla.mozilla.org/show_bug.cgi?id=1723112.
        # This prevents that I run this script direct on the LabREI server but
        # appear not affect the docker on Gitlab CI/CD. Also the fix appear to be
        # released on v91.
        # print('Initializing Firefox web browser...')
        try:
            if os.path.isfile('/snap/bin/geckodriver'):
                # Workaround off Snap missing gecko driver.
                self.driver = Firefox(options=opts,
                                      service=FirefoxService(executable_path='/snap/bin/geckodriver'))
            else:
                self.driver = Firefox(options=opts)
        except WebDriverException:
            gecko_driver_path_list = [os.path.abspath('geckodriver'),
                                      os.path.join('..', os.path.abspath('geckodriver')),
                                      os.path.join(os.path.expanduser('~'), 'Downloads', 'geckodriver')]
            for p in gecko_driver_path_list:
                if os.path.isfile(p):
                    print(f'Using "{p}" for gecko drive.')
                    # opts.binary_location = p
                    self.driver = Firefox(options=opts, service=FirefoxService(executable_path=p))
                    return
            exit('Not found the geckodriver.')

    def new_tab(self, url: str | None = None) -> str:
        '''Open a new browser tab.'''
        # browser.execute_script('window.open();')
        self.driver.execute_script('window.open("about:blank", "_blank");')
        # ActionChains(browser).key_down(Keys.COMMAND).send_keys('t').key_up(Keys.COMMAND).perform()
        # Change the active tab to the most recent opened.
        # browser.current_window_handle
        self.driver.switch_to.window(self.driver.window_handles[-1])
        if url:
            self.driver.get(url)
            # WebDriverWait(browser, timeout).until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
        return self.driver.window_handles[-1]

    def close_tab(self, handle: int | str | list[int | str] | None = None) -> None:
        ''''Close a tab or list of tabs by it index or handle.'''
        if handle is None:
            self.driver.switch_to.window(self.driver.window_handles[-1])
            self.driver.close()  # Close last tab.
        elif isinstance(handle, list):
            # Necessary to first get the handle from index to
            # not change the internal handle list length.
            handle = [h if isinstance(h, str) else self.driver.window_handles[h] for h in handle]
            for h in handle:
                self.driver.switch_to.window(h)
                self.driver.close()
        elif isinstance(handle, int):
            handle = self.driver.window_handles[handle]
        elif isinstance(handle, str):
            self.driver.switch_to.window(handle)
            self.driver.close()
        else:
            raise ValueError

    def wait_downloads(self, timeout: int = 60):
        '''Wait for all download to finish.'''
        if isinstance(self.driver, Firefox):
            self.new_tab('about:downloads')
            # files = browser.find_elements_by_class_name('download-state')
            # WebDriverWait(self.driver, URL_LOAD_TIMEOUT * 40).until(
            #    EC.presence_of_element_located((By.CLASS_NAME, 'downloadIconShow')))
            WebDriverWait(self.driver, timeout).until_not(
                EC.presence_of_element_located((By.CLASS_NAME, 'downloadIconCancel')))
            # 'downloadIconCancel'
        else:
            raise NotImplementedError

    def close(self):
        '''Close the web browser and clean the memory.'''
        self.wait_downloads()
        self.driver.close()
        self.driver.quit()

    def __enter__(self, *args, **kwargs):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


# %%
# Main entry, used just for debug.
if __name__ == '__main__':
    with SeleniumBrowser() as browser:
        tab_handle = [browser.new_tab(), browser.new_tab()]
        browser.new_tab('https://www.google.com.br/')
        browser.close_tab(tab_handle)
        browser.close_tab()
