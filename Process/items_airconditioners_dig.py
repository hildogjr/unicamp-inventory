#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide more deep specific definitions for search and scrape
# air conditioners ("ar condicionados") from manufacture/model
# and other databases or simple inference through the manufacture
# /model strings.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['get_refrigeration_power_from_model']

# Base libraries.
import re

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# from numba import jit

# Internal libraries.
# This is a auxiliary module to `items_arconditioners.py`.


# %%
# Specific functions to scrap from manufacture/model strings.

# @jit
def get_refrigeration_power_from_model(company: str, model: str) -> int | None:
    '''Get the refrigeration power.

    Parameters:
        Company name.
        Model name.

    Return BTU/h power.
    '''
    if pd.isnull(company) or pd.isnull(model):
        return None
    switch_manufacture = {}

    def search_btu_h_on_model_string(model: str,
                                     min_len: int | None = None,
                                     max_len: int | None = None,
                                     expected_results: int | None = None,
                                     result_idx: int = 0,
                                     digit_len_re: int | str = '2') -> int | None:
        '''Macro to search BTU/h information on string of manufacture models.

        Parameters:
            Model string.
            Minimum length expected to be a model string.
            Maximum length expected to be a model string.
            Expected quantity of number match on the strings.
            Index of the result on the math.
            Quantity of digits to math on the search.

        Return the BTU/h value.
        '''
        if isinstance(digit_len_re, int):
            digit_len_re = str(digit_len_re)
        if (not min_len or len(model) >= min_len) and (not max_len or len(model) <= max_len):
            if re.findall(r'[a-z]+', model, re.IGNORECASE):
                # Should be present alphabetic characters and number to caracterize a model string.
                btu_value = re.findall(r'\d{' + digit_len_re + r'}', model)
                if (btu_value and not expected_results) or len(btu_value) == expected_results:
                    return int(btu_value[result_idx]) * 1e3
        return None

    # switch_manufacture.update({'brize': lambda x: search_btu_h_on_model_string(x, 10, 10, 2, -1)})
    # switch_manufacture.update({'brastemp': lambda x: search_btu_h_on_model_string(x, 10, 10, 2, -1)})

    def carrier(x):
        if re.findall(r'\d{6}', x):
            if re.findall(r'^[a-z]+\d{3}', x, re.IGNORECASE):
                return search_btu_h_on_model_string(x, 12, 14, None, 1, 3)
            else:
                return search_btu_h_on_model_string(x, 12, 14, None, 0, 3)
        else:
            return search_btu_h_on_model_string(x, 12, 14, None, 1, 2)
    switch_manufacture.update({'carrier': carrier})
    switch_manufacture.update({'carier': switch_manufacture['carrier']})

    # switch_manufacture.update({'coldex trane': lambda x: search_btu_h_on_model_string(x, 10, 10, 2, -1)})
    switch_manufacture.update({'consul': lambda x: search_btu_h_on_model_string(x, 3, 14, None, -1)})
    switch_manufacture.update({'eletrolux': lambda x: search_btu_h_on_model_string(x, 3, None)})

    def elgin(x):
        if re.findall(r'\d{5}', x):
            if re.findall(r'^\d{2}[a-z]', x, re.IGNORECASE):
                return search_btu_h_on_model_string(x, None, None, None, 0, 5) / 1e3
            elif re.findall(r'\d0{3,}$', x):
                return int(re.findall(r'\d0{3,}$', x)[0])
            else:
                return search_btu_h_on_model_string(x, None, None, None, 0)
        elif re.findall(r'^[a-z]{3,}\d{2,}', x, re.IGNORECASE):
            return search_btu_h_on_model_string(x, None, None, None)
        else:
            return search_btu_h_on_model_string(x, 11, 13, 2, -1)
    switch_manufacture.update({'elgin': elgin})

    switch_manufacture.update({'fujitsu': lambda x: search_btu_h_on_model_string(x, 8, 10)})
    switch_manufacture.update({'gree': lambda x: search_btu_h_on_model_string(x, 3, 18, None, 0, '1,2')})
    switch_manufacture.update({'hitachi': lambda x: search_btu_h_on_model_string(x, 10, 14, None, 1)})
    switch_manufacture.update({'hi wall': lambda x: search_btu_h_on_model_string(x, None, None, None, -1, 5)})
    switch_manufacture.update({'lg': lambda x: search_btu_h_on_model_string(x, 8, 12)})
    switch_manufacture.update({'luna': lambda x: search_btu_h_on_model_string(x, 9, 11, 2, -1)})
    switch_manufacture.update({'midea': lambda x: search_btu_h_on_model_string(x, 8, 10, 2, -1)})
    # switch_manufacture.update({'miller': lambda x: search_btu_h_on_model_string(x, 10, 10, 2, -1)})
    switch_manufacture.update({'komeco': lambda x: search_btu_h_on_model_string(x, 7, 10, None, -1)})
    switch_manufacture.update({'homeco': switch_manufacture['komeco']})

    def philco(x):
        if re.findall(r'\d{5}', x):
            return search_btu_h_on_model_string(x, 10, 12, None, 0, 5) / 1e3
        else:
            return search_btu_h_on_model_string(x, 11, 14, None, 0)
    switch_manufacture.update({'philco': philco})

    # switch_manufacture.update({'raith': lambda x: search_btu_h_on_model_string(x, 10, 10, 2, -1)})
    switch_manufacture.update({'rheem': lambda x: search_btu_h_on_model_string(x, 11, 13, None, 0)})
    switch_manufacture.update({'rheen': switch_manufacture['rheem']})
    switch_manufacture.update({'samsung': lambda x: search_btu_h_on_model_string(x, 12, 14, None, 0)})
    switch_manufacture.update({'sansung': switch_manufacture['samsung']})
    switch_manufacture.update({'sansumg': switch_manufacture['samsung']})

    def springer(x):
        if re.match(r'^\d', x):
            if re.findall(r'\d{6}', x):
                return search_btu_h_on_model_string(x, 13, 14, None, 0, 3)
            elif re.findall(r'\d{4}', x):
                return search_btu_h_on_model_string(x, 7, 13, None, 0, 3)
            else:
                # return search_btu_h_on_model_string(x, 7, 11, None, 0, 3)
                return search_btu_h_on_model_string(x, 7, 11, None, 1, 2)
        else:
            if re.findall(r'[a-z]\d{3}[a-z]', x, re.IGNORECASE):
                if re.findall(r'^[a-z]{4,}', x, re.IGNORECASE):
                    return search_btu_h_on_model_string(x, 7, 9, None, 0, 2)
                else:
                    value = search_btu_h_on_model_string(x, 7, 9, None, 0, 3) / 1e1
                    if value % 1e3 == 500 and value > 12e3:
                        return value - 500
                    return value
            else:
                if len(x) >= 6:
                    value = search_btu_h_on_model_string(x, None, None, None, 0, '3,') / 1e1
                    if value % 1e3 == 500 and value > 12e3:
                        return value - 500
                    return value
                return search_btu_h_on_model_string(x, 7, 9, None, 0)
    switch_manufacture.update({'springer': springer})

    # switch_manufacture.update({'thermo': lambda x: search_btu_h_on_model_string(x, 10, 10, 2, -1)})
    switch_manufacture.update({'toshiba': lambda x: search_btu_h_on_model_string(x, 9, 12, None, 0)})
    switch_manufacture.update({'trane': lambda x: search_btu_h_on_model_string(x, 14, 16, None, 1)})
    switch_manufacture.update({'york': lambda x: search_btu_h_on_model_string(x, 10, 13, None, -1)})
    switch_manufacture.update({'yrok': switch_manufacture['york']})

    # Sometime is used a partial model name in the manufacture/company column of the database.
    c: str
    for c in re.findall(r'\w+', company):
        if not model:
            return None
        model = model.split(' ')
        # Remove text with just character (sometimes the user add descriptions or company
        # name/line here) but rejoin the space (sometimes the user add wrongs spaces).
        for idx in range(len(model) - 1, -1, -1):
            if re.match('^[a-z]+$', model[idx], re.IGNORECASE):
                del model[idx]
        if not model:
            return None
        model = ''.join(model)
        model = re.sub(r'[\-\.]', '', model)

        try:
            return switch_manufacture[c.lower()](model)
        except IndexError:
            print(f'Erro ao extrair BTU/h de Marca="{c}" e Modelo="{model}".')
            continue
        except KeyError:
            continue

    return None


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    from os import path
    from general_definitions import _

    # Use some hardcoded results to test the refrigeration power
    # extraction from the air conditioner equipment model.
    equipments = [
        ('CARRIER', '38XCA024515MC', 24e3),
        ('CARRIER', '38CCM060535MC', 60e3),
        ('carrier', '42BQAO24510HC', 24e3),
        ('CARRIER', 'Z138KC024515MC', 24e3),
        ('CONSUL', 'CBM22CBBNA', 22e3),
        ('CONSUL', 'CCF 15A', 15e3),
        ('ELETROLUX', 'PL12F', 12e3),
        ('elgin', 'SPLIT 45HVFI12B2IA', 12e3),
        ('ELGIN', 'PHQI-18000-2', 18e3),
        # ('ELGIN', 'SRF-24.000-2', 24e3),
        ('ELGIN', '45HVFE12B2IA', 12e3),
        ('ELGIN', '45PHFI600002', 60e3),
        ('ELGIN', '020899', None),
        ('ELGIN', 'PHF130000', 30e3),
        # ('ELGIN', 'SRF1 24000-2', 24e3),
        ('ELGIN', 'HEFE308B21', 30e3),
        ('FUJITSU', 'AOBA18LAL', 18e3),
        ('fujitsu', 'ABBA24LALL', 24e3),
        ('GREE', 'CJ1222LMA', 12e3),
        ('GREE', 'WC9', 9e3),
        ('GREE', 'WC18', 18e3),
        ('GREE', 'GJ-18 22 LM', 18e3),
        ('gree', 'GWC18QD-D3DNB8M/I', 18e3),
        # ('HITACHI', 'RAP050F5L', 1),
        # ('HITACHI', 'RAH 314V4', 1),
        ('LG', 'WME180FGA', 18e3),
        ('LG', 'WGM071FGA', 7e3),
        ('LG', 'TSNH1825MA1', 18e3),
        ('LG', 'USNQ242CSZ2F', 24e3),
        # ('LG', 'WGMO71FGA', 7e3),
        ('LUNA', '42MLCB18M5', 18e3),
        ('KOMECO', 'KOS30FC', 30e3),
        ('KOMECO', 'KOP60FC-C', 60e3),
        ('KOMECO', '40P48FC', 48e3),
        ('KOMECO', 'HOS 09 FC 2HX', None),
        ('MIDEA', 'CARRIER 38VFCA18M5', 18e3),
        ('MIDEA', '39VFCA1M5', None),  # Missing a number.
        ('MIDEA', '42MDCA12M5', 12e3),
        ('MIDEA ELITE', '38KCS30M5', 30e3),
        ('PHILCO', 'PAC18000IFM4', 18e3),
        ('PHILCO', 'F-70G32', None),
        ('PHILCO', 'PH18000IFM5', 18e3),
        ('SAMSUNG', 'ASV18PSBTNXAZ', 18e3),
        ('SAMSUNG', 'AR12HSSPASNXAZ Q', 12e3),
        # ('SPRINGER', 'YCA 215', None),
        ('springer', '42mca012515ms', 12e3),
        # ('springer', '42mcb1815ms', 10e3),
        ('SPRINGER', '38CQD060535MS', 60e3),
        ('SPRINGER', 'QQA075RBB', 7.5e3),
        # ('springer midea', '38kcx18s5', 18e3),
        ('springer midea', 'inverter horiz 38mbca18m5', 18e3),
        ('SPRINGER', '38KCB0225MS', 22e3),
        ('SPRINGER', 'ZIYCD305D', 30e3),
        ('SPRINGER', 'MCA-105BB', 10.5e3),
        ('SPRINGER', '38CCM060535MC', 60e3),
        ('SPRINGER', 'INNOVARE RCA075.', 7.5e3),
        # ('SPRINGER MIDIA', 'MC1125BB', 12e3),
        ('SPRINGER', 'MCA125BB', 12e3),
        ('SPRINGER', 'ZCA195BB', 19e3),
        # ('springer', 'zcl18588', 18e3),
        ('TOSHIBA', 'MMC-APO181H', 18e3),
        ('TOSHIBA', 'MMCAPO361H', 36e3),
        ('TRANE', '2MCW0518G1000AA', 18e3),
        ('YORK', 'HLDA18TSADAB', 18e3),
        ('york', 'yds71tc16ia', 16e3),
        ('YORK', 'YDS28GC16IA', 16e3),
        ('york', 'yds71tc16ia', 16e3),
        ('york', 'HLDA24FSADA-B', 24e3),
    ]
    for idx, (manufacture, model, expected) in enumerate(equipments):
        evaluated = get_refrigeration_power_from_model(manufacture, model)
        if evaluated != expected:
            print(get_refrigeration_power_from_model(manufacture, model))  # NOTE: debug exactly here.
        assert evaluated == expected, f'Error on {idx} item: {manufacture} {model}: "{evaluated}" should be "{expected}".'

    # Use the test case pf the `test_airconditioners.csv` to extract
    # refrigeration power from the air conditioner equipment model.
    db = pd.read_csv(path.join('Test_data', 'test_airconditioners.csv'))

    if not pd.isnull(db).all().all():
        for idx, db_row in db.iterrows():
            manufacture = db_row.get('Marca')
            model = db_row.get('Modelo')
            expected = db_row.get('Potência frigorífica (BTU/h)')
            if (pd.isnull(manufacture) and pd.isnull(model)
                    and model != '--não consta--'
                    and db_row['Tipo'] in (_('condensador'), _('evaporador'), _('split'), _('janela'))):
                evaluated = get_refrigeration_power_from_model(manufacture, model)
                if evaluated != expected:
                    print(get_refrigeration_power_from_model(manufacture, model))  # NOTE: debug exactly here.
                assert evaluated == expected, f'Error on {idx} item: {manufacture} {model}: "{evaluated}" should be "{expected}".'
