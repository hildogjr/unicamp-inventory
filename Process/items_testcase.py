#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Test case for validate the search function of all items_*.py modules.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['test_all', 'test_search_fncs', 'test_match', 'test_not_match']

# Base libraries.
import os
import re
import glob
# import unittest
# from parameterized import parameterized, param, parameterized_class

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
from general_definitions import COLUMN_NAME_MATCH


# File global definitions.
TEST_CASES_FOLDER = 'Test_data'  # Path to the test cases folder.


def test_all(file_name_pattern: str,
             items_definitions: dict):
    '''Make the complete test of identification library.

    Parameters:
        file_name: File name of call the test or pattern of the
                   CSV file with the test examples.
        items_definitions: Items references.

    Usage on each item module:
    >>> from items_testcase import test_all
    >>> test_all(__file__, items_definitions)
    '''
    # Configure the patterns of file names and to search into the examples.
    if file_name_pattern[-3:] == '.py':
        file_name_pattern = re.findall(r'.*_(.*).py', file_name_pattern)[0]
    print('Testando item do tipo "%s"...' % file_name_pattern)

    # Join the all sub categories of match.
    desc_match = '('
    desc_not_match = '('
    for sub_item_definitions in items_definitions.values():
        desc_match += sub_item_definitions['desc_match'][3:-1] + r'|'
        desc_not_match += sub_item_definitions['desc_not_match'][3:-1] + r'|'
    desc_match = desc_match[:-1] + r')'
    desc_not_match = desc_not_match[:-1] + r')'
    item_handles = items_definitions[list(items_definitions.keys())[0]]
    search_fncs_references = item_handles['search']

    # Define the files and parameters to test bench.
    files_match = os.path.join(TEST_CASES_FOLDER, 'test_' + file_name_pattern + '.csv')
    files_not_match = os.path.join(TEST_CASES_FOLDER, 'test_not_' + file_name_pattern + '.csv')
    # Add the file example of matching of the other items as
    # not match for this current one. This add more examples
    # and provide better validation.
    # It necessary to remove the example test match of the
    # current item category from the search by `glob`.
    file_match_other = glob.glob(os.path.join(TEST_CASES_FOLDER, 'test_*.csv'))
    # file_match_other = [f for f in file_match_other
    #                    if not re.search('test_not_(?!%s))\.csv' % file_name_pattern, f)]
    file_match_other = [f for f in file_match_other if not re.search(r'test_not_.+\.csv', f)]
    if file_match_other and file_match_other.__contains__(files_match):
        file_match_other.remove(files_match)
    files_not_match = [files_not_match] + file_match_other

    # Check the no-match patterns (use example that was wrong identified before).
    test_not_match(desc_match, desc_not_match, files_not_match)
    # Check the match patterns with a pre-defined list.
    test_match(desc_match, files_match)
    # Check parameters extraction.
    test_search_fncs(search_fncs_references, files_match)


def test_match(re_pattern: str,
               file_name: list[str] | str):
    '''Test the match description of this kind of item.

    Parameters:
        re_pattern: Regular expression to match.
        file_name: CSV file name with the test examples.
    '''
    # Check if is passed a example file or a list of files.
    if isinstance(file_name, list):
        for f in file_name:
            test_match(re_pattern, f)
        return
    # Teste if the example test file exist and read it.
    if not os.path.isfile(file_name):
        file_name = os.path.join(TEST_CASES_FOLDER, file_name)
        if not os.path.isfile(file_name):
            return
    db = pd.read_csv(file_name)

    ''' Test the output of the search functions that have an
    expected value written on the test file database (use as
    test cases).
    '''
    if pd.isnull(db).all().all():
        return
    print('\tItems a INCLUIR no arquivo "%s"...' % file_name)
    for _, db_row in db.iterrows():
        match_result = re.search(re_pattern, db_row['Descrição'], re.IGNORECASE)
        if not match_result:
            print('%i : %s -> %s' %  # NOTE DEGUB EXACTLY HERE!!!
                  (db_row['Identificador'], str(match_result), db_row['Descrição']))
        # Check the match track column if it exist and have valid value.
        # if COLUMN_NAME_MATCH in db.columns:
        #    match = db_row[COLUMN_NAME_MATCH]
        #    if not pd.isnull(match) and match.lower() != match_result.lower():
        #        print('%s? (%s) -> %s' % (match_result.lower(), match.lower(), db_row['Descrição']))
        assert match_result, \
            'Não identificado (%i - %s) "%s' % \
            (db_row['Identificador'], db_row['Área de Patrimônio'].upper(), db_row['Descrição'])


def test_not_match(re_pattern: str,
                   re_not_pattern: str,
                   file_name: list[str] | str):
    '''Test the no match description of this kind of item.

    Parameters:
        re_pattern: Regular expression of match.
        re_not_pattern: Regular expression of not match.
        file_name: CSV file name with the test examples.
    '''
    # Check if is passed a example file or a list of files.
    if isinstance(file_name, list):
        for f in file_name:
            test_not_match(re_pattern, re_not_pattern, f)
        return
    # Teste if the example test file exist and read it.
    if not os.path.isfile(file_name):
        file_name = os.path.join(TEST_CASES_FOLDER, file_name)
        if not os.path.isfile(file_name):
            return
    db = pd.read_csv(file_name)

    ''' Test the output of the search functions that have an
    expected value written on the test file database (use as
    test cases).
    '''
    if db.isnull().all().all():
        return
    print('\tItems a EXCLUIR no arquivo "%s"...' % file_name)
    for _, db_row in db.iterrows():
        match_result = re.search(re_pattern, db_row['Descrição'], re.IGNORECASE)
        not_match_result = re.search(re_not_pattern, db_row['Descrição'], re.IGNORECASE)
        if match_result and not not_match_result:
            print('%i : %s %s -> %s' %  # NOTE DEBUG EXACTLY HERE!!!
                  (db_row['Identificador'], str(match_result),
                   str(not_match_result), db_row['Descrição']))
        assert not match_result or not_match_result, \
            'Identificado erroneamente (%i - %s) "%s' % \
            (db_row['Identificador'], db_row['Área de Patrimônio'].upper(), db_row['Descrição'])


def test_search_fncs(search_fncs_references: dict,
                     file_name: list[str] | str):
    '''Test the return of the search function by examples.

    Parameters:
        search_fncs_references: Callable functions for search data.
        file_name: CSV file name with the test examples.
    '''
    # Check if is passed a example file or a list of files.
    if isinstance(file_name, list):
        for f in file_name:
            test_search_fncs(search_fncs_references, f)
        return
    if not os.path.isfile(file_name):
        file_name = os.path.join(TEST_CASES_FOLDER, file_name)
        if not os.path.isfile(file_name):
            return
    db = pd.read_csv(file_name)

    ''' Test the output of the search functions that have an
    expected value written on the test file database (use as
    test cases).
    '''
    if pd.isnull(db).all().all():
        return
    print('\tItems a OBTER PARÂMETROS no arquivo "%s"...' % file_name)
    for statistic_name in [n for n in db.columns
                           if n in search_fncs_references.keys()]:
        # Bypass some columns that may be present at the test case
        # database but are direct copy of the original database.
        # They  are present here just to be used in other statistics.
        if statistic_name.lower() in ['data de compra', ]:
            continue

        for idx, db_row in db.iterrows():
            value_expected = db_row[statistic_name]
            if pd.isnull(value_expected):
                continue  # If not expected value assigned.
            search_fnc = search_fncs_references[statistic_name]
            value = search_fnc(idx, '', db_row, {'Tipo': db_row['Tipo']})
            if not value and not pd.isnull(value_expected):
                print(search_fnc(idx, '', db_row))
            # Convert the `value_expected` to same type as `value`,
            # this is necessary in case that the expected value is
            # read just as string by `pandas` library. It happen
            # on `'Potência frigorífica (BTU/h)'` type.
            # if value:
            output_type = type(value)
            value_expected = output_type(value_expected)  # Dynamic convert type.
            if isinstance(value_expected, str) and isinstance(value, str):
                value = value.lower()
                value_expected = value_expected.lower()
            if value != value_expected:
                print('%s %i : "%s"? (Esperado "%s") -> %s' %
                      (statistic_name, db_row['Identificador'], value, value_expected, db_row['Descrição']))
                print(search_fnc(idx, '', db_row))  # NOTE DEBUG EXACTLY HERE!!!
            assert value == value_expected, (
                f'Valor errado de "{statistic_name}" (item {idx}):'
                f' ({db_row["Identificador"]} - {db_row["Área de Patrimônio"].upper()}):'
                f' {db_row["Descrição"]}\n'
                f'Esperado: {value_expected}\nEncontrado: {value}\n{db_row}')


# %%
# Entry point, running all the tests:
# Execute Python calling each library that heritage this file.
if __name__ == "__main__":
    os.system('python ' + os.path.join('Process', 'items_*.py'))
