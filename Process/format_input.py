#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas classified information.
#
# Format definitions for read files and other inputs.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import os
import re
from sys import stderr

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
from general_definitions import GENERAL_SEP, configs
# from format_equip_data import read_equipment_database


'''
# Map the patrimony spreadsheet file header
# (just for documentation, not used into the code).
header_map = {
    'A': 'Unidade',
    'B': 'Propriedade',  # Bem próprio ou não.
    'C': 'ID', 'D': 'ID antigo', 'E': 'Descrição',
    'F': 'Marca', 'G': 'Modelo', 'H': 'Série',
    'I': 'Situação', 'J': 'Data da situação',  # Bem ativo ou não.
    'K': 'Estado do bem',  # Em uso ou não.
    'L': 'Categoria', 'M': 'Conta SIAFEM', 'N': 'Conta SIAFEM 2',
    'O': 'Grupo',
    'P': 'Modalidade',  # Se foi compra ou doação ou comodata.
    'Q': 'Processo',  # Processo UNICAMP para patrimoniamento.
    # Número do processo do Sistema de compras da BEC.
    'R': 'NE', 'S': 'AF', 'T': 'Convênio',
    'U': 'Termo', 'V': 'Auxílio',
    'W': 'Doc. fornecedor', 'X': 'Fornecedor',
    'Y': 'Doc. cedente', 'Z': 'Cedente',
    'AA': 'Doc. beneficiário', 'AB': 'Beneficiário',
    'AC': 'Fabricante', 'AD': 'Tipo de NF', 'AE': 'Número da NF',
    'AF': 'Data de compra', 'AG': 'Data de incorporação',
    'AH': 'Vigência da garantia',
    'AI': 'Moeda de compra', 'AJ': 'Valor do bem', 'AK': 'Valor corrigido',
    'AL': 'Número do imóvel', 'AM': 'Órgão', 'AN': 'Imóvel',
    'AO': 'Local 1', 'AP': 'Local 2', 'AQ': 'Local 3',
    'AR': 'Local 4', 'AS': 'Local 5',
    'AT': 'Complemento de endereço',
    'AU': 'Situação contábil',
    'AV': 'Fim de cessão', 'AW': 'Fim de comodato',
    'AX': 'Motivo de baixa', 'AY': 'Data da baixa',
    'AZ': 'Valor da baixa', 'BA': 'Processo da baixa',
    'BB': 'Depreciação', 'BC': 'Vida útil',
    'BD': r'% residual', 'BE': 'Valor residual',
    'BF': 'Depreciação acumulada', 'BG': 'Saldo contábil'
}
'''


def read_database(file_name: str) -> tuple[pd.DataFrame | None, list | None]:
    '''Specific function to read the UNICAMP database in all it
    variations of used libraries to export.
    It also take into account initial and final line/rows with
    general information that do not belong to the database it self.

    Returns:
        Database pandas.DataFrame() object.
        Set of units identified into the database.
    '''
    if not os.path.isfile(file_name):
        return None, None
    if os.path.splitext(file_name)[-1].lower() in ('.xls', '.xlsx', '.ods'):

        with pd.ExcelFile(file_name) as excel_file_handle:
            # Infer each UNICAMP system export the data.
            sheet_to_read = excel_file_handle.sheet_names
            if len(sheet_to_read) == 1:
                # The file was possible exported by a system that have
                # one unit/school/institute by spreadsheet. This was
                # the system used by Lindinalva on my first development.
                sheet_to_read = sheet_to_read[0]
                row_to_skip = []  # Skip no row.
                footer_to_skip = 0  # No footer to skip.
            elif sheet_to_read == ['Filtros Utilizados', 'Dados']:
                # The spreadsheet data was exported by
                # https://www.siad.unicamp.br/dadosger. This system
                # export a double sheet spreadsheet: first with not
                # useful information here and second with the data,
                # 2 lines header and a final liner (footer) unuseful
                # totalization.
                sheet_to_read = 'Dados'
                row_to_skip = [0, 1]  # First two rows.
                footer_to_skip = 1  # Skip last row with "totalization".

            # excel_file_handle.XlrdReader.get_sheet_data(excel_file_handle, sheet=sheet_to_read, convert_float=True)
            # Force some data conversion.
            # Force identification stays as string until the bellow conversion.
            # columns_type_conversion = {'Identificador': str, }
            # Read the data second the above standards.
            database = pd.read_excel(excel_file_handle, sheet_name=sheet_to_read,
                                     skiprows=row_to_skip, skipfooter=footer_to_skip,
                                     dtype=str)  # Force all columns to not be interpreted.
            # converters=columns_type_conversion)

            # Pandas doesn't allow to force engine='xlrd' for .xlsx files, it
            # is reserved to be just used with .xls (old format).
            '''
            db = pd.read_excel(file_name, sheet_name=None, engine='xlrd', skiprows=row_to_skip)
            if isinstance(db, dict):
                if len(db) == 1:
                    db = db[list(db.keys())[0]]
                elif list(db.keys()) == ['Filtros Utilizados', 'Dados']:
                    db = db['Dados']
            if db.empty:
                # Despite the file extension, try a older codification
                # to read, because such file may be exported by a too
                # older library.
                # NOTE: This is exactly the case of the new patrimony
                # system that the IT give me access. The exportation
                # before used did not need of this procedure.
                # The xlrd engine became decrepit for pandas>=1.2, using
                # by default openpyxl to read .xlsx files.
                db = pd.read_excel(file_name, engine='xlrd', sheet_name=None)
            '''
            # This bellow code in included as `skipefooter` directive/value.
            '''
            # Remove trash last lines.
            drop_last_line = True
            num_columns = len(db.columns)
            while drop_last_line:
                last_index = len(db) -1
                drop_last_line = False
                count_NaN = 0
                for _, v in db.iloc[-1].iteritems():
                    if pd.isna(v[-1]):
                        count_NaN += 1
                if count_NaN / num_columns > 0.92:
                    drop_last_line = True
                if drop_last_line:
                    db.drop(last_index, inplace=True)
            '''
    elif os.path.splitext(file_name)[-1].lower() == '.csv':
        database = pd.read_csv(file_name, sep=';', dtype=str)  # Force all columns to not be interpreted.
    else:
        return None, None

    def uniformize_date(value):
        # Remove the time if present (some exportation of UNICAMP
        # database includes unuseful 0h0min0s time).
        try:
            if isinstance(value, str):
                value = pd.to_datetime(value)
            # pd.to_datetime(df['DateTime']).dt.date
            result = pd.to_datetime(value).date()
            return result
        except Exception:
            return None

    def uniformize_unit_name(value: str) -> str:
        # Uniformize the unit/schools/institute names.
        # Remove some bad characters such "/" that may be not allowed on
        # some file names on the internal intermediary same process.
        # May be also typing errors on the acronyms that create false
        # different values, I can do nothing about, it MUST be fix on
        # the database.
        return value.strip().upper().replace(r'[:\\/\?*\[\]_]', GENERAL_SEP)

    def uniformize_id_number(value: str) -> int | str:
        # The new identification ID are simple integer numbers.
        # The old exportation (used by Lindinalva with different spreadsheet
        # to each unit) gave this correct. In the new one, it is converted to
        # string just to force a dot thousand separator, instead of configure
        # the numeric format of the spreadsheet.
        try:
            return int(re.sub(r'[\,\.\s]', '', value))
        except Exception:
            return value

    def uniformize_currency(value: str) -> float | str:
        # Convert some values for better software understanding.
        # The developers of UNICAMP DB export software was not concerned about
        # the correct variable type but about how it will be printed. Basically
        # they used a virtual correct visualization by forcing the conversion
        # of all value to string instead of format the column of the exported
        # spreadsheet.
        try:
            return float(value.replace('.', '').replace(',', '.'))
        except Exception:
            # Some columns in the designed string into the name may
            # be a not numeric one.
            return value

    def convert_to_int(value: str) -> int | str:
        try:
            return int(value.replace('.', ''))
        except Exception:
            return value

    def remove_spaces(value: str) -> str:
        try:
            return value.replace(' ', '')
        except Exception:
            return value

    # Rename the columns name.
    if 'configs' in globals():
        if columns_name := configs['identify'].get('data columns'):
            database.rename(columns={v: k for k, v in columns_name.items()}, inplace=True)

    # Apply the standardization using the internal Pandas iteration system
    # control instead of look/iterate through each row.
    database['Área de Patrimônio'] = database['Área de Patrimônio'].apply(uniformize_unit_name)
    database['Identificador'] = database['Identificador'].apply(uniformize_id_number)
    database['Imóvel'] = database['Imóvel'].apply(convert_to_int)
    database['Órgão'] = database['Órgão'].apply(remove_spaces)
    column_name: str
    for column_name in database.keys():  # Same as `db.columns`.
        if any(n in column_name.lower() for n in ('valor', 'saldo', )):
            database[column_name] = database[column_name].apply(uniformize_currency)
        if 'data' in column_name.lower():
            database[column_name] = database[column_name].apply(uniformize_date)
        if any(n in column_name.lower() for n in ('área', 'tipo', 'situação', 'faixa',
                                                  'grupo', 'material', 'classe', 'órgão',
                                                  'moeda', 'siaf')):
            database[column_name] = database[column_name].astype('category')

    # Get some general statistics.
    units = set(database['Área de Patrimônio'])
    print('\tIdentificado %i unidades:' % len(units), sorted(units))
    print('\tCarregado %i items.' % len(database))
    # print('\tColunas disponíveis:', database.keys())
    stderr.write(f'Memory used by loaded DB {database.memory_usage(deep=True)}')

    return database, units


# The function `read_equipment_database(file_name: str)` was moved to
# `format_equip_data.py` to generate a general processing file
# for external studies.


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    import glob
    if os.name == 'nt':
        # On Windows is needed to specify the current path to search, appear
        # that the current project path is not inserted into the Python path.
        base_path = os.path.split(__file__)[0]
        db_files = glob.glob1(base_path, '*.xlsx')
        if not db_files:
            base_path = os.path.join(base_path, 'Databases')
            db_files = glob.glob1(base_path, '*.xlsx')
        db_file = os.path.join(base_path, db_files[0])
    else:
        db_files = glob.glob(os.path.join('Databases', '*.xlsx')) or glob.glob('*.xlsx')
        db_file = db_files[0]

    print('Lendo "%s"...' % db_file)
    read_database(db_file)
