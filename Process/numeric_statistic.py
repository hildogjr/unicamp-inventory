#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide functions for specific numeric statistics used for more
# than one equipment type.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from datetime import datetime
from dateutil.relativedelta import relativedelta
from collections import Counter

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Specific application libraries.
from general_definitions import UNIDENTIFIED_TYPE, print
from analysis_chart import analyze_percentage_chart, data_to_chart

date_now = datetime.now()


def count_purchases(data: pd.DataFrame,
                    column: str,
                    years_count_rules: list[int] = [10, 5, 1],
                    *args, **kwargs) -> dict:
    '''Execute `analyze_percentage_chart` to plot and analyze percentage groups
    list creating a pie chart and ADD special count statistics by year.

    Parameters:
        data (ps.DataFrame): Database.
        column (str): Specific column of the database with the data to
                      analyze. Other columns may be used into the analysis.
        years_count_rules (list of int): Path and file name to save the chart
                         (for future use into the report).
        Other inputs of `analyze_percentage_chart` function.

    Return statistics in a dictionary with the output of `analyze_percentage_chart`
           added with the .
    '''
    # Get the base statistics and chart for the other function.
    statistics = analyze_percentage_chart(data=data, column=column, *args, **kwargs)

    # Make sure to start the count by the bigger period.
    years_count_rules = sorted(years_count_rules, reverse=True)

    # Evaluate the specific period count logic.
    for count_rule in years_count_rules:
        confront_data = date_now - relativedelta(years=count_rule)
        data_to_analyze = data[data['Data de compra'] > confront_data]

        data_to_analyze, _ = data_to_chart(data_to_analyze[column])

        qty_elem_rule = len(data_to_analyze)
        text = (f'{count_rule} últimos anos' if count_rule > 1 else 'Último ano')
        statistics.update({f'{text}': qty_elem_rule})
        if qty_elem_rule > 0:
            # Doesn't add this statistic if no element is present
            most_frequent = Counter(data_to_analyze)
            most_frequent = sorted(most_frequent,
                                   key=most_frequent.__getitem__,
                                   reverse=True)
            if most_frequent[0] == UNIDENTIFIED_TYPE and len(most_frequent) >= 2:
                most_frequent = most_frequent[1]
            else:
                most_frequent = most_frequent[0]
            text = ('anos' if count_rule > 1 else 'ano')
            statistics.update({f'Frequente ({count_rule} {text})': most_frequent})
        else:
            break  # Stop calculation if 0 reached on some last period.

    return statistics


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    pass
