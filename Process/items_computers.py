#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide specific definitions for searh and scrape
# computers ("computadores").
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import re

# Required but not default libraries.
# import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# from numba import jit

# Internal libraries.
from items_general_info import *
from analysis_chart import *
from general_definitions import UNIDENTIFIED_TYPE, _, print
from numeric_statistic import count_purchases


# %%
# Functions to get data from the original databases.

def get_computer_type(item_idx: int,
                      category: str,
                      row: pd.DataFrame,
                      found_statistics: dict[str, Any] = {},
                      column_search: str = 'Descrição') -> str:
    '''Get the computer type.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled in 'Modelo'.

    Return the computer type (str): desktop, notebook, servidor,
        all-in-one, estação de trabalho...
    '''

    def is_match_type(pattern: str,
                     column_to_search: list[str] = [column_search]) -> bool:
        '''Get the type on the description.

        Tip: Always check the most specific category before the
        most generic one, because the first one may have the
        second word/noun as part of it description.

        Parameters:
            Pattern (str) in regular expression to search.
            Columns to search.

        Return (bool).'''
        match = None
        for c in column_to_search:
            match = match or re.search(pattern, row[c], re.IGNORECASE)
            if match:
                break
        return match

    # Order for search here is important, it must check first
    # the less generic words because the groups formed by
    # such words may contain the generic words.

    # If start with "monitor", it is for sure. In other cases
    # the word "monitor" can stay in the middle and/or used
    # together with a computer description.
    if is_match_type(r'(?:^monitor(?![a-z]))'):
        return _('monitor')

    if is_match_type(r'((\s|^)ipa[dq](\s|$))', [column_search, 'Modelo', 'Marca']):
        return _('tablet ipad')
    if is_match_type(r'(tablet|de +m[ãa\?]o)'):
        return _('tablet')
    if is_match_type(r'(app*l+e)') or \
            is_match_type(r'((^|\s)i*mac(\s*book)*)', [column_search, 'Modelo', 'Marca']):
        return _('computador Apple')
    if is_match_type(r'(palmtop)'):
        return _('palmtop')
    if is_match_type(r'(notebo[oc]k|netbo[oc]k|ultrabo[oc]k|laptop|port[aá\?]*[tr]il)'):
        # and not is_match_type(r'(monitor)'):
        return _('notebook')
    if is_match_type(r'(servidor)'):
        return _('servidor')
    if is_match_type(r'(all[ \-]*in[ \-]*[oi]ne|(^|\s)aio(?![a-z]))', [column_search, 'Modelo']):
        return _('all-in-one')
    if is_match_type(r'(esta[cç\?][aã\?]o[ \-]de[ \-]trabalho|workstation)'):
        # and not is_match_type(r'(desktop)'):
        return _('estação de trabalho')
    # More generic categories must be the last match tests,
    # because possible all before match with the bellow words.

    # Check first "televisão" because some are described as
    # "televisão com conexão p/ PC".
    if is_match_type(r'(televis(or|[ãa\?]o))'):
        return _('monitor')
    if is_match_type(r'((micro[ \-]*)*((co)*mputador|processador)|desktop'
                    r'|(\s|^)cpu(\s|$)|(\s|^)pc(\s|$))'):
        return _('desktop')
    if is_match_type(r'(monitor)'):
        return _('monitor')

    print(text_warning_missing(row, 'Tipo', category))
    return UNIDENTIFIED_TYPE


# %%
# References to functions for scrape data from the original databases
# and statistics and chart analysis.

# Scrape description for the property description item.
# The main key is 'category name (sub category name as optional)'.

# Base to ignore.
base_ignore = (
    r'(?:aparelho|sistema|no[ \-]*break|switch|sca(?:nn|n)er|placa'
    r'|split(?:t*er)*|seletor|distribuidor|replicador|chaveador'
    r'|projetor|interface|c[aâ\?]mera|temperatura|(?:caixa )*ac[úu\?]stica|instrumento'
    r'|anali[zs]ador|detec*tor|amplificador'
    r'|caixa de som|filmadora|terminal|anal[oó\?]gic[oa]|refletor'
    r'|espectro(?:metro|fot[oô]metro)|agregometro|densitom[eê]tro'
    r'|desfibrilador'
    r'|microsc[oó\?]pio|balan[cç\?]a|freezer|registrador|oxim[ée\?]tro|co[2²]|cardiosc[óo\?]pio'
    r'|cadeira'
    r')'
)

# Specific computers/notebooks/tablets identification.
_category_name = 'computadores'
items_definitions.update({
    _category_name + ' (geral)': {
        # This search partners will be saved as columns at the
        # final spreadsheet file. The order specified here may
        # not affect the columns order of the spreadsheet, it
        # must be set on the save routine.
        'desc_match': r'(?:(?:micro[ \-]*)*computador|(?:\s|^)cpu(\s|$)|(?:\s|^)pc(?:\s|$)'
                      r'|servidor(?![a-z])|workstation'  # |esta[cç\?][aã\?]o de trabalho'
                      r'|notebook|netbook|ultrabook|laptop'
                      r'|tablet|(?:\s|^)ipad(?:\s|$)|palmtop)',
        'desc_not_match': base_ignore[:-1]
                          + r'|computadorizad[oa]|anali[zs]ador|detec*tor|amplificador'  # noqa
                          r'|caixa de som|filmadora|terminal|anal[oó\?]gic[oa]|refletor'  # noqa
                          # Used by accessories and models names.
                          r'|(?:de|via|com) +PC|PC *\d| \d+ *PC'
                          # Some specific items with "PC".
                          r'|(?:^|\s)PCR(?:\s|$)|compressor|impressora|roteador|oscilosc[óo\?]pio|externo'
                          # Exclude all "estação de trabalho" they will be deal in the above subcategory.
                          r'|esta[cç\?][aã\?]o de trabalho'
                          # Exclude some other equipments.
                          r'|servidor de (?:cam[ea]ra)|receptor'
                          r'|espectro(?:metro|fot[oô]metro)|agregometro|densitom[eê]tro'
                          r'|desfibrilador|medi[çc\?][ãa]o|aquisi[çc\?][ãa]o'
                          r'|press*[aã\?]o|bicicleta(?:\s+ergom[ée\?]trica)*|piano'
                          r'|microsc[oó\?]pio|balan[cç\?]a|freezer|registrador|oxim[ée\?]tro|co[2²]|cardiosc[óo\?]pio'
                          r'|lousa|bomba|equipamento|m[áa\?]quina|fluxo|motor|rotor|rel[oó\?]gio'
                          r'|(?:video)*game|console'
                          r'|roteador|ponto de acesso|(?:^|\s)rfid(?![a-z])'
                          r')',
        # List of groups that may belongs to this category. They
        # are used for double match and report inconsistences
        # in the original database procedure (as already reported).
        'groups_check': [86, 71, 67, 60],
        # References to functions of scrap data.
        'search': {
            'Tipo': get_computer_type,
            'Data de compra': get_purchase_data,
            'Localização': get_location,
            'Aquisição': get_incorporation_method,
        },
        # References to functions for statistic and chart analysis.
        'analysis': {
            # "Tipo" must be the first analysis because it result may be used into other charts.
            'Tipo': analyze_percentage_chart,
            'Data de compra': analyze_discrete_chart,
            'Aquisição': count_purchases,  # analyze_percentage_chart,
            # 'Saldo Contábil': analyze_scatter_chart,
        }
    }
})
# Exclude accessories.
items_definitions[_category_name + ' (geral)']['desc_not_match'] = (
    items_definitions[_category_name + ' (geral)']['desc_not_match'][:-1]
    # Prefixes used to exclude the item from this subcategory.
    + r'|(?:por|para|p\/|como*|c\/|ao|conecta|d[eoa]) *(?:\:|a*o|um)* *'
    + items_definitions[_category_name + ' (geral)']['desc_match']
    # Suffixes used to exclude the item from this subcategory.
    + r'|' + items_definitions[_category_name + ' (geral)']['desc_match']
    + r' *(?:based|com|c\/)'
    + r')'
)

# "estação de trabalho" is a description used com computer, table and
# so much other things. It is need specific rules for consider just
# that one that is a computer and exclude for sure table and others.
items_definitions[_category_name + ' (estação de trabalho)'] = \
    items_definitions[_category_name + ' (geral)'].copy()
items_definitions[_category_name + ' (estação de trabalho)']['desc_match'] = \
    r'(?:esta[cç\?][aã\?]o de trabalho)'
# Use all above rules plus some specific to not consider tables and others.
items_definitions[_category_name + ' (estação de trabalho)']['desc_not_match'] = (
    base_ignore[:-1]
    + r')'
)

# Some specific rules to "Monitor". It is a generic word for specify
# any equipment that registry or keep watching some parameter.
items_definitions[_category_name + ' (monitor)'] = \
    items_definitions[_category_name + ' (geral)'].copy()
items_definitions[_category_name + ' (monitor)']['desc_match'] = \
    r'(?:monitor(?![a-z]))'
# Use all above rules plus some specific to not consider physiologic
# and environmental monitors.
items_definitions[_category_name + ' (monitor)']['desc_not_match'] = (
    base_ignore[:-1]
    # Generic monitor and displays.
    + r'monitor.+(?:atividade|t[ée\?]rmic[ao]|radia[çc\?][ãa\?]o|ambiental'
    + r'|\sUV(?![a-z])|\sAR(?![a-z])|chuva'
    + r'|gases|oxig[êe\?]nio|nitrog[eê\?]nio'
    + r'|fisiol[óo\?]gico|press*[aã\?]o|fetal|card[ií\?]ac[oa]|corporal'
    + r'|metaboli(?:smo|co)|ambulatorial'
    + r'|(?:multi )*par[aâ\?]men*tr(?:o|ico)|\sECG|freq[uü\?][eê\?]ncia|coagula[çc\?][ãa\?]o'
    + r'|n[íi\?]vel|sensor|controle|ponto|for[cç\?]a'
    + r'|acess*[óo\?]rio'
    + r')'
    # Other equipments to remove.
    + r'|injetora|cardiac|simulador|medidor|aquecedor'
    # Prefixes used to exclude the item from this subcategory.
    + r'|(?:para|p\/|com|c\/|contendo|) *'
    + items_definitions[_category_name + ' (monitor)']['desc_match']
    + r')'
)

# TODO Colocar "STORAGE DE ARMAZENAMENTO DE DADOS 14.4TB SAS COMPOSTO DE   DOIS MODULOS.
# MARCA HP. MODELO P4500 G2. COM MEM. 12 GB. 24 HD 600 GB" item 612852 do IQ?


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    from items_testcase import test_all
    test_all(__file__, items_definitions)
