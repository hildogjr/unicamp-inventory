#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas NOT classified
# information, it is covered by Federal Brazilian Law 12527
# from 18/Nov./2011 (public data access):
# http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm.
#
# Do the statistics analysis using the identified items and save
# the results in a new file/database with sheet for each item
# category.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

# %%
from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Callable
import os
import glob
from datetime import datetime
import fnmatch
# from functools import partial, reduce

# Required but not default libraries.
# import xlsxwriter as xlsx
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
from general_definitions import (path_output, file_name_analysis, path_temporary,
                                 configs, get_units_group,
                                 GENERAL_SEP, SPREADSHEET_PAGE_INFO_NAME,
                                 GROUPS_NAME_LIST, FILE_NAME_DB)
from items_definitions import (items_definitions, items_category)
from classification_definitions import classification_definitions
from format_output import (spreadsheet_adjust_columns, spreadsheet_write_info,
                           spreadsheet_format_columns, compose_chart_file_name,
                           STATISTIC_SUBNAMES)
from db_utils import merge_units
from format_equip_data import read_equipment_database

# Definitions of shortcut methods.


def temporary_path(f): return os.path.join(path_temporary, f)  # noqa


print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Análise realizada em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)


# %%
# Read each XLSX file data base in the folder/subfolder.

# Find all files with the before items identification.
path_db_file_names = glob.glob(os.path.join(path_output, '*.xlsx'))
path_db_file_names += glob.glob(os.path.join(path_output, '*.ods'))
general_report_db_file_name_pattern = os.path.splitext(file_name_analysis)[0] + '.*'
for idx in reversed(range(len(path_db_file_names))):
    if (fnmatch.fnmatch(path_db_file_names[idx], os.path.join(path_output, FILE_NAME_DB))
            or fnmatch.fnmatch(path_db_file_names[idx], general_report_db_file_name_pattern)):
        # Remove also the own output file of this script.
        del path_db_file_names[idx]

if not path_db_file_names:
    exit('Nenhum pré-processamento de identificação salvo.')

evaluated_statistics: dict[str, dict[str, dict[str, str | float | int]]] = {}
count_items: int = 0
for count_db_load, db_file in enumerate(path_db_file_names, 1):
    # Use the bellow bypass to run the code with less
    # databases. This is used for better quick debug.
    # if os.path.splitext(db_file_name)[0].lower() not in ['ar condicionados', ]:
    #    continue

    # Load all spreadsheets/pages form the database/Excel file.
    # Do not use the result file if it was already created.
    db_file = os.path.abspath(db_file)
    if os.path.normpath(db_file) == os.path.normpath(os.path.abspath(file_name_analysis)):
        print(f'Arquivo "{db_file}" ({count_db_load}/{len(path_db_file_names)}) ignorado.')
        continue
    item_name: str = os.path.splitext(os.path.split(db_file)[1])[0]
    print(f'Carregando dados sobre "{item_name}" ({count_db_load}/{len(path_db_file_names)})... ', end='')
    database, _ = read_equipment_database(db_file)
    print(' carregado')

    # Start the output report.
    print(f'Gerando análise sobre "{item_name}" ({count_db_load}/{len(path_db_file_names)})... ')

    # print(database)
    # print('Informações disponíveis para análise:', list(database[list(database.keys())[0]].columns))
    evaluated_statistics[item_name] = {}

    # General function logic to evaluate statistics used by single unit/school
    # and by the group of those as a common code.
    def evaluate_statistic_chart(unit_name: str,
                                 database: dict | pd.DataFrame,
                                 analysis_name: str,
                                 analysis_fnc: Callable[[pd.DataFrame, str, str | None, str | None],
                                                        dict[str, float | int | str]]):
        '''Evaluate the statistics (and substatistics) and generate charts for the
        data of a unit/school or a group second the definition given by `analysis_func`.

        Parameters:
            Unit/school or group name.
            Callable function to evaluate the statistic (and generate charts).
            Name of the statistic.

        Return the statistic and substatistics of the unit/school or group by the
            `analysis_fnc` definition.
        '''
        # In case of unit/school evaluation loop is passed a dict of DataFrames.
        if isinstance(database, dict):
            database = database[unit_name]
        # If the statistic evaluation is not defined of there is no correlated data
        # to be used in the database, is not possible to evaluate any calculation.
        if analysis_fnc is None or (analysis_name not in database.keys()):
            return  # In case of not implemented yet or present on data.
        if unit_name not in evaluated_statistics[item_name].keys():
            evaluated_statistics[item_name][unit_name] = {}
            # The bellow statistic is also present into the "`'Tipo'`" to better report format.
            evaluated_statistics[item_name][unit_name][STATISTIC_SUBNAMES['total']] = len(database)
            if len(database) == 0:
                print(' NENHUM item.')
            elif len(database) == 1:
                print(' 1 item.')
            else:
                print(' %i itens.' % len(database))
        # Evaluate the current statistic and substatistics for each unit/school or group of those.
        chart_file_name = temporary_path(compose_chart_file_name([item_name, analysis_name, unit_name]))
        evaluated_statistics[item_name][unit_name][analysis_name] = \
            analysis_fnc(data=database, column=analysis_name, file_name=chart_file_name)
        if evaluated_statistics[item_name][unit_name][analysis_name][STATISTIC_SUBNAMES['missing']] > 0:
            missing = evaluated_statistics[item_name][unit_name][analysis_name][STATISTIC_SUBNAMES['missing']]
            print('\t"%s": faltando em %i itens (%.2f%%).' % (analysis_name, missing, missing / len(database) * 100))
        elif configs['analyze'].get('debug zero missing', True):
            print('\t"%s": sem dados faltantes.' % analysis_name)
        return

    # Do the analysis for each unit/institute of UNICAMP.
    unit_name: str
    for unit_name in database.keys():
        print(f'Análise de "{item_name}" em "{unit_name}"...', end='')
        # configs['analyze'].get('debug zero qty', True)
        # if len(database[unit_name]) == 0:
        #    print(' NENHUM item.')
        #    # continue
        # It is necessary to try to evaluate the statistics on an empty
        # pd.DataFrame to create each 0 value.
        for analysis_name, analysis_fnc in items_definitions[items_category[item_name.lower()][0]]['analysis'].items():
            # Use the same analysis coded logic for unit/school and grouped units/schools.
            evaluate_statistic_chart(unit_name, database, analysis_name, analysis_fnc)
    # Performs the analysis by the groups of units/institutes as like a single unit.
    group_unit_name: str
    for group_unit_name in GROUPS_NAME_LIST:
        # Grid the general/global analysis of UNICAMP, this analysis partitioned on before code section.
        print(f'Análise de "{item_name}" no grupo "{group_unit_name}"...', end='')
        # Get the units/institutes that belong to the current in analysis group.
        grouped_database = merge_units(database, group_unit_name)
        if grouped_database.empty:
            continue

        # Iterate though each statistic to be evaluated to groups (same as for single units).
        analysis_name: str
        analysis_fnc: callable
        for analysis_name, analysis_fnc in items_definitions[items_category[item_name.lower()][0]]['analysis'].items():
            # Use the same analysis coded logic for unit/school and grouped units/schools.
            evaluate_statistic_chart(group_unit_name, grouped_database, analysis_name, analysis_fnc)

            # TODO Create some specific analysis and charts about classification between
            # the units/institutes using the previous evaluated statistics.
            classification_chart = classification_definitions.get(analysis_name)
            if classification_chart:
                # Specific classification chart, it have not output values as the
                # conventional unit analysis.
                chart_file_name = compose_chart_file_name([item_name, analysis_name, group_unit_name, 'classificação'])
                classification_chart(data=grouped_database, column=analysis_name, file_name=chart_file_name)

            # Check for code consistence. Check some group statistic (total and missing data)
            # with the sum of all unit that compose the group. Because some unit are empty
            # in this current statistic, it is necessary to check if that is preset on the
            # dictionary.
            if group_unit_name in evaluated_statistics[item_name].keys():

                def get_statistic_qty_all_units(item_name: str,
                                                statistic_name: str,
                                                substatistic_name: str | None = None) -> int | float:
                    '''Get all quantity of values of one specific statistic in all units
                    of a specific category item.
                    This is used to confront with the merged dataframe (all unit that
                    belongs to a specific group).

                    Parameters:
                        Name of the equipment category.
                        Name of the statistic.
                        Name of the sub-statistic, if needed.
                    '''
                    statistic_values = []
                    for unit_name in get_units_group(database.keys(), group_unit_name):
                        if unit_name in evaluated_statistics[item_name].keys():
                            if substatistic_name:
                                statistic_values.append(
                                    evaluated_statistics[item_name][unit_name][statistic_name][substatistic_name])
                            else:
                                statistic_values.append(
                                    evaluated_statistics[item_name][unit_name][statistic_name])
                    return sum(statistic_values)

                if evaluated_statistics[item_name][group_unit_name][STATISTIC_SUBNAMES['total']] != \
                        get_statistic_qty_all_units(item_name, STATISTIC_SUBNAMES['total']):
                    print('\t---->> Error: wrong totalization.')
                if evaluated_statistics[item_name][group_unit_name][analysis_name][STATISTIC_SUBNAMES['missing']] != \
                        get_statistic_qty_all_units(item_name, analysis_name, STATISTIC_SUBNAMES['missing']):
                    print('\t---->> Error: "missing count" for "%s" statistic.' % analysis_name)

    print('\n', end='')


# %%
# Save the evaluated statistics into a spreadsheet.

print('Formatando os dados...')
statistics_to_save = {}
for item_name, statistics_item in evaluated_statistics.items():
    statistics_to_save[item_name] = {}
    for unit_name, statistics_item_unit in statistics_item.items():
        statistics_to_save[item_name][unit_name] = {}
        for statistic_name, statistic_value in statistics_item_unit.items():
            if not isinstance(statistic_value, dict):
                statistics_to_save[item_name][unit_name][statistic_name] = statistic_value
            else:
                for statistic_subname, statistic_subvalue in statistic_value.items():
                    name = GENERAL_SEP.join([statistic_name, statistic_subname])
                    statistics_to_save[item_name][unit_name][name] = statistic_subvalue
                # TODO allow substatistics in a interactive form to save
                # (the read is already solved). It will be necessary to
                # "Selo PROCEL" scores and other if I plan to use.

print('Salvando análise...', end='')
with pd.ExcelWriter(file_name_analysis) as spreadsheet_writer:
    # Add authorship information.
    # It will be the first and active sheet on the spreadsheet file.
    spreadsheet_write_info(spreadsheet_writer)

    # Add each statistic.
    for item_name, statistics_item in statistics_to_save.items():
        statistics_table = pd.DataFrame.from_dict(statistics_item)
        statistics_table = statistics_table.transpose()
        columns = statistics_table.columns.values
        # Necessary to keep `index=True` to save the main keys in the first column.
        statistics_table.to_excel(excel_writer=spreadsheet_writer,
                                  sheet_name=item_name,
                                  columns=columns)
        spreadsheet_adjust_columns(spreadsheet_writer, statistics_table, sheet_names=item_name)
        spreadsheet_format_columns(spreadsheet_writer, statistics_table, sheet_names=item_name)

print(' salvo.')
