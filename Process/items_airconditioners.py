#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide specific definitions for searh and scrape
# air conditioners ("ar condicionados").
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import re

# Required but not default libraries.
import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# from numba import jit

# Internal libraries.
from special_data import UniqueElemList
from items_general_info import *
from analysis_chart import *
from general_definitions import UNIDENTIFIED_TYPE, _, configs, print
from numeric_statistic import count_purchases
from items_airconditioners_dig import get_refrigeration_power_from_model
from format_equip_data import unify_airconditioners_systems

TYPES_TO_EXTRACT_BTU_H_FROM_MODEL = (_('condensador'), _('evaporador'), _('split'), _('janela'))


# %%
# Functions to get data from the original databases.

def get_electric_power_airconditioner(item_idx: int,
                                      category: str,
                                      row: pd.DataFrame,
                                      found_statistics: dict[str, Any] = {}) -> float:
    '''Get the electrical power for air conditioners.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled into 'Modelo'.
        Search on 'Modelo' column if did not found at 'Descrição'.
        Print message if not found value.

    Return watt or volt-ampere power (float).
    '''
    return (get_electric_power(item_idx, category, row,
                               units_re=['W'],
                               search_on_model=False, print_warning=False)
            or get_electric_power(item_idx, category, row,
                                  units_re=[r'W *h* *\/ *m[eê\?]s'],
                                  search_on_model=False, print_warning=False) / 30
            )


def get_refrigeration_power(item_idx: int,
                            category: str,
                            row: pd.DataFrame,
                            found_statistics: dict[str, Any] = {}) -> int | float | None:
    '''Get the refrigeration power.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.

    Return BTU/h power (int usually, sometimes float).
    '''
    # Use multiple methods to get the refrigeration power prioritizing the first.
    found_btu_h = UniqueElemList(accept_none=False)
    found_btu_h.add(get_refrigeration_power_from_string(item_idx, category, row, found_statistics))
    if is_subtype_equipment(found_statistics.get('Tipo'), TYPES_TO_EXTRACT_BTU_H_FROM_MODEL):
        if found_btu_h and list(found_btu_h)[0] % 500 != 0:
            text_warning_missing(row, 'Potência frigorífica', category,
                                 message=f'Possível erro ({found_btu_h}) na identificação de ')
        found_btu_h.add(get_refrigeration_power_from_model(row.get('Marca'), row.get('Modelo')))

    found_btu_h.discard(0)
    if not found_btu_h:
        print(text_warning_missing(row, 'Potência frigorífica', category))
        return None
    elif len(found_btu_h) > 1:
        print(text_warning_missing(row, 'Potência frigorífica', category,
                                   message=f'Diferente ({found_btu_h}) '))
    return list(found_btu_h)[0]


# %%
# Functions to get data from the original databases.

def get_refrigeration_power_from_string(item_idx: int,
                                        category: str,
                                        row: pd.DataFrame,
                                        found_statistics: dict[str, Any] = {},
                                        column_search: str = 'Descrição') -> int | float | None:
    '''Get the refrigeration power.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled in 'Modelo'.

    Return BTU/h power (int usually, sometimes float).
    '''
    # Search definitions.
    RE_DECIMAL_SEP = r'[\.,;\s]'
    # The bellow definition is used to catch the user wrong with
    # decimal separator and wrong space typing like " 7.50 0BTU S"
    # RE_NUM = r'(\d+' + RE_DECIMAL_SEP + r'*[o\d]*)\s*'
    # Added `r'([a-z]\.)*'` for get the case of "CAPAC.75TR" on
    # description that was causing `0.75` value output instead `75`,
    # this made `idx_result` default value became 2 instead 1.
    RE_NUM = r'([a-z]\.)*(?P<result>\d*\.*' + r'\d+' + RE_DECIMAL_SEP + r'*[o\d]*)\s*'
    # Use `RESULT_EXPECTED_IDX` definition to relative change the
    # expected point of regular expression match, remembering the
    # the 0-index is the full/complete pattern match.
    RESULT_EXPECTED_IDX = 2

    # To catch a specific error by a condition debug, use `idx == ID_INT_NUMBER`
    # to pause it is equivalent to `row['id'] == ID_INT_NUMBER` but `row['id']`
    # is used as index by my definition on the `pandas.dataframe` object.

    def desc_to_power(pattern: str,
                      idx_result: int | None = None,
                      remove_decimal_mark: bool = True,
                      min_len_consider: int | None = None,
                      min_len_break: int | None = None) -> int | None:
        '''Get a power number from the `row[column_search]`.

        Parameters:
            Pattern to search in the `row[column_search]`.
            Optional index expected by the match answer. If `None`, use
                the group 'result'.
            Remove decimal mark (comma or dot) from numeric string. It also
                allow a lambda function using the matched `power_string` as
                unique parameter.
            Minimum length to consider a correct match.
            Minimum length of characters to consider at the right side of
                the matched string before break (and not consider the left
                side) if there is a space.

        Return the BTU power numeric value.
        '''
        match = re.search(pattern, row[column_search], re.IGNORECASE)
        if match:
            if idx_result is None:
                power_string = match.group('result')
            else:
                power_string = match[idx_result]
            if not power_string and idx_result > 0:
                power_string = match[idx_result - 1]
            # Some typing error using '[oO]' instead '0' (zero).
            power_string = re.sub(r'o', '0', power_string, flags=re.IGNORECASE)
            power_string = power_string.strip()

            # Check if it is a minimum viable match string length.
            if min_len_consider and len(power_string) < min_len_consider:
                return None

            # If the description text have, in sequence, the model finished
            # with number that is the same of the power plus the power text,
            # the two could have concatenated. Remove this (example, ID 575779).
            # This need to be done before the decimal mark remotion to allow
            # espaces.
            idx_half_string = int((len(power_string) - 1) / 2)
            if len(power_string) % 2 == 1 and power_string[idx_half_string] == ' ':
                if power_string[0:idx_half_string] == power_string[idx_half_string + 1:]:
                    power_string = power_string[idx_half_string + 1:]

            # Check if there is a minimum string length and space in the matched
            # string to consider only the right side-part of the string.
            # This happens when a model or description finished with a number
            # character is followed by the refrigeration power description.
            # NOTE as example: '... 38VFCA18M5 18000 BTUS' from 665303 of CPQBA.
            if min_len_break and ' ' in power_string:
                power_string_split = power_string.split()
                power_string = ''
                idx_count = len(power_string_split) - 1
                while len(power_string) < min_len_break and idx_count >= 0:
                    power_string = power_string_split[idx_count] + power_string
                    idx_count -= 1

            # Check if there was misplaces zeros after a thousand indicator,
            # this just resolve part of the issue (when there are the ".")
            # for thousand indication.
            power_string_split = power_string.split('.')
            if len(power_string_split) == 2 and power_string_split[-1] == '0000':
                power_string = power_string[:-1]

            # Remove the decimal markers.
            remove_decimal_mark = ((isinstance(remove_decimal_mark, bool) and remove_decimal_mark)
                                   or (not isinstance(remove_decimal_mark, bool)
                                   and remove_decimal_mark(power_string)))
            if remove_decimal_mark:
                power_string = re.sub(RE_DECIMAL_SEP, '', power_string)

            try:
                if remove_decimal_mark:
                    return int(power_string)  # Return the integer.
                else:
                    return float(power_string.replace(',', '.'))  # If vale a decimal mark, the return will be float.
            except ValueError:
                print('\tPotência frigorífica "(%s)" não interpretada para %i (%s): %s' %
                      (power_string, item_idx, category, row['Descrição'].strip()))
                return None
        else:
            return None

    # Order for search here is important, it must check first
    # the less generic words because the groups formed by
    # such words may contain the generic words.

    # Get the kBTU value instead BTU. The correct unity is
    # kBTU/h but usually the omit the "/h" (lazy people).
    # NOTE Search first by the multiple of the unit because of
    # item 352395 of CCUEC have double entry kBTU plus BTU.
    power_value = desc_to_power(RE_NUM + r'K\ *BTU\ *S*',
                                remove_decimal_mark=False)
    if power_value:
        if power_value > 1e3 and str(round(power_value))[-3:] == '000':
            # Case return a thousand value, it will be converted
            # to million in the `return`. This is a possible
            # mistake of the register user that filled kBTU/h
            # with the BTU/h value.
            # NOTE This is the case of on 751212 item of IB.
            power_value /= 1e3
        return power_value * 1e3
    # Get the BTU value. Consider all the wrong typing:
    # BTU, BTUS, MTU, MTUS, BTS.
    power_value = desc_to_power(RE_NUM + r'[\ \/B]*[BMN]\ *(T\ *U|U\ *T|T\ *S)\ *S*',
                                min_len_break=4)
    if power_value:
        if power_value < 100:
            # NOTE Case of 770340 and 770342 of FE and 755106
            # of CAISM, some user did not type "000" or forgot "K",
            # just "12" or "24", instead of "12000" or "24KBTU".
            power_value *= 1000
        if power_value > 100e3:
            # NOTE Case of 569149 of IB some more zeros but not dot
            # to indicate.
            power_value /= 10
        if power_value < 2e3:
            # NOTE Case of 491973 of IB and others.
            power_value *= 10
        return power_value
    # Get the MIL BTU value instead BTU.
    power_value = desc_to_power(RE_NUM + r'(MIL|M)\ *BTU\ *S*')
    if power_value:
        if power_value > 1e6:
            # The registered the item did not concerned about zeros
            # and "kilo".
            # NOTE This happened if items 74883 and 74884 of HC.
            power_value /= 1e3
        if power_value < 1e3:
            power_value *= 1e3
        return power_value
    # Get the TR value and convert to BTU.
    # In this case the decimal mark is always placed correct and the
    # resulted number should be small (it will be multiplied to by converted
    # to BTU/h).
    # Usually `remove_decimal_mark=False` but in some case the register
    # user fill a BTU value as TR. To identify it and not remove the
    # decimal point, I check value on the other (BTU type) conversion.
    # NOTE This is the case on 368831 item of CLE.
    power_value = desc_to_power(RE_NUM + r'TRS*',
                                # remove_decimal_mark=False)
                                remove_decimal_mark=lambda x:
                                    int(re.sub(RE_DECIMAL_SEP, '', x)) > 10e3)
    if power_value:
        if power_value < 1000:
            # If the value as really in TR, otherwise the user the
            # list the item just made a wrong description and want
            # to mean BTU instead.
            # See item 368831 of CLE por example.
            power_value *= 12000  # TR to BTU/h conversion.
        if power_value / int(power_value) < 1.001:
            power_value = int(power_value)
        return power_value
    # Get kCal/h.
    power_value = desc_to_power(RE_NUM + r'K\ *CAL(\ *\/\ *H)*',
                                min_len_break=4)
    if power_value:
        power_value *= 3.968320165  # kcal/h to BTU/h conversion.
        if power_value / int(power_value) < 1.001:
            power_value = int(power_value)
        return power_value
    # Get just K value and suppose that it is kBTU/h.
    power_value = desc_to_power(RE_NUM + r'K(\.|\s|$|\))',
                                min_len_break=2)
    if power_value:
        return power_value * 1e3
    # Get the FR (that may be not a unit) and assume as BTU/h or KBTU/h.
    # Consider typing errors as CR and FK.
    power_value = desc_to_power(RE_NUM + r'[CF][RK]*(\s|\.|\;|$)',
                                min_len_consider=2,
                                min_len_break=2)
    if power_value:
        if power_value < 100:
            power_value *= 1e3  # This case for kBTU/h.
        return power_value
    # No power unit specified, adopted BTU to capture the integer value.
    power_value = desc_to_power(r'(^|\s|[a-z])(\d{1,4}'
                                + RE_DECIMAL_SEP
                                + r'*[05]00)(\.|\;|\s|$)', RESULT_EXPECTED_IDX - 2)
    if power_value:
        return power_value
    # The refrigeration power is sometimes in the middle of the model
    # string, try to extract it from something like the model with "K"
    # make of unit.
    power_value = desc_to_power(r'(\s|^)'
                                r'[a-z]+(\d{2,})K[a-z]+'
                                r'(\.|\;|\s|$)', 2)
    if power_value:
        if power_value < 100:
            power_value *= 1e3  # This case for kBTU/h.
        return power_value

    # Special rules for just the model column.
    # if column_search == 'Modelo':
    if is_subtype_equipment(found_statistics.get('Tipo'), TYPES_TO_EXTRACT_BTU_H_FROM_MODEL):
        power_value = desc_to_power(r'(\s|^)'
                                    r'(\d{4,5}|\d{1,2}\.\d{3})'
                                    r'\,*(\.|\;|\-|\s|$|[a-z])', 2)
    if power_value:
        return power_value

    # If not found at the 'Descrição' column, try to found at the
    # 'Modelo' column by recursive use of the function.
    if column_search == 'Descrição':
        return get_refrigeration_power_from_string(item_idx, category, row,
                                                   found_statistics, column_search='Modelo')

    return None


def get_airconditioner_type(item_idx: int,
                            category: str,
                            row: pd.DataFrame,
                            found_statistics: dict[str, Any] = {},
                            column_search: str = 'Descrição') -> str:
    '''Get the air conditioner type.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default
            is 'Descrição', some user filled in 'Modelo'.

    Returns the air conditioner type (str):
                janela, evaporador (inverter),
                condensador (inverter), portátil, central...
    '''

    def is_match_type(pattern: str) -> bool:
        '''Get the type on the description.
        Tip: Always check the most specific category before the
        most generic one, because the first one may have the
        second word/noun as part of it description.

        Parameter pattern (str).

        Return (bool).'''
        if pd.isnull(row[column_search]):
            return False
        return re.search(pattern, row[column_search], re.IGNORECASE)

    def get_subtype() -> str:
        '''Get some sub-type classification to be added at the end of the
        classification string.'''
        sub_string = ''
        # Check if the item have the subclassification "inverter".
        if is_match_type(r'(inver(ter|sora*)|sistema +vrf(\.|\s|$)*)'):
            sub_string += ' ' + _('inverter')
        # Sometimes is filled the model with "inverter" word instead the manufacture model number.
        elif not pd.isnull(row['Modelo']) and re.search(r'inverter', row['Modelo'], re.IGNORECASE):
            sub_string += ' ' + _('inverter')
        return sub_string

    def get_subtype_split() -> str:
        '''Get some sub-type classification to be added at the end of the
        classification string.'''
        sub_string = ''
        # Check if is a "cassete" subtype.
        # In a research, and BEC enrolled equipments (https://www.bec.sp.gov.br/,
        # 2021), the evaporator part of the split air conditioner type can be:
        # - Hi-wall: the small model assembled in the wall, refrigeration powers
        # from 2kBTU/h to 36kBTU/h (that is expansive compared with the next sub type).
        # - Floor ceiling air conditioning: the most used for splits of bigger BTU/h
        # into the UNICAMP, at BEC is enrolled 18kBTU/h to 92kBTU/h.
        # - Cassete:ceil built-in, most used for hallway and big rooms (30kBTU/h to 60kBTU/h).
        #
        # It only diagnosed the last type because of the special installation method,
        # the other two may be replaced between them using the criteria of cheap one,
        # additionally the differentiation of the firstly two are not always present at
        # the original inventory DB.
        if is_match_type(r'(cass*et+e*)'):
            sub_string += ' ' + _('cassete')
        return sub_string + get_subtype()

    if is_match_type(r'(janela|janlea)'):  # More typing errors.
        return _('janela') + get_subtype()

    if is_match_type(r'(evaporador*|evap\.'
                     r'|fan[ \-]*coi*l|m[óo\?]dulo[\.\ \w]+(ventilador|[sc]erpentina)'
                     r'|(unidade +de +)*eva[ \-]*pora([çc\?][ãa\?]o|dora*))'):
        if is_match_type(r'(fan[ \-]*coi*l|m[óo\?]dulo[\.\ \w]+(ventilador|[sc]erpentina))'):
            return _('fancoil') + get_subtype()
        if not is_match_type(r'chill*er'):
            if is_match_type(r'(unidade +de +)*condensa([çc\?][ãa\?]o|dora*)'):
                # The description used was "unidade evaporadora mais condensadora".
                return _('split') + get_subtype_split()
            return _('evaporador') + get_subtype_split()

    if is_match_type(r'(condensan*dora*'
                     r'|troc(a|ador)* de calor|exaust(or|[aã\?]o))') and not is_match_type(r'chill*er'):
        return _('condensador') + get_subtype_split()

    # "Split" must be checked after "evaporador" or "condicionador", because sometimes
    # they are described as "ar condicionado split - unidade evaporadora/condensadora".
    # or "unidade condensador/evaporadora do tipo split".
    if is_match_type(r'((\.|\s|^)sp*lit*(\.|\s|$))'):
        # Sometime is used the expression "parede" as a part of the split
        # that stays inside the room.
        if is_match_type(r'parede'):
            return _('evaporador') + get_subtype_split()
        return _('split') + get_subtype_split()
    if is_match_type(r'(cass*t+e)'):
        # It is equivalent of `return _('evaporador') + ' ' + _('cassete') + get_subtype()`.
        return _('evaporador') + get_subtype_split()

    if is_match_type(r'(chill*er|central|sistema +(de +)ar[\ ,-]*condicionado)'):
        return _('chiller') + get_subtype()

    if is_match_type(r'(port[aá\?]til|port\.*)'):
        return _('portátil') + get_subtype()

    # if is_match_type(r'(central|sistema)'):
    #    return _('central')

    print(text_warning_missing(row, 'Tipo', category))
    return UNIDENTIFIED_TYPE


def get_COP_index(item_idx: int,
                  category: str,
                  row: pd.DataFrame,
                  found_statistics: dict[str, Any] = {}) -> float:
    '''Evaluate the "Coefficient of Performance" of a air conditioner.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.

    Returns the COP index.
    '''
    refrigeration_btu_h = found_statistics.get('Potência frigorífica (BTU/h)', np.nan)
    electric_W = found_statistics.get('Potência elétrica (kWh)', np.nan)
    if refrigeration_btu_h is not np.nan and electric_W is not np.nan:
        return (refrigeration_btu_h * 0.29307107) / electric_W
    else:
        return np.nan


# %%
# Auxiliary functions to filter data or get specific analysis/charts based
# on the `analysis_chart.py` functions.

# The function `unify_airconditioners_systems(foo)` was moved to
# `format_equip_data.py` to generate a general processing file
# for external studies.


def simplify_airconditioner_subtype(data: pd.DataFrame) -> pd.DataFrame:
    '''Simplify the data type. It is used for better (simplified/grouped subtyped)
    pie chart on the report.
    '''
    try:
        if configs['analyze']['categories']['airconditioners']['simplify subtype']:
            data['Tipo'] = data['Tipo'].str.replace(_('cassete'), '')
            data['Tipo'] = data['Tipo'].str.replace(_('inverter'), '')
    except KeyError:
        pass
    data['Tipo'] = data['Tipo'].str.strip()
    return data


def chart_type(data: pd.DataFrame, *args, **kwargs) -> dict:
    '''Sometimes the split air conditioners equipments appear on the inventory
    as tow different parts, "evaporator"+"condenser" or "condenser"+"split" unit,
    sometimes just as "split". Those should be count as a unit item.

    This function remove the "condenser" data of this duplicated registry case.

    Parameters and returns: same as `analyze_percentage_chart`.
    '''
    statistics = analyze_percentage_chart(data, *args, **kwargs)

    original_qty = len(data)
    data = unify_airconditioners_systems(data)
    if original_qty > len(data):
        statistics.update({STATISTIC_SUBNAMES['qty']: len(data)})
        statistics = sort_statistics(statistics)

    return statistics


# %%
# References to functions for scrape data from the original databases
# and statistics and chart analysis.

# Scrape description for the property description item.
# The main key is 'category name (sub category name as optional)'.
_category_name = 'ar condicionados'
items_definitions.update({
    _category_name + ' (geral)': {
        # This search partners will be saved as columns at the
        # final spreadsheet file. The order specified here may
        # not affect the columns order of the spreadsheet, it
        # must be set on the save routine.
        'desc_match': r'(?:ar[\ ,-]*condicionado|condicionador +de +ar)',
        'desc_not_match': r'(?:evaporadora*|container|condensadora*'
                          r'|split|fan[ \-]*coi*l|ve[ií\?]culo|autom[oó\?]vel'
                          r'|esta[cç\?][aã\?]o|cortina|refrigerador'
                          r'|ventilador)',
        # List of groups that may belongs to this category. They
        # are used for double match and report inconsistences
        # in the original database procedure (as already reported).
        'groups_check': [41, 65, 43, 44],
        #        'material_check': [5533],
        # References to functions of scrap data.
        'search': {
            'Tipo': get_airconditioner_type,
            'Data de compra': get_purchase_data,
            'Potência frigorífica (BTU/h)': get_refrigeration_power,  # Needs type evaluation first.
            'Potência elétrica (W)': get_electric_power_airconditioner,
            # 'COP (W/W)': get_COP_index,  # Needs refrigeration and electric power evaluation.
            'Selo PROCEL': get_procel_stamp,
            'Localização': get_location,
            'Aquisição': get_incorporation_method,
        },
        # References to functions for statistic and chart analysis.
        'analysis': {
            # analyze_percentage_chart,  # "Tipo" must be the first analysis because it result may be used into other charts.
            'Tipo': lambda data, *args, **kwargs: chart_type(simplify_airconditioner_subtype(data), *args, **kwargs),
            'Data de compra': lambda data, *args, **kwargs: analyze_continuous_chart(
                unify_airconditioners_systems(data), *args, **kwargs),
            'Potência frigorífica (BTU/h)': lambda data, *args, **kwargs: analyze_continuous_chart(
                unify_airconditioners_systems(data), *args, **kwargs),
            # 'Potência elétrica (W)': analyze_continuous_chart,
            'Selo PROCEL': analyze_percentage_chart,
            'Aquisição': lambda data, *args, **kwargs: count_purchases(unify_airconditioners_systems(data), *args, **kwargs),
            # 'Saldo Contábil': analyze_scatter_chart,
        }
    }
})
# TODO o que é "SISTEMA DE REFRIGERACAO" item 687400 do IQ?
# TODO colocar "DESUMIDIFICADOR"?

items_definitions[_category_name + ' (evaporador-condensador)'] = \
    items_definitions[_category_name + ' (geral)'].copy()
items_definitions[_category_name + ' (evaporador-condensador)']['desc_match'] = (
    r'(?:evaporadora*|condensadora*|chill*er'
    r'|(?:\s|^)fan[ \-]*coi*l(?:\s|$)|m[óo\?]dulo[\.\ \w]+climatizador)'  # suporte p[a-z\/\ ]+ condicionado)'
)
items_definitions[_category_name + ' (evaporador-condensador)']['desc_not_match'] = (
    r'(?:split|microsc[oó\?]*pio|microscopia|liofilizador|incubadora'
    r'|espectro(?:fotometro)*|geladeira|freezer|destilador|bateria'
    r'|evaporador +rotati[vc]o|rotae*vapor(?:ador)*|microfone|painel'
    r'|imagem|container|epi|refrigerador|projetor|refluxa[cç\?][aã\?]o'
    r'|seringa|uhv|banda +uhf|feixe +de +el[eé\?]trons|bebedouro|testador'
    r'|transformador|l[aâ]mpada|c[aâ\?]mara(?: +frigorifica)*|banho'
    r'|(?:de +)*feixe|(?:de +)*metais|(?:bomba +de +)*v[aá\?]cuo|caixa|bomba'
    r'|condensador +de +emiss*[aã\?]o|concentrador|separador|medidor'
    r'|maquete)'
)

items_definitions[_category_name + ' (split)'] = \
    items_definitions[_category_name + ' (geral)'].copy()
items_definitions[_category_name + ' (split)']['desc_match'] = \
    r'(?:(?:\s|^)split(?:\s|$))'  # "split" must be a isolated word into the sentence.
items_definitions[_category_name + ' (split)']['desc_not_match'] = (
    # The split air conditioner will be a "evaporadora _ condensadora".
    # See CECOM 665238 and 665239.
    # r'(?:evaporadora*|condensadora*|gabinete|'
    r'(?:chill*er'
    r'|ethernet|switch|video|hdmi|vga|amplificador'
    r'|cromatograf(?:o|ia)|limpeza|[a-z]c[aâ\?]mera|[oó\?]tica'
    r'|espectr[óo\?]metro|testador|cortadora*'
    r'|trif[aá\?]sico +de +indu[cç\?][aã\?]o)'
)


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    test_data = pd.DataFrame.from_dict(
        {
            'Identificador': [654773],
            'Descrição': ['CONDICIONADOR DE AR; DO TIPO JANELA/PAREDE. DESLIZANTE; COM CAPACIDADE DE NO MIN.'
                          ' 21.000 BTUS; OPERACAO: CICLO FRIO; VAZAO DE AR DE NO MINIMO 940 M3H;VOLTAGEM: 220 VOLTS;'
                          ' COM CONSUMO APROXIMADO DE 21 80 W; COM SELO PROCEL. CLASSIFICACAO A ;'
                          ' FUNCOES:TURBO/SLEEP/TIMER/AJUSTE AUTOMATICO DE TEMPERATUR']
        }
    )
    for idx, db_row in test_data.iterrows():
        try:
            print('Tipo do ar condicionado:', get_airconditioner_type(idx, '--nenhuma--', db_row))
        except (KeyError, TypeError):
            pass  # Abode function may use "Modelo" and "Fabricante" information.
        print('Potência de refrigeração:', get_refrigeration_power(idx, '--nenhuma--', db_row))

    from items_testcase import test_all
    test_all(__file__, items_definitions)
