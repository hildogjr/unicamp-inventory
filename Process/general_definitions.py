#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide all the generic/general definitions for common
# use into the software (paths, file names, special characters,
# ...).
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import os
import re
import yaml
# import tempfile
import gettext  # Used for translation, mainly to the articles.
import locale
import functools
# import sys
# print(sys.getrecursionlimit())
# sys.setrecursionlimit(2000)

# Required but not default libraries.


# Internal libraries.
# This is base, no more level file present bellow.


# %%
# General configurations

# Databases folder os.path.
path_db = 'public'  # os.path.join(os.path.dirname(__file__), 'Databases')
path_resources = 'Resources'  # os.path.join(os.path.dirname(__file__), 'Resources')
path_temporary = 'Temporary'  # os.path.join(os.path.dirname(__file__), 'Temporary')
# path_temporary = os.path.join(tempfile.gettempdir(), 'Análise de patrimônio')
# Use the "public" folder as output to match with the CI/CD
# path definitions to deploy/publish the report and website.
path_output = 'public'  # os.path.join(os.path.dirname(__file__), 'public')

# if not os.path.isdir(path_temporary):
#    os.mkdir(path_temporary)

file_name_report = os.path.join(path_output, 'Relatório.pdf')
file_name_analysis = os.path.join(path_output, 'Análise geral.xlsx')

# Get the configurations for analysis and report from the YAML/YML
# configuration file. Windows OS require a full path here and mandatory
# specify the encoding of the file.
with open(os.path.join(os.path.split(__file__)[0], 'configurations.yml'),
          'r', encoding='utf-8') as config_file:
    configs: dict[str, dict | Any] = yaml.load(config_file, Loader=yaml.FullLoader)


# Force some configurations as English and image definitions in case of the
# output be set to articles.
if int(os.getenv('ARTICLE', 0)):
    print('--->>> Running under article customizations...')
    # Set the language for the article.
    configs.update({'translate language': 'en_US'})
    # Set better image file output and without the reduction of size.
    configs['charts'].update({'file extension': '.png',
                              'add watermark': False,
                              'reduced': False})
    # configs['unidentified type'] = 'NO CLASS'
    # Set specific definitions od analysis for each category.
    configs['analyze'].update({'categories': {'airconditioners': {'simplify subtype': True}}})
    # print('All configurations:', configs)

# Multicores use definition.
MULTIPROCESSING_CORE_USAGE_QTY: int | None = int(os.getenv('MULTIPROCESSING_CORE_USAGE_QTY', 1)) or None
if isinstance(MULTIPROCESSING_CORE_USAGE_QTY, int) and MULTIPROCESSING_CORE_USAGE_QTY > os.cpu_count():
    MULTIPROCESSING_CORE_USAGE_QTY = None
if not MULTIPROCESSING_CORE_USAGE_QTY:
    core_usage_qty = os.cpu_count()
else:
    core_usage_qty = MULTIPROCESSING_CORE_USAGE_QTY
print(f'Using {core_usage_qty}/{os.cpu_count()} CPU cores for multiprocessing routines.')
if MULTIPROCESSING_CORE_USAGE_QTY == 1:
    print = print
else:
    # To print after the multiprocessing pool finish with the execution order.
    print = functools.partial(print, flush=True)


# Configuration to the translations.
locale_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'Languages')
# gettext.bindtextdomain('myapplication', locale_dir)
# gettext.textdomain('myapplication')
is_get_translate = configs.get('translate language')
if is_get_translate:
    if is_get_translate == True:  # noqa
        current_locale, encoding = locale.getdefaultlocale()
        if os.path.isdir(os.path.join(locale_dir, current_locale)):
            _ = gettext.translation('messages', locale_dir, [current_locale]).gettext
    elif isinstance(is_get_translate, str) and \
            os.path.isdir(os.path.join(locale_dir, is_get_translate)):
        _ = gettext.translation('messages', locale_dir, [is_get_translate]).gettext
    # gettext.install()
else:
    _ = gettext.gettext


# General definitions.
FILE_NAME_DB = 'AGI*.*'  # Filter for general DB file of the university.
FILE_NAME_INVENTORY_DB = 'AGI*BemMovel*.*'  # Filter for general DB file inventory.
FILE_NAME_BUILDS_DB = 'AGI*Imoveis*.*'  # Filter for general DB file inventory.
FILE_NAME_SECTORS_DB = 'AGI*Areas*.*'  # Filter for general DB file inventory.

FILE_SPREADSHEET_RE_NOT_ALLOWED_CHARS = r'[:\\\/\?*\[\]_]'  # Not allowed chars for file names and spreadsheet page name.
# Column used to track the software progress / debug the match of the item.
COLUMN_NAME_SEARCHER = 'Searcher'  # Category/sub-category search name whose match.
COLUMN_NAME_MATCH = 'Match'  # Match response.
# String and names configurations.
GENERAL_SEP = ' - '  # Logic general separator for file names and substatistics in the spreadsheet.
SPREADSHEET_PAGE_INFO_NAME: str = configs.get('about page name', 'Informações gerais')
UNIDENTIFIED_TYPE: str = configs.get('unidentified type', _('SEM CLASSE'))

GROUPS_NAME_LIST: dict = configs.get('groups')
if GROUPS_NAME_LIST:
    GROUPS_NAME_LIST = GROUPS_NAME_LIST.keys()
COLUMN_UNIT_NAME = 'Área de Patrimônio'


# %%
# General functions.

# unit_groups = None
def get_units_group(units_presents: list[str],
                    group_name: str,
                    check_previous_for_duplicated: bool = True,
                    remove_groups_names_from_list: bool = True) -> list[str]:
    ''' Get the list of units/schools that belongs to a specific group
    name by use of the `configurations.yml` file definitions.

    Parameters:
        units_presents: (list) List of the units/schools present (read and valid).
        group_name: (str) Name of the specific group.
        check_previous_for_duplicated: (default True) check if this group have
                        content duplicated from previous groups and return `None`
                        if affirmative.
        remove_groups_names_from_list: (default True) Remove the own group names
                        from the list of present units. It is necessary for use
                        into the report generation.

    Return the list of names of the units/schools that belongs to
    the specific group.
    '''
    unit_groups: dict = configs.get('groups')
    if not unit_groups:
        # No group configuration present.
        return []
    if group_name not in unit_groups.keys():
        # Group configuration present but specific name not found
        # at the group list.
        return []

    # Group name is on the list, make the specific logics to get the list.
    units_list = unit_groups.get(group_name)
    if not units_list:
        # Configuration empty.
        return []

    # Check for duplicated data list between the groups and return `[]`
    # if this actual group have same list of units/schools of a previous
    # one. Use a recursive method for calculation.
    if check_previous_for_duplicated:
        groups_list = list(unit_groups.keys())
        group_name_index = groups_list.index(group_name)
        if group_name_index > 0:
            units_list_group = get_units_group(units_presents, group_name, False)
            if not units_list_group:
                return []
            for group_previous_name in groups_list[:group_name_index]:
                units_list_group_previous = get_units_group(units_presents, group_previous_name, False)
                if set(units_list_group) == set(units_list_group_previous):
                    print(f'->Grupo "{group_name}" possui as mesmas unidades de "{group_previous_name}" e não será listado.')
                    return []
            return units_list_group

    # Remove the own group names from the list of present units. It is
    # necessary because inside `generate_report.py` the list of units
    # may virtually contain the group names.
    if remove_groups_names_from_list:
        units_presents = list(units_presents)
        for n in unit_groups.keys():
            if n in units_presents:
                units_presents.remove(n)
        # The bellow code may be not necessary, because the group name
        # have to be present at `unit_groups.keys()`.
        if group_name in units_presents:
            units_presents.remove(group_name)

    # Case of single name in the group "list".
    if isinstance(units_list, str):
        units_list = units_list.strip()
        if units_list[0] != '~':
            if units_list not in units_list:
                # One single unit/school, some exception case.
                if units_list not in units_presents:
                    return []
                return [units_list]
            else:
                return []
        else:
            if len(units_list) == 1:
                # Operator of all present groups.
                return list(units_presents)
            else:
                # This group is the complement of previous one.
                # That the full list present and remove the pointed group.
                units_list_clean = list(units_presents)
                for units_to_remove in get_units_group(units_presents, units_list[1:], False):
                    if units_to_remove in units_list_clean:
                        units_list_clean.remove(units_to_remove)
                return sorted(list(set(units_list_clean)))

    # Deal with the list and operator that could be inside.
    if isinstance(units_list, list):
        # Allow group names, to add it, or '~' operator inside a list, to remove it.
        name_to_add: list[str] = []
        name_to_remove: list[str] = []
        for name in units_list:
            # The numeric reference is used as reference to the build. #TODO
            # This matches with the administrative entity. #TODO

            # String logic: is is used for units/schools reference as well as
            # reference to other groups.
            if name in unit_groups.keys():
                name_to_remove.append(name)  # Must remove the group name indicator.
                # Add all units/schools of the specific group.
                name_to_add += get_units_group(units_presents, name, False)
            elif name[0] == '~' and name[1:] in unit_groups.keys():
                name_to_remove.append(name)  # Must remove the group name indicator.
                # Remove all the units/schools of the specific group.
                name_to_remove += get_units_group(units_presents, name[1:], False)
            elif name == '~':
                name_to_remove.append(name)  # Must remove the indicator.
                # "~" alone is just a indicator for all units present in the data.
                name_to_add += units_presents
            elif name[0] == '~':
                name_to_remove.append(name)  # Must remove the name indicator.
                if name[1:] in units_presents:
                    name_to_remove.append(name[1:])  # Remove the correspondent unity.
        # Add the listed school/unit names.
        units_list += name_to_add
        # Remove the listed school/unit names.
        for name in name_to_remove:
            units_list.remove(name)
        # Check if there some invalid name and return `None`
        units_list = sorted(list(set(units_list)))
        for name in units_list:
            if name not in units_presents:
                print(f'-> "{name}" não consta na lista de unidades presentes.\n{units_list}')
                return []
        return units_list

    # No type match.
    return []


def sort_by_units(units: dict[str, Any] | list[str]) -> dict[str, Any] | list[str]:
    '''Sort a dictionary of statistics by unit/school keys
    or a list of unit/school names prioritizing the
    totalization to be first (UNICAMP, campus X, ...).

    Parameter (dict or list) Unsorted dict/list.

    Return the sorted dictionary.'''
    group_name_list_reverse = list(GROUPS_NAME_LIST)
    group_name_list_reverse.reverse()
    if isinstance(units, dict):
        sorted_key_list = sorted(units.keys())
        for group_name in group_name_list_reverse:
            if group_name in sorted_key_list:
                # Add the summaries as first pages.
                sorted_key_list.remove(group_name)
                sorted_key_list = [group_name] + sorted_key_list
        sorted_unit_statistics = {}
        for k in sorted_key_list:
            sorted_unit_statistics[k] = units[k]
        return sorted_unit_statistics
    elif isinstance(units, list):
        sorted_list = sorted(units)
        for group_name in group_name_list_reverse:
            if group_name in sorted_list:
                # Add the summaries as first pages.
                sorted_list.remove(group_name)
                sorted_list = [group_name] + sorted_list
        return sorted_list
    else:
        # In case of `dict_items`.
        return sorted(units)


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    # Debug the above functions.
    '''
    import glob
    import importlib
    format_input = importlib.import_module('format_input.py')
    read_database = format_input['read_database']

    db_files = glob.glob(os.path.join('Databases', '*.xlsx')) or glob.glob('*.xlsx')
    present_units = []
    for db_file in db_files:
        _, units = read_database(db_file)
        present_units += units
    '''
    present_units = ['AEPLAN', 'ASCOM', 'BCCL', 'BENS DISPONÍVEIS', 'CAISM', 'CCG', 'CCS',
                     'CCUEC', 'CEB', 'CECOM', 'CEL', 'CEMEQ', 'CEMIB', 'CENAPAD', 'CGU', 'CIPA',
                     'CLE', 'COCEN', 'COCEN/CBMEG', 'COCEN/CEPAGRI', 'COCEN/CESOP', 'COCEN/CIDDIC',
                     'COCEN/CMU', 'COCEN/CPETRO', 'COCEN/LUME', 'COCEN/NEPA', 'COCEN/NEPAM',
                     'COCEN/NEPO', 'COCEN/NEPP', 'COCEN/NICS', 'COCEN/NIED', 'COCEN/NIPE',
                     'COCEN/NUDECRI', 'COCEN/PAGU', 'COMVEST', 'COTIL', 'COTUCA', 'CPF', 'CPO',
                     'CPP', 'CPQBA', 'CT', 'DAC', 'DEA', 'DEAS', 'DEDIC', 'DEPI', 'DERI', 'DGA',
                     'DGRH', 'DIRCUL', 'DLIE', 'EA2', 'EDITORA', 'EDUCORP', 'EXTECAMP', 'FCA',
                     'FCF', 'FCM', 'FE', 'FEA', 'FEAGRI', 'FEC', 'FEEC', 'FEF', 'FEM', 'FENF',
                     'FEQ', 'FOP', 'FT', 'GASTROCENTRO', 'GGBS', 'GGUS', 'GR', 'HC', 'HEMOCENTRO', 'IA',
                     'IB', 'IC', 'IE', 'IEL', 'IFCH', 'IFGW', 'IG', 'IMECC', 'INOVA', 'IQ', 'LACTAD',
                     'MAV', 'MEC', 'NACSES', 'OUVIDORIA', 'PG', 'PME', 'PRDU', 'PRE', 'PRE/CAC',
                     'PRE/CASA DO LAGO', 'PRE/CIS GUANABARA', 'PRE/DIRCUL', 'PREFEITURA', 'PRG',
                     'PRG/GGTE', 'PRG/PROFIS', 'PRP', 'PRPG', 'RTV', 'SAE', 'SAPPE', 'SAR', 'SEC',
                     'SG', 'SIARQ', 'SIC']

    present_units_qty = len(present_units)
    print('Todas unidades presentes (%i):\n' % present_units_qty, present_units)
    # print('Checando %i grupos.' % len(GROUPS_NAME_LIST))
    print('\n')
    for idx, unit in enumerate(present_units):
        present_units[idx] = re.sub(FILE_SPREADSHEET_RE_NOT_ALLOWED_CHARS, GENERAL_SEP, unit)

    # Add the group names list, as it is possible into the
    # `generate_report.py` code.
    present_units += GROUPS_NAME_LIST
    group_unit_qty = [
        present_units_qty, present_units_qty - 1, present_units_qty - 7,
        10, present_units_qty - 10 - 1
    ]
    # For help debug if I missed some quantity above, just append `None`.
    # Normally this happess when I am editing `configurations.yml` to
    # add new groups to analyze.
    if len(GROUPS_NAME_LIST) > len(group_unit_qty):
        group_unit_qty += [None] * (len(GROUPS_NAME_LIST) - len(group_unit_qty))

    for idx, (group_name, qty) in enumerate(zip(configs['groups'].keys(), group_unit_qty)):
        units_into_group = sort_by_units(get_units_group(present_units, group_name))
        qyt_units_into_group = len(units_into_group)
        print(f'Grupo {idx + 1}/{len(GROUPS_NAME_LIST)} -> {group_name} ({qyt_units_into_group}):\n', units_into_group)

        # If hadn't expressed the expected quantity and the group is a list,
        # assumes the length of such list. Warning: this will not validate
        # groups that use wildcard operators but it will check if they unit
        # names were correctly written by the `present_units`.
        if qty is None and isinstance(configs['groups'][group_name], list):
            qty = len(configs['groups'][group_name])

        assert qyt_units_into_group == qty, \
            'Quantidade errada de unidades no grupo "%s".' % group_name
        print('\n')

    print(get_units_group(present_units, '10 maiores kW·h'))
