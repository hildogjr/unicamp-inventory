#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas NOT classified
# information, it is covered by Federal Brazilian Law 12527
# from 18/Nov./2011 (public data access):
# http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm.
#
# Identify the patrimony items from the given databases and
# save the results in a new condensed database with sheets
# for each item category.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

# %%
from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import os
import glob
import re
from datetime import datetime
# from functools import partial, reduce
import collections

# Required but not default libraries.
# import xlsxwriter  # Used as subset as `pandas` to better edit the outputted spreadsheet.
# import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
from db_utils import search_fnc_by_unit, merge_subcategories
from general_definitions import (path_db, path_output, configs, sort_by_units,
                                 COLUMN_NAME_SEARCHER, COLUMN_NAME_MATCH,
                                 GENERAL_SEP, FILE_SPREADSHEET_RE_NOT_ALLOWED_CHARS,
                                 FILE_NAME_INVENTORY_DB)
from items_definitions import items_definitions, items_category
from format_input import read_database
from format_output import (spreadsheet_adjust_columns, spreadsheet_format_columns,
                           spreadsheet_write_info, join_list_str)

print('=' * 55)
print('Analisando dados de patrimônio da UNICAMP e identificando items consumidores.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Itens identificado em %s às %s' %
      (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)


# %%
# Read each XLSX file data base in the folder/subfolder.

# Find all the databases.
# Prioritize a folder 'Databases' with all the spreadsheet files,
# model used by the multiple file exportation by the old UNICAMP
# patrimony system (exportation by help of Lindnalva). After
# use the single file model that could be on the project root
# folder (my new system access and model to the real implementation).
path_db_file_names = glob.glob(os.path.join(path_db, FILE_NAME_INVENTORY_DB)) or glob.glob(FILE_NAME_INVENTORY_DB)
if not path_db_file_names:
    exit('Nenhum banco de dados encontrado.')

count_items, count_db_files = (0, 0)
units: set = set()
database = pd.DataFrame()
db_date: datetime | None = None
RE_DATE = re.compile(r'(?P<year>\d{4})_*(?P<month>\d{2})_*(?P<day>\d{2})'
                     r'(_*(?P<hour>\d{2})_*(?P<minute>\d{2})_*(?P<second>\d{2})*)*\.(xlsx*|ods)$')
for count_db_load, db_file_name in enumerate(path_db_file_names, 1):
    if os.path.splitext(db_file_name)[-1].lower() in ('.zip', '.7z', '.rar'):
        continue
    print('Carregando banco de dados "%s" (%i/%i)... ' %
          (db_file_name, count_db_load, len(path_db_file_names)))

    # Extract the date from the file name. It is used as metadata.
    try:
        date_re = RE_DATE.search(db_file_name)
        current_db_date = datetime(int(date_re.group('year')), int(date_re.group('month')), int(date_re.group('day')),
                                   int(date_re.group('hour') or '0'), int(date_re.group('minute') or '0'),
                                   int(date_re.group('second') or '0'))
        if current_db_date:
            if not db_date:
                db_date = current_db_date
            elif db_date != current_db_date:
                db_date = min(db_date, current_db_date)
                print('Aviso: datas diferentes na extração dos arquivos de origem. Usando menor.')
    except AttributeError:
        db_date = datetime.today()
        print('Aviso: assumido o dia de hoje como data de atualização da DB de origem.')

    # Load the database.
    # db_file_name = os.path.abspath(db_file_name)
    db_file, units_file = read_database(db_file_name)
    if (isinstance(db_file, pd.DataFrame) and db_file.empty):
        print('Arquivo "%s" vazio or não válido.' % db_file_name)
    # db_file.set_index('Identificador', inplace=True)
    count_db_files += 1
    units = units.union(units_file)
    database = pd.concat([database, db_file], axis=0, ignore_index=True)
    count_items += db_file.shape[0]  # Same as `len(db_file)`.
del db_file
del units_file

if not units:
    # Not database successfully read.
    exit('Nenhum arquivo usado retornou identificações válidas para o programa.')
elif count_db_files > 1:
    print('%i banco(s) de dados analisados(s) com  total de %i itens patrimoniados e %i unidades:' %
          (count_db_load, count_items, len(units)), sorted(units))
print('\n')

# Search by the patterns on each unit/school/institute.
items_found_db, items_found_statistic = search_fnc_by_unit(database, sorted(units))


# %%
# General statistics about each item kind.

print('Total por categoria de item explorada:')
print('As subcategorias listadas refletem apenas aspectos internos de'
      ' busca de software NÃO devem ser consideradas como contabilização'
      ' do (sub)tipo do item. Para isso use o relatório final do software.')

# Show some general statistics about the read items into the databases.
for equip_category, equip_names in items_category.items():
    equip_category_qty = 0
    equip_category_qty_collected = 0
    for equip_name in equip_names:
        for unit in units:
            equip_category_qty += items_found_statistic[equip_name][unit]['qty']
            equip_category_qty_collected += items_found_statistic[equip_name][unit]['collected']

    # If only one sub category, use this as full name category.
    if len(equip_names) == 1:
        equip_category = equip_names[0]
    if equip_category_qty > 0 or configs['identify'].get('debug zero qty'):
        if equip_category_qty_collected > 0:
            print('Total de "%s": %i (%i coletados e não listados)' %
                  (equip_category, equip_category_qty, equip_category_qty_collected))
        else:
            print('Total de "%s": %i' % (equip_category, equip_category_qty))

        # If more the one sub category present, print the statistics for each one.
        if len(equip_names) > 1:
            for equip_name in equip_names:
                qty_total_equip = 0
                qty_total_collected_equip = 0
                for unit in units:
                    qty_total_equip += items_found_statistic[equip_name][unit]['qty']
                    qty_total_collected_equip += items_found_statistic[equip_name][unit]['collected']
                if qty_total_equip > 0 or configs['identify'].get('debug zero qty'):
                    if qty_total_collected_equip > 0:
                        print('\t- %s: %i (%i coletados e não listados)' %
                              (equip_name, qty_total_equip, qty_total_collected_equip))
                    else:
                        print('\t- %s: %i' % (equip_name, qty_total_equip))

print('\n')

# Show some statistics about the items by unit/school/institute.
# This code was before (when using the one spreadsheet database by
# unit exported by the old system) inside the loop of the first
# code section. Now it have to be outside because of the unique
# file exportation format used.
unit_name: str
for unit_name in units:
    print('Unidade "%s"' % unit_name, end='')
    report_not_found = True
    for equip_category, equip_names in items_category.items():
        equip_category_qty = 0
        equip_category_qty_collected = 0
        for equip_name in equip_names:
            equip_category_qty += items_found_statistic[equip_name][unit_name]['qty']
            equip_category_qty_collected += \
                items_found_statistic[equip_name][unit_name]['collected']

        # If only one sub category, use this as full name category.
        if len(equip_names) == 1:
            equip_category = equip_names[0]
        if equip_category_qty or configs['identify'].get('debug zero qty'):
            if report_not_found:
                print('')
            report_not_found = False
            if equip_category_qty_collected > 0:
                print('\tTotal de "%s": %i (%i coletados e não listados).' %
                      (equip_category, equip_category_qty, equip_category_qty_collected))
            else:
                print('\tTotal de "%s": %i' % (equip_category, equip_category_qty))

            # If more the one sub category present, print the statistics for each one.
            if len(equip_names) > 1:
                for equip_name in equip_names:
                    equip_qty = items_found_statistic[equip_name][unit_name]['qty']
                    equip_qty_collected = items_found_statistic[equip_name][unit_name]['collected']
                    if equip_qty > 0 or configs['identify'].get('debug zero qty'):
                        if equip_qty_collected > 0:
                            print('\t\t%i %s (%i coletados e não listados).' %
                                  (equip_qty, equip_name, equip_qty_collected))
                        else:
                            print('\t\t%i %s.' % (equip_qty, equip_name))
    if report_not_found:
        print(': NADA encontrado.')

print('\n')


# %%
# Look for duplicated items, found by two different filters or
# possible duplicated identifier in the origin databases
# (this last will never be the case since the identifier
# should be a master key on the original database).
all_id = []
for items_db in items_found_db.values():
    for items_unit in items_db.values():
        all_id += list(items_unit['Identificador'])
items_found_duplicated = collections.Counter(all_id)

# Print the information about the duplicated items/ids.
if len(items_found_duplicated) < len(all_id):
    print('Identificações duplicadas (it should be ZERO):',
          sum(qty > 1 for qty in items_found_duplicated.values()))

    # Print the category totals. The `Searcher` column name
    # identification must be present to do and it can be disabled
    # of the columns to save ('info to save') on the configuration file.
    if all(COLUMN_NAME_SEARCHER in items_unit.keys()
            for item_category in items_found_db.values()
            for items_unit in item_category.values()):
        # Duplicated detection inside each main category, catch
        # by two subcategories.
        category_name: str
        for category_name, subcategories in items_category.items():
            item_name_found_duplicated_qty = 0
            for item_id, qty in items_found_duplicated.items():
                if qty > 1:
                    items_unit: dict
                    for items_unit in items_found_db.values():
                        for item in items_unit.values():
                            for id, searcher in zip(item['Identificador'], item[COLUMN_NAME_SEARCHER]):
                                if id == item_id and searcher in subcategories:
                                    item_name_found_duplicated_qty += 1
            if item_name_found_duplicated_qty or configs['identify'].get('debug zero qty'):
                print('\t' + category_name.capitalize() + ':', item_name_found_duplicated_qty)

            '''
            # Duplicated inside each sub category.
            for item_name in subcategories:#items_definitions.keys():
                item_name_found_duplicated_qty = 0
                for item_id, qty in items_found_duplicated.items():
                    if qty > 1:
                        for items_unit in items_found_db.values():
                            for item in items_unit.values():
                                if item['Identificador'] == item_id and item[COLUMN_NAME_SEARCHER] == item_name:
                                    item_name_found_duplicated_qty += 1
                if item_name_found_duplicated_qty:
                    print('\t\t' + item_name.capitalize() + ':', item_name_found_duplicated_qty)
            '''
    else:
        print('Não é possível rastrear a duplicação por categoria devido a'
              ' ausência da coluna "%s" nos dados.' % COLUMN_NAME_SEARCHER)

    # Print each item description found in more than one identification.
    # Duplicated across the diferente search filters may indicate a possible
    # filter mismatch by missing 'desc_not_match' field definition.
    for id_duplicated, duplicated_qty in items_found_duplicated.items():
        if duplicated_qty > 1:
            print('- %i' % id_duplicated)
            # Duplicated across the different databases. This only
            # happens by a not unique ID control of UNICAMP databases.
            print_description = True
            past_unit = None
            item_name: str
            for item_name, item_db in items_found_db.items():
                # Duplicated across the units should not happens and indicate
                # and problem on the original database.
                unit_name: str
                items: dict
                for unit_name, items in item_db.items():
                    duplicated_items_idx = [idx
                                            for idx, id in enumerate(items['Identificador'])
                                            if id == id_duplicated]
                    if not duplicated_items_idx:
                        continue  # Not in the current unit in search.
                    # Duplicated at same filter of same data base. This may should
                    # not occur by an error in this script but by incorporated items
                    # composed by two or more parts.
                    if print_description:
                        print('\t%s' % items['Descrição'][duplicated_items_idx[0]])
                        print_description = False
                    if not past_unit:
                        print('\t%s -> ' % unit_name, end='')
                    for idx_loop, idx_item in enumerate(duplicated_items_idx):
                        category_string = item_name
                        # Add the match result if present in the table.
                        if COLUMN_NAME_MATCH in items.keys():
                            category_string += ' : ' + items[COLUMN_NAME_MATCH][idx_item]
                        if (idx_loop > 0 and idx_loop < len(duplicated_items_idx)) \
                                or (past_unit and past_unit == unit_name):
                            print(' | %s' % category_string, end='')
                        else:
                            print(category_string, end='')
                    past_unit = unit_name
            print('')
else:
    print('Não encontrado items duplicados.\n')

# Free some memory.
del all_id
del items_found_duplicated

print('\n')


# %%
# Enumerate the missing information in quantity by each database.

missing_info: dict[str, dict[str, dict[str, int]]] = {}
is_missing_info = False
for item_name, item_db in items_found_db.items():
    if unit_name not in missing_info.keys():
        missing_info[item_name] = {}
    for unit_name, items in item_db.items():
        if item_name not in items.keys():
            missing_info[item_name][unit_name] = {}
        for info_name, infos in items.items():
            missing_info[item_name][unit_name][info_name] = sum(not v for v in infos)
            if missing_info[item_name][unit_name][info_name]:
                is_missing_info = True  # Sinalize the found missing infos (to be printed).

# Print the missing information statistic in a logic way.
if is_missing_info:
    print('As informações faltantes / não preenchidas pelo software:')
    item_names = sorted(items_definitions.keys())
    # Info names could be different for each item searched.
    info_names = []
    for unit_name, item in items_found_db.items():
        for item_name in item.keys():
            info_names += list(missing_info[unit_name][item_name].keys())
    info_names = sorted(set(info_names))
    # Print the list for each information showing the
    # missing in each origin database.
    for info_name in info_names:
        for item_name in item_names:
            all_missing = []
            for unit in units:
                if info_name in missing_info[item_name][unit].keys() and \
                        missing_info[item_name][unit][info_name] > 0:
                    all_missing.append('%s (%i)' % (unit, missing_info[item_name][unit][info_name]))
            if any(all_missing):
                missing = join_list_str(all_missing)
                print('\t- "%s" em "%s": %s' % (info_name, item_name, missing))

    print('\n')


# %%
# Save the result in a readable Excel .xlsx file.

# Merge all subcategories into the master category.
items_found_db = merge_subcategories(items_found_db)

# Save one file by category of items splitting the db by sheets
# and category column to the sub-category information.
units = sorted(units)
equip_category: str
equip_unit_db: dict
for equip_category, equip_unit_db in sort_by_units(items_found_db.items()):
    file_name = equip_category.capitalize() + '.xlsx'
    print('Saving (overwriting) "%s" found items...' % equip_category, end='')
    file_path = os.path.join(path_output, file_name)
    # Remove the file to not overlap older (execution output) data.
    if os.path.isfile(file_path):
        os.remove(file_path)

    # Write the spreadsheet file.
    with pd.ExcelWriter(file_path) as spreadsheet_writer:
        # Format the spreadsheet and add authorship information.
        # It will be the first and active sheet on the spreadsheet file.
        spreadsheet_write_info(spreadsheet_writer, date_db=db_date)

        # Sort the unit to make easy to find the unit/school at each
        # spreadsheet file for each equipment category.
        sheets_name = []
        for unit_name, equip_unit_items in sort_by_units(equip_unit_db.items()):
            if unit_name == units[-1]:
                # Check if exist items found (may happen not).
                if equip_unit_items.empty:
                    print(' *%s*.' % unit_name)
                else:
                    print(' %s.' % unit_name)
            else:
                # Check if existe items found (may happen not).
                if equip_unit_items.empty:
                    print(' *%s*,' % unit_name, end='')
                else:
                    print(' %s,' % unit_name, end='')

            # Replace special characters that can not be used as sheet
            # name by `GENERAL_SEP` (it is expected to be already done
            # by `uniformize_unit_name` into `format_input.py`).
            sheet_name = re.sub(FILE_SPREADSHEET_RE_NOT_ALLOWED_CHARS, GENERAL_SEP, unit_name)
            sheets_name.append(sheet_name)
            equip_unit_items.to_excel(excel_writer=spreadsheet_writer,
                                      sheet_name=sheet_name,
                                      # columns=columns_to_save,
                                      index=False)

            # Adjust the columns width to better user visualization.
            spreadsheet_adjust_columns(spreadsheet_writer, equip_unit_items, sheet_names=sheets_name)
            spreadsheet_format_columns(spreadsheet_writer, equip_unit_items, sheet_names=item_name)
            # spreadsheet_writer.save()  # Save at the end, returning the scope of `with .. as spreadsheet_writer`.
