#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Copyright definitions, used for attach information of my
# authorship to some specific file extensions.
# Uses the opencv library.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['imagefile_add_watermark', 'image_add_watermark']

# Base libraries.
import os

# Required but not default libraries.
import numpy as np
import cv2 as cv
try:
    from numba import jit
except ImportError:
    from debug_fncs import empty_decorator as jit

# Internal libraries.
# from debug_fncs import time_it


# %%

# @jit
def image_rotate(img: np.ndarray, angle: float) -> np.ndarray:
    '''Rotates an image in the center and expands image to avoid cropping.

    Parameters:
        Original image.
        Rotation angle in degrees.

    Return the rotated image.
    '''
    # Code borrow from https://stackoverflow.com/questions/43892506/opencv-python-rotate-image-without-cropping-sides.
    # Evaluate the rotation matrix to by applied to the image.
    height, width = img.shape[:2]
    image_center = (width / 2, height / 2)
    rotation_mat = cv.getRotationMatrix2D(image_center, angle, 1.)
    # Rotation calculates the cos and sin, taking absolutes
    # of those to find the new width and height bounds.
    abs_cos = abs(rotation_mat[0, 0])
    abs_sin = abs(rotation_mat[0, 1])
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)
    # Subtract old image center (bringing image back to origin) and
    # adding the new image center coordinates
    rotation_mat[0, 2] += bound_w / 2 - image_center[0]
    rotation_mat[1, 2] += bound_h / 2 - image_center[1]
    # Rotate image with the new bounds and translated rotation matrix
    rotated_img = cv.warpAffine(img, rotation_mat, (bound_w, bound_h))
    return rotated_img


@jit
def image_add_watermark(img: np.array,
                        watermark_img: np.array,
                        opacity: float | None = 0.2,
                        position: tuple[int, int] | None = None,
                        relative_max_size: float | None = 0.65,
                        watermark_rotation: float | None = 20.0) -> np.array:
    '''Add watermark to the center of the image.

    Parameters:
        Image matrix to add the watermark.
        Image matrix of the watermark.
        Opacity of the watermark.
        Position where the image to be placed.
        Maximum relative size.
        Watermark rotation in degrees.

    Return the image watermarked.
    '''
    h_logo, w_logo, _ = watermark_img.shape
    h_img, w_img, _ = img.shape

    # Get the dimensions to insert the watermark in the center of the main image.
    watermark_scale = min([relative_max_size * h_img / h_logo, relative_max_size * w_img / w_logo])

    # Code borrow from https://www.life2coding.com/how-to-add-logo-or-image-watermark-on-images-with-opencv-python/.
    if watermark_rotation:
        watermark_img = image_rotate(watermark_img, watermark_rotation)
    watermark_img = cv.resize(watermark_img, (0, 0), fx=watermark_scale, fy=watermark_scale)
    h_logo, w_logo, _ = watermark_img.shape  # Size of foreground.
    img = img.copy()
    h_img, w_img, _ = img.shape  # Size of background Image.
    # Position of foreground/overlay image.
    if position:
        x, y = position[0], position[1]
    else:
        x, y = (int(h_img / 2) - int(h_logo / 2), int(w_img / 2) - int(w_logo / 2))
    # The bellow code is the equivalent of the commented one but using a internal
    # language loop approach as Matlab wich is mush more quick to execute, even
    # more using `numba`.
    alpha_logo = watermark_img[:, :, 3] / 0xff * opacity
    alpha_img = 1 - alpha_logo
    # Ignore and keep the channel 4 (alpha/opacity) of the original image, if present (PNG files.)
    for ch_count in (0, 1, 2):  # Loop on RGB.
        img[x:(x + h_logo), y:(y + w_logo), ch_count] = (
            np.multiply(alpha_logo, watermark_img[:, :, ch_count])
            + np.multiply(alpha_img, img[x:(x + h_logo), y:(y + w_logo), ch_count]))
    '''
    # Loop over all pixels and apply the blending equation.
    for i in range(h_logo):
        for j in range(w_logo):
            if x + i >= h_img or y + j >= w_img:
                continue
            alpha_logo = float(watermark_img[i][j][3] / 0xff) * opacity  # Read the alpha channel.
            alpha_img = 1 - alpha_logo
            # alpha_img = float(img[i][j][3] / 0xff) * (1 - opacity)
            # Ignore and keep the channel 4 (alpha/opacity) of the original image, if present (PNG files.)
            img[x + i][y + j][0:3] = alpha_logo * watermark_img[i][j][:3] + alpha_img * img[x + i][y + j][0:3]
    '''
    # cv.addWeighted(overlaid_image, opacity, img, 1 - opacity, 0, img)
    return img


watermark_img = None  # Initialize the global (really used just as static) variable.


def imagefile_add_watermark(file_name: str,
                            file_output_name: str | None = None,
                            logo_file_name: str | None = None,
                            opacity: float | None = 0.2,
                            relative_max_size: float | None = 0.65,
                            watermark_rotation: float | None = 20.0):
    '''Add my logo as watermark to the center of the `file_name` image file.

    Parameters:
        File to add the watermark.
        Path to save, if not given it will overwrite the original image.
        Logo path file name.
        Opacity of the watermark.
        Maximum relative size.
        Watermark rotation in degrees.
    '''
    # I didn't use OpenCV since 2008, so I need to borrow some
    # reference code from https://www.youtube.com/watch?v=xfgtcAw-VIA.
    # Some future reference for project: https://www.youtube.com/watch?v=MkcUgPhOlP8.
    global watermark_img  # Make it local to save reload procedure.
    if not os.path.isfile(file_name):
        return  # File not created.
    # Load the watermark image just once to speed-up the code.
    if watermark_img is None:
        if not logo_file_name:
            logo_file_name = os.path.join('Resources', 'logo_HGJ_big.png')  # 'logo_HGJ.png'
        watermark_img = cv.imread(logo_file_name, -1)
        # The watermark must be white on a black background, my
        # logo is a black symbol with a transparent background.
        # It is necessary to convert it.
        # watermark_img = 255 - watermark_img
    img = cv.imread(file_name, -1)  # Load the source file.

    # Get the dimensions to insert the watermark in the center of the main image.
    img = image_add_watermark(img, watermark_img,
                              opacity=opacity,
                              relative_max_size=relative_max_size,
                              watermark_rotation=watermark_rotation)

    # cv.imshow('Final', img)
    # cv.waitKey(0)
    # cv.destroyAllWindows()

    if not file_output_name:
        file_output_name = file_name  # Overwrite the original file.

    file_extension = file_output_name.split('.')[-1].lower()
    if file_extension == 'png':
        # Higher level compression for the PNG image format.
        cv.imwrite(file_output_name, img, [int(cv.IMWRITE_PNG_COMPRESSION), 9])
    elif file_extension == 'jpg' or file_extension == 'jpeg':
        cv.imwrite(file_output_name, img, [int(cv.IMWRITE_JPEG_QUALITY), 70])
    else:
        cv.imwrite(file_output_name, img)


# %%
# Main entry, used for debug. In this case also used to prompt use the watermark function.
if __name__ == "__main__":
    from debug_fncs import time_it

    # Time it to check the small time on after first execution due
    # the @jit decorator used on core routines (do not print this
    # time debug on normal call executions).
    @time_it
    def watermark_img_fnc(path: str) -> None:
        imagefile_add_watermark(path)

    # Test image output.
    print('Using the debugging tests...')
    import shutil
    base_image_path = os.path.join('Test_data', 'chart_image.png')
    image_path1 = os.path.join('public', 'image1-opencv.png')
    shutil.copyfile(base_image_path, image_path1)
    watermark_img_fnc(image_path1)
    assert os.path.isfile(image_path1), f'Failed {image_path1}'
    # Test a second time, because the test of logo already loaded.
    image_path2 = os.path.join('public', 'image2-opencv.png')
    shutil.copyfile(base_image_path, image_path2)
    watermark_img_fnc(image_path2)
    assert os.path.isfile(image_path2), f'Failed {image_path2}'
    # Test a third time with other file extension.
    image_path3 = os.path.join('public', 'image3-opencv.jpg')
    shutil.copyfile(base_image_path, image_path3)
    watermark_img_fnc(image_path3)
    assert os.path.isfile(image_path3), f'Failed {image_path3}'
