#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas NOT classified
# information, it is covered by Federal Brazilian Law 12527
# from 18/Nov./2011 (public data access):
# http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm.
#
# Generate and send e-mails with official message and report,
# log file and (if necessary) pre-processed spreadsheets attached.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import re
from datetime import date, datetime
import os
from glob import glob
import smtplib
# from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
# from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication

# Libraries to compress attached file into the e-mail.
from io import BytesIO
import mimetypes
# Just import one of the follow compression methods.
try:
    import py7zr  # Needs installation.
except ImportError:
    try:
        import tarfile
    except ImportError:
        import zipfile


# Required but not default libraries.

# Internal libraries.
from general_definitions import configs, path_resources, path_output
from items_definitions import items_category
from format_output import evaluate_variables, get_link_from_string

print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Enviando e-mail em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)

EMAIL_MAX_ATTACH_SIZE = 25 * (1024 ** 2)
SIZE_CHECK_COMPRESS_LOG = 3 * (1024 ** 2)


# %%
# Create the the message header.

print('Preparando conteúdo do e-mail...')

msg_root = MIMEMultipart()
msg_root['Subject'] = 'Análise patrimonial de cargas elétricas'
msg_root['From'] = os.getenv('EMAIL_ADDRESS') or 'campus.sustentavel@unicamp.br'
msg_root['To'] = ';'.join(configs['email']['send to'])
# msg_root['Cc'] = os.getenv('EMAIL_CC')
# msg_root['Cco'] = os.getenv('EMAIL_CCO')


# %%
# Auxiliary functions.

def compress_file_into_memory(files_name: list[str] | str, files_data: list[bytes] | bytes):
    '''Compress file(s) into a IO on memory without create a
    external or temporary file into the disk.'''
    if isinstance(files_name, str):
        files_name = [files_name]
        files_data = [files_data]
    data_compressed = BytesIO()
    if 'py7zr' in globals():
        with py7zr.SevenZipFile(data_compressed, mode='w') as fh:  # noqa: F821
            for n, d in zip(files_name, files_data):
                print('\tComprimindo arquivo "%s"...' % n, end='')
                fh.writestr(d, n)  # This use a opposite order as `tarfile` and `zip` modules.
                print(' feito.')
    elif 'tarfile' in globals():
        with tarfile.open(fileobj=data_compressed, mode='w') as fh:  # noqa: F821
            from io import StringIO
            for n, d in zip(files_name, files_data):
                print('\tComprimindo arquivo "%s"...' % n, end='')
                file_info = tarfile.TarInfo(name=n)  # noqa: F821
                file_info.size = len(d)
                fh.addfile(tarinfo=file_info, fileobj=BytesIO(d))
                print(' feito.')
    elif 'zipfile' in globals():
        with zipfile.ZipFile(data_compressed, mode='w',  # noqa: F821
                             # compression=zipfile.ZIP_BZIP2) as fh:
                             compression=zipfile.ZIP_DEFLATED, compresslevel=9) as fh:  # noqa: F821
            for n, d in zip(files_name, files_data):
                print('\tComprimindo arquivo "%s"...' % n, end='')
                fh.writestr(n, d)
                print(' feito.')
    else:
        raise RuntimeError('Not present any fle compression library.')
    return data_compressed.getvalue()


def add_attachment(file_path: str,
                   content_id: str | None = None,
                   compress_file: bool = False):
    '''Add an attachment or image to be used into the message.

    Parameters:
        File path
        Image id for the HTML use case.
        Compress the file.
    '''
    filename = os.path.split(file_path)[1]
    # Guess the content type based on the file's extension.
    ctype, encoding = mimetypes.guess_type(file_path)
    if ctype is None or encoding is not None:
        ctype = 'application/octet-stream'
    maintype, subtype = ctype.split('/', 1)
    with open(file_path, 'rb') as fp:
        file_size = os.fstat(fp.fileno()).st_size
        if file_size >= EMAIL_MAX_ATTACH_SIZE:
            compress_file = True  # Force compression.
        file_data = fp.read()
        # Compress the file before send (it is asked to
        # or the uncompressed file size surpasses the
        # e-mail attachment limit).
        if compress_file:
            file_data = compress_file_into_memory(filename, file_data)
            # Replace some original information used as file
            # meta data on the send procedure.
            ctype, subtype = 'application/7z'.split('/', 1)
            filename = os.path.splitext(filename)[0] + '.' + subtype
            '''
            fh = open(os.path.join('Output', filename), 'wb')
            fh.write(file_data)
            fh.close()
            '''
        # Get the file type.
        if maintype == 'application':
            mime_file = MIMEApplication(file_data, subtype, name=filename)
        elif maintype == 'image':
            mime_file = MIMEImage(file_data, subtype, name=filename)
        else:
            return
        # Add the ID to the attachment content.
        if not content_id:
            content_id = os.path.splitext(filename)[0]
        mime_file.add_header('Content-ID', f'<{content_id}>')
        # Add the attachment to the e-mail
        msg_root.attach(mime_file)


# %%
# Create the the message body.
# This is the only section that need to be changes to change de message.

with open(os.path.join(path_resources, 'e-mail.html'), 'r') as f_h:
    body_message = f_h.read()

# Get the release version notes.
with open('RELEASE.md', 'r') as f_h:
    release_notes = f_h.read()
# Get each release notes on the file.
release_notes = re.findall(r'(#\s*(.|\s)+?)\s\s', release_notes)
if release_notes:
    # Use just the last release notes.
    last_release_notes = re.search(r'#\s*(?P<month>\d+)-(?P<year>\d+)\s'
                                   r'(?P<description>(.|\s)+)', release_notes[0][0])
    # Sometimes I amm working on next release and just not tagged the release.
    if not last_release_notes:
        last_release_notes = re.search(r'#\s*(?P<month>\d+)-(?P<year>\d+)\s'
                                       r'(?P<description>(.|\s)+)', release_notes[1][0])
    # Check if there is a release note for this correspondent release.
    if (int(last_release_notes.group('year')) == date_now.year
            and int(last_release_notes.group('month')) == date_now.month):
        last_release_notes = last_release_notes.group('description')
        last_release_notes = re.sub(r'\n', r'<br>', last_release_notes)
        last_release_notes = '<br><b>Notícias da versão:</b><br>' + last_release_notes + '<br><br>'
    else:
        last_release_notes = ''
        print('Não encontrado informações específicas sobre a versão.')
    # The `last_release_notes` is included on the e-mail body by the
    # '{{last_release_notes}}' definition on "Resources\e-mail.html".

# Replace some values on the template message be software variable values.
items_categories = [n.capitalize() for n in sorted(items_category.keys())]

# Get the bi-annual data string.
if int(date_now.strftime(r'%m')) < 7:
    semester_string = '1'
else:
    semester_string = '2'
semester_string += 'º semestre'
semester_now = '%s/%s' % (semester_string, date_now.strftime(r'%Y'))

with open('articles.txt') as f_articles:
    articles = f_articles.read().splitlines()
    for idx in reversed(range(len(articles))):
        if not articles[idx]:
            del articles[idx]
    if articles:
        articles_html = '<p>Artigos como referências do trabalho:<ul>'
        for article in articles:
            if article:
                url = get_link_from_string(article)
                if url:
                    article = article.replace(url, '<a href="{url}">{url}</a>'.format(url=url))
                articles_html += '<li>' + article + '</li>'
        articles_html += '</ul></p>'
        body_message = body_message.replace(r'<!--ARTICLES TO CITE-->', articles_html)

# Put inside the message string: bi-annual data string, recognized items
# and release notes.
body_message = evaluate_variables(dict(globals()), body_message, convert_to_str=True)
body_message = re.sub(r'{{PROJECT_URL}}', os.getenv('CI_PROJECT_URL', 'https://gitlab.com/hildogjr/unicamp-inventory'), body_message)
body_message = re.sub(r'{{CI_PAGES_URL}}', os.getenv('PAGE_URL', 'https://hildogjr.gitlab.io/unicamp-inventory/'), body_message)

print('Anexando arquivos...')
add_attachment(os.path.join(path_output, 'Relatório.pdf'), compress_file=True)
if configs['email'].get('add log'):
    # if (isinstance(configs['email']['add log'], bool)
    #        or (isinstance(configs['email']['add log'], str) and unit == configs['email']['add log'])
    #        or (isinstance(configs['email']['add log'], list) and unit in configs['email']['add log'])):
    log_file = glob(os.path.join(path_output, 'Identifi*o.*'))[0]
    if os.path.splitext(log_file)[-1] in ('.log', '.txt'):
        add_attachment(log_file, compress_file=True)
    else:
        add_attachment(log_file, compress_file=(os.path.getsize(log_file) > SIZE_CHECK_COMPRESS_LOG))


# %%
# Last message configurations need to better composition.

# Add the embedded images.
print('Adding embedded files...')
path_logos = os.path.relpath(os.path.join(path_resources, 'page_files'))
for id_match in re.finditer(r'\<img.* src=\"cid:(?P<id>[\w\_]+)\".*\>', body_message, re.I):
    obj_id = id_match.group('id')
    file_path = glob(os.path.join(path_logos, obj_id + '.*'))[0]
    add_attachment(file_path, content_id=obj_id)

# Remove HTML comments from HTML and plain message version.
body_message = re.sub(r'<!--((.|\s)*?)-->', '', body_message)
# Add a plain message based on the HTML message.
# print('Creating pain message for low resource readers or connection...')
# plain_message = re.sub(r'\n', '', body_message)
# plain_message = re.sub(r'<br>', r'\n', plain_message)
# plain_message = re.sub(r'(<.+?>|&.+?;)', '', plain_message)
# msg_alternative.attach(MIMEText(plain_message, 'plain', 'utf-8'))
# Add the HTML message (the last is the priority).
msg_root.attach(MIMEText(body_message, 'html'))


# %%
# Send the message.

# Check the presence of the environment.
if not os.getenv('EMAIL_ADDRESS') or not os.getenv('EMAIL_PASSWORD') or not os.getenv('EMAIL_SMTP_SERVER'):
    try:
        from dotenv import load_dotenv
        print('Trying to force environment load (used on debug mode)...', end='')
        load_dotenv()
        print(' loaded.')
    except ImportError:
        pass
    if not os.getenv('EMAIL_ADDRESS') or not os.getenv('EMAIL_PASSWORD') or not os.getenv('EMAIL_SMTP_SERVER'):
        exit('Missed user and password registry on application.')

# Send the e-mail though an SMTP Server.
# https://support.google.com/mail/?p=InvalidSecondFactor
# https://security.google.com/settings/security/apppasswords
try:
    print('Conectando-se ao servidor de e-mail...', end='')
    smtp_host_name = os.getenv('EMAIL_SMTP_SERVER')
    smtp_host_port = int(os.getenv('EMAIL_SMTP_PORT', 587))
    smtp_server = smtplib.SMTP(smtp_host_name, smtp_host_port)
    # smtp_server.ehlo_or_helo_if_needed()
    smtp_server.ehlo()
    print(' conectado.')
    print('Iniciando comunicação criptografada...', end='')
    # smtp_server.starttls(context=ssl.create_default_context())
    smtp_server.starttls()
    # smtp_server.ehlo()
    print(' TTLS iniciado.')
    print('Autenticando login do e-mail...', end='')
    smtp_server.login(os.getenv('EMAIL_ADDRESS'), os.getenv('EMAIL_PASSWORD'))
    print(' autenticado.')
    print('Enviando mensagem de e-mail...', end='')
    smtp_server.timeout = 60 * 5 * 3  # It is necessary some (five) minutes to send the attached files.
    smtp_server.send_message(msg_root)
    print(' enviado.')
    smtp_server.quit()
    print('Finalizada conexão com servidor.')
except Exception as e:
    print('Erro no envio do email:', e)
    if 'smtp_server' in globals() and smtp_server.esmtp_features.get('size'):
        max_email_size = int(smtp_server.esmtp_features['size'])
        actual_email_size = len(msg_root.as_string())
        print('Tamanho permitido de mensagem: %iB (%.2fMB)' % (max_email_size, max_email_size / 1024**2))
        print('Tamanho atual de mensagem: %iB (%.2fMB)' % (actual_email_size, actual_email_size / 1024**2))
        smtp_server.quit()
    exit('Email não enviado.')
