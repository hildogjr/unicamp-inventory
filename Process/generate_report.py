#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas NOT classified
# information, it is covered by Federal Brazilian Law 12527
# from 18/Nov./2011 (public data access):
# http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm.
#
# Use the statistics saved into the evaluated database to
# generate a formal report.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

# %%
from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import os
import re
from datetime import datetime
from math import isnan
import argparse
from pkg_resources import parse_version


# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
try:
    from bs4 import BeautifulSoup as bs
except ImportError:
    pass
import weasyprint

# Internal libraries.
# Some variable/functions imported here are reported as not being use. Do not remove
# those, they are indirectly referenced by `evaluate_variables` function.
from general_definitions import (path_temporary, path_resources,
                                 file_name_report, file_name_analysis,
                                 GENERAL_SEP, configs, GROUPS_NAME_LIST,
                                 COLUMN_NAME_SEARCHER, COLUMN_NAME_MATCH, SPREADSHEET_PAGE_INFO_NAME,
                                 get_units_group, sort_by_units)
from items_definitions import (PROCEL_STAMP_START_DATE, PROCEL_STAMP_BEFORE_TAG, items_category)
# from classification_definitions import classification_definitions
from format_output import (evaluate_variables, compose_chart_file_name,
                           number_to_html, image_to_html, html_section_id_name, report_link,
                           join_list_str, get_default_unit, get_link_from_string,
                           STATISTIC_SUBNAMES)

print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Relatório gerado em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)


# %%
# Entry point and arguments definition.

arg_parser = argparse.ArgumentParser(
    description='Gera o relatório de análise das cargas consumidoras '
                'da UNICAMP. Elaborado por: %s.' % __author__)
arg_parser.add_argument('-o', '--output',
                        type=str,
                        help='Force the output file name. Default: "%s"' % file_name_report,)
# default=file_name_report)
arg_parser.add_argument('-u', '--unit', '--units', '--school', '--schools',
                        type=str, nargs='+',
                        help='Defines unit(s)/school(s) to generate the report. '
                             'It includes global totalization analysis where the unit are present.')
arg_parser.add_argument('-i', '--item', '--items',
                        type=str, nargs='+',
                        help='Defines item(s) to generate the report.')
args = arg_parser.parse_args()
report_units_to_include = args.unit
if report_units_to_include:
    report_units_to_include = [i.upper() for i in report_units_to_include]
report_items_to_include = args.item
if report_items_to_include:
    report_items_to_include = [i.lower() for i in report_items_to_include]
# Check if the output file name was forced.
if (report_units_to_include or report_items_to_include) and not args.output:
    file_name_report = os.path.splitext(file_name_report)
    file_name_complement = ''
    if report_units_to_include:
        names = [n.upper() for n in report_units_to_include]
        file_name_complement += GENERAL_SEP + ' '.join(names)
    if report_items_to_include:
        names = [n.capitalize() for n in report_items_to_include]
        file_name_complement += GENERAL_SEP + ' '.join(names)
    file_name_report = file_name_report[0] + file_name_complement + file_name_report[1]
    print('Arquivo de saída forçado para "%s"' % file_name_report)
if args.output:
    default_file_name_report = os.path.split(os.path.realpath(file_name_report))
    file_name_report = os.path.split(args.output)
    if not file_name_report[0]:
        file_name_report = os.path.join(default_file_name_report[0], file_name_report[-1])
    if os.path.splitext(file_name_report)[-1].lower() != '.pdf':
        file_name_report += '.pdf'
    file_name_report = os.path.relpath(file_name_report)
    print('Arquivo de saída forçado para "%s"' % file_name_report)
cover_message = []
if report_units_to_include:
    cover_message.append(join_list_str(report_units_to_include,
                                       msg_singular='da unidade "%s"',
                                       msg_plural='das unidades: %s'))
    print('Esse relatório incluirá apenas %s.' % cover_message[-1][1:])
if report_items_to_include:
    names = [name.capitalize() for name in report_items_to_include]
    cover_message.append(join_list_str(names,
                                       msg_singular='do item "%s"',
                                       msg_plural='dos itens: %s'))
    print('Esse relatório incluirá apenas %s.' % cover_message[-1][1:])
if cover_message:
    cover_message = '<h3>Esse relatório apenas inclui dados %s.</h3>' % \
                    ' e, '.join(cover_message)


# %%
# Extra definitions to the software.

# Definitions of shortcut methods.
def resource_path_join(f):
    return os.path.join(path_resources, f)


# temporary_path_join = lambda f: os.path.join(path_temporary, f)
# Definition to get name and unit of a statistic.
RE_NAME_UNIT = re.compile(r'^(?P<name>.+?) *([\(\[](?P<unit>.+)[\)\]])?$',
                          re.IGNORECASE)


# %%
# Load the data.

# Read each XLSX file data base in the folder/subfolder.
print('Carregando dados da análise...', end='')
if os.path.isfile(file_name_analysis):
    database = pd.read_excel(file_name_analysis, sheet_name=None)
elif os.path.isfile(os.path.splitext(file_name_analysis)[0] + '.ods'):
    file_name = os.path.splitext(file_name_analysis)[0] + '.ods'
    with pd.ExcelFile(file_name) as excel_file_handle:
        database: dict[str, pd.DataFrame] = {}
        for page in excel_file_handle.sheet_names:
            database[page] = pd.read_excel(excel_file_handle, sheet_name=page)
else:
    exit('\nInexiste análise dos items de patrimônio para se gerar um relatório.')
if SPREADSHEET_PAGE_INFO_NAME in database.keys():
    # Remove the general authorship and running information.
    del database[SPREADSHEET_PAGE_INFO_NAME]
evaluated_statistics = {}
for k, db in database.items():
    # Set the index for the unit/school or statistic names, if transposed
    # or not by the previous algorithm, fix it by checking the columns
    # name (the school/institutes must be in the row names, not in the
    # column names).
    db.set_index('Unnamed: 0', inplace=True)
    if any([name not in db.columns for name in GROUPS_NAME_LIST]):
        db = db.transpose()
    evaluated_statistics[k] = pd.DataFrame.to_dict(db)
print(' carregado.')

# Format the data as before saved into the spreadsheet. Save into
# a spreadsheet as used as a temporary development solution but it
# was keep on the final software to save temporary data and allow
# future parallelization of the statiscs calculation.
print('Formatando os dados lidos...', end='')
for item in evaluated_statistics.values():
    item_unit: dict
    for item_unit in item.values():
        statistic_name_delete = []
        # Iterate through the statistics, necessary to generate a
        # previsous list of the names intead of the `.keys()` method
        # because its content is updated inside the function.
        statistic_name: str
        for statistic_name in list(item_unit.keys()):
            statistic = item_unit[statistic_name]
            names = statistic_name.split(GENERAL_SEP)
            if len(names) >= 2:
                statistic_name_delete.append(statistic_name)
                # Iteractive logic for multiple level dictionary update.
                d = item_unit
                for k in names[:-1]:
                    if k not in d.keys():
                        d[k] = {}
                    d = d[k]
                d[names[-1]] = statistic
        # Delete the previous statistic by its condensated name.
        for d in statistic_name_delete:
            del item_unit[d]
print(' formatado.')


# %%
# Generate the report.

# Creates the report and summary HTML to be build at the end of
# this script.
print('Gerando o relatório...')
report_html = ''  # Empty HTML page string for report content.
report_html_summary = ''
item_name: str
item: dict
for item_name, item in sorted(evaluated_statistics.items()):
    # Check if the item category is included on a list in case of
    # partial report with only few item(s) category(ies).
    # It is important to remove the data because of the specific
    # messages test on next section of this script.
    if report_items_to_include and \
            item_name.lower() not in report_items_to_include:
        del item
        continue
    # If there is not any identify item for all units/school/institutes
    # or groups, does not create the specific section item chapter.
    if sum([item[u][STATISTIC_SUBNAMES['total']] for u in item.keys()]) == 0:
        print('\t"%s" não mencionado.' % item_name)
        continue

    print('\t"%s" para...' % item_name, end='')

    # Generate the report item chapter.
    chapter_id_name = html_section_id_name(item_name)
    report_html += '<article id="chapter"><h1><dummy id="%s">%s</dummy></h1></article>' % (chapter_id_name, item_name)
    report_html_summary += '<li><a href="#%s"></a></li>' % chapter_id_name

    # Generate each subchapter with the unit/school total of that item.
    sorted_items_by_unit = sort_by_units(item)
    unit_name: str
    item_unit: dict
    for unit_name, item_unit in sorted_items_by_unit.items():
        units_in_group = get_units_group(sorted_items_by_unit.keys(), unit_name)
        # Check if the item unit/school is included on a list in
        # case of partial report with only few item(s) school(s).
        # This partial report MUST included all totalization and
        # global analysis that the school/unit are present.
        # It is important to remove the data because of the specific
        # messages test on next section of this script.
        if report_units_to_include:
            if (unit_name not in report_units_to_include
                    and (unit_name in GROUPS_NAME_LIST
                         and all([u not in report_units_to_include for u in units_in_group]))):
                del item_unit
                continue
            if unit_name not in report_units_to_include and \
                    all([u not in units_in_group for u in report_units_to_include]):
                del item_unit
                continue
        # If there is not any identify item, does not create the
        # specific section.
        if item_unit[STATISTIC_SUBNAMES['total']] == 0:
            print(' *%s*' % unit_name, end='')
            if unit_name == list(sorted_items_by_unit.keys())[-1] or \
                    (report_units_to_include and unit_name == report_units_to_include[-1]):
                print('.')
            else:
                print(',', end='')
            continue

        print(' %s' % unit_name, end='')
        if unit_name == list(sorted_items_by_unit.keys())[-1] or \
                (report_units_to_include and unit_name == report_units_to_include[-1]):
            print('.')
        else:
            print(',', end='')

        # Metadata and style configuration, add the watermark
        # class to the contents, if configured to do.
        if configs.get('report') and configs['report'].get('add watermark'):
            report_html += '<article id="typography" class="%s"><h2><dummy id="%s">%s</dummy></h2>' % \
                ('watermark', html_section_id_name(unit_name, item_name), unit_name)
        else:
            report_html += '<article id="typography"><h2><dummy id="%s">%s</dummy></h2>' % \
                (html_section_id_name(unit_name, item_name), unit_name)
        # Add specific unit/institute/school scetion message if present.
        try:
            unit_message = configs['report']['section messages']['units'].get(unit_name)
            if unit_message:
                unit_message = evaluate_variables(dict(globals()), unit_message, True)
                report_html += '<p>' + unit_message + '</p>'
        except KeyError:
            pass

        # Add information of the units/school/institutes add
        # to the totalization analysis.
        # It not necesary to check the belonging, if the code
        # arrive here, `unit_name` have a group that contain
        # `report_units_to_include` or must include all.
        if unit_name in GROUPS_NAME_LIST:

            def unit_to_html(unit_name: str) -> str | None:
                '''Create the correpondent HTML string with text and hyperlink
                if the page exists from the unit/school name given.
                It is used to return `None` in case of this `unit_name` is a
                group name and add a marker in case of the correspondent page is
                not present (zero item quantity).
                '''
                # Do not include the group names on the listing
                # of unit that belongs to the group.
                if unit_name in GROUPS_NAME_LIST:
                    return None
                # Make a more clear report just adding the PDF section
                # hyperlinks in case of they have the target.
                if sorted_items_by_unit[unit_name][STATISTIC_SUBNAMES['total']] > 0:
                    section_id_unit_name = html_section_id_name(unit_name, item_name)
                else:
                    return '&#42;%s&#42;' % unit_name  # Use the asterisk HTML code.
                if section_id_unit_name and (not report_units_to_include
                                             or unit_name in report_units_to_include):
                    return '<a href="#%s">%s</a>' % (section_id_unit_name, unit_name)
                else:
                    return unit_name

            units_included = list(sort_by_units(item).keys())
            units_included.remove(unit_name)
            # Create a link in the name of each unit/school pointing to the unit-item section.
            unit_names_html = [u for u in list(map(unit_to_html, units_in_group)) if u]
            report_html += '<p>Unidades inclusas (%i): ' % len(unit_names_html)
            report_html += join_list_str(unit_names_html) + '.</p>'

            # Create the hyperlink for the not included units, if configurated to do so.
            if configs['report'].get('message not include'):
                units_not_in_group = set(units_included) - set(units_in_group)
                units_not_in_group = [u for u in list(map(unit_to_html, sorted(units_not_in_group))) if u]
                if units_not_in_group and len(units_not_in_group) <= configs['report'].get('message not include'):
                    report_html += '<p>Não inclusas (%i): ' % len(units_not_in_group)
                    report_html += join_list_str(units_not_in_group) + '.</p>'

        # Iterate through the analysis.
        analysis_name: str
        analysis_value: dict
        for analysis_name, analysis_value in item_unit.items():
            # Statistics values of each unit/institute.
            # The 'total' may be as specific contabilization of a sub
            # statistic of the item type (or other) in the pie chart /
            # percentage analysis, if it is present in both, show use
            # the second (sub statistic) for better report layout.
            if analysis_name == STATISTIC_SUBNAMES['total']:
                skip_current_statistic = False
                s_value: dict
                for s_value in item_unit.values():
                    if isinstance(s_value, dict):
                        if (STATISTIC_SUBNAMES['total'] in s_value.keys()
                                and analysis_value == s_value[STATISTIC_SUBNAMES['total']]):
                            skip_current_statistic = True
                            break
                if skip_current_statistic:
                    continue

            # If the statistic is empty or the only one is the missing data
            # and it equal to zero, do not add it to the report.
            if (not analysis_value
                    or (list(analysis_value.keys()) == [STATISTIC_SUBNAMES['missing']]
                        and analysis_value[STATISTIC_SUBNAMES['missing']] == 0)):
                continue

            # Get the name and unit of the statistic.
            match_name = RE_NAME_UNIT.search(analysis_name)
            statistic_name = match_name.group('name')
            if statistic_name == 'Tipo':
                statistic_name = 'Totalização'
            statistic_unit = match_name.group('unit') or get_default_unit(statistic_name)

            # Create the HTML page.
            report_html += '<section id="numbers"><h4>' + statistic_name + '</h4><dl>'
            chart_file_name = compose_chart_file_name([item_name, analysis_name, unit_name],
                                                      folder_path=path_temporary, check_exist=True)
            if chart_file_name:
                report_html += '<dd><dl>'  # Add additional column split.
            if not isinstance(analysis_value, dict):
                # Because of the Pandas spreadsheet read method some values
                # that not exist as statistics may be interpreted as `nan`
                # (not a number on the column). It is due the statistics storage
                # procedure adopted.
                if analysis_value and not pd.isnull(analysis_value):
                    if analysis_name.lower() == STATISTIC_SUBNAMES['total'].lower():
                        report_html += '<dt>' + 'Total' + '</dt>'
                    else:
                        report_html += '<dt>' + analysis_name.capitalize() + '</dt>'
                    report_html += '<dd>' + number_to_html(analysis_value, statistic_unit, 'Total') + '</dd>'
            else:
                analysis_name: str
                sub_analysis_name: str
                for sub_analysis_name, sub_analysis_value in analysis_value.items():
                    # NOTE
                    # By some reason, now, it read the integer of the year in a
                    # data statistic as a `float`. Before it did not happen.
                    try:
                        if (not isnan(sub_analysis_value)
                                and any(n in analysis_name.lower() or n in sub_analysis_name.lower() for n in ('data', 'ano'))):
                            sub_analysis_value = int(sub_analysis_value)
                    except TypeError:
                        pass
                    # Because of the Pandas spreadsheet read method some values
                    # that not exist as statistics may be interpreted as `nan`
                    # (not a number on the column). It is due the statistics storage
                    # procedure adopted.
                    if sub_analysis_value and not pd.isnull(sub_analysis_value):
                        report_html += '<dt>' + sub_analysis_name + '</dt>'
                        report_html += '<dd>' + number_to_html(sub_analysis_value, statistic_unit,
                                                               sub_analysis_name, extra_data=item_unit) + '</dd>'
            # Add the correspondent analysis chart, if it exists.
            if chart_file_name:
                report_html += '</dd><dt>' + image_to_html(chart_file_name, 1.0) + '</dt></dl>'
            report_html += '</dl></section>\n'

            # Add the classification chart, if it exists (only generated on group analysis).
            chart_file_name = compose_chart_file_name(
                [item_name, analysis_name, unit_name, 'classificação'], check_exist=True)
            if chart_file_name:
                report_html += image_to_html(chart_file_name, 1.0)

        # Charts are add after numeric statistics summary.
        # for analysis_name, analysis_value in item_unit.items():
        #    chart_file_name = compose_chart_file_name([item_name, analysis_name, unit_name])
        #    report_html += image_to_html(chart_file_name)
        # for analysis_name, analysis_value in item_unit.items():
        #    chart_file_name = compose_chart_file_name(
        #        [item_name, analysis_name, unit_name, 'classificação'], check_exist=True)
        #    if chart_file_name:
        #        report_html += image_to_html(chart_file_name, 1.0)

        # End of the page of this unit/institute.
        report_html += '</article>'

print('Criado HTML para compilação.')


# %%
# Save the report.

print('Compilando o HTML...')

print('\tCarregando template...', end='')

# Add the front cover, styles and definitions
with open(resource_path_join('report.html'), 'r') as cover:
    report_html_cover = cover.read()

print(' carregado.')

# Add some aditional informaiton used in the analysis into the
# "Editotal" text.

print('\tConseguindo estatísticas do próprio software...', end='')

# Capture the total programming lines of this application and other
# specific software meta statistics.
software_total_lines = os.popen('make meta').read()
software_total_lines = re.findall(r'(\d+)\s+total', software_total_lines, re.IGNORECASE)[0]
software_total_lines = number_to_html(int(software_total_lines))
report_html_cover = report_html_cover.replace(r'{{SOFTWARE_TOTAL_LINES}}', software_total_lines)

print(' análise terminada.')

print('\tSubstituindo variáveis...', end='')

report_html_cover = report_html_cover.replace(r'{{DATE}}', date_now.strftime(r'%d/%m/%Y'))
report_html_cover = report_html_cover.replace(r'{{COLUMN_NAME_SEARCHER}}', COLUMN_NAME_SEARCHER)
report_html_cover = report_html_cover.replace(r'{{COLUMN_NAME_MATCH}}', COLUMN_NAME_MATCH)
report_html_cover = report_html_cover.replace(
    r"{{STATISTIC_SUBNAMES['projected']}}", STATISTIC_SUBNAMES['projected'])
report_html_cover = report_html_cover.replace(
    r"{{STATISTIC_SUBNAMES['projected mode']}}", STATISTIC_SUBNAMES['projected mode'])
items_name_message = map(report_link, [e.capitalize() for e in sorted(items_category.keys())])
report_html_cover = report_html_cover.replace(r'{{ITEMS_TO_ANALYZE}}',
                                              join_list_str(items_name_message, pre_mark='<b>', post_mark='</b>'))

print(' substituídas.')

# Add some saying into the "Editorial" for specific analysis.


def is_statistic_in(statistic_name: str) -> bool:
    '''Check if a statistic name was evaluated for the before
    script (`performe_analysis.py`) and saved at the temporary
    spreadsheet (`Análise geral.xlsx`) read at the beginning of
    this script.'''
    name_item: str
    item: dict
    for name_item, item in evaluated_statistics.items():
        # Check if the item is included in the report or not.
        if (not report_items_to_include
                or (report_items_to_include and name_item.lower() in report_items_to_include)):
            item_unit: dict
            for item_unit in item.values():
                if statistic_name in item_unit.keys():
                    return True
    return False


def is_unit_in(unit_name: str) -> bool:
    '''Check if a unit/institute name is present into the data
    evaluated for the before script (`performe_analysis.py`) and
    saved at the temporary spreadsheet (`Análise geral.xlsx`)
    read at the beginning of this script.'''
    item: dict
    for item in evaluated_statistics.values():
        if not report_units_to_include or \
                unit_message.upper() in report_units_to_include:
            if unit_name.upper() in item.keys():
                return True
    return False


print('\tAdicionando texto sobre analises realizadas...', end='')

# Add specific messages at the beginning of the report.
statistic_specific_saying = ''
if configs.get('report') and configs.get('report').get('messages'):
    configs_messages: dict = configs.get('report').get('messages')
    # Statistics messages.
    configs_messages_statistics: dict = configs_messages.get('statistics')
    if configs_messages_statistics:
        name: str
        message: str
        for name, message in configs_messages_statistics.items():
            if is_statistic_in(name):
                statistic_specific_saying += '<p>' + message + '</p>'
    # Items messages.
    configs_messages_items: dict = configs_messages.get('items')
    if configs_messages_items:
        name: str
        message: str
        for name, message in configs_messages_items.items():
            if name.capitalize() in evaluated_statistics.keys() and \
                    (not report_items_to_include or name in report_items_to_include):
                statistic_specific_saying += '<p>' + message + '</p>'
    # Units/schools messages.
    configs_messages_units = configs_messages.get('units')
    if configs_messages_units:
        for name, message in configs_messages_units.items():
            if is_unit_in(name) and (not report_units_to_include
                                     or name in report_units_to_include or name in GROUPS_NAME_LIST):
                statistic_specific_saying += '<p>' + message + '</p>'
# Evaluate variable by string between double braces, e.g. `'{{foo}}`.
statistic_specific_saying = evaluate_variables(dict(globals()), statistic_specific_saying, True)
if statistic_specific_saying:
    report_html_cover = report_html_cover.replace(r'<!--SPECIFIC ANALYSIS SAYING-->', statistic_specific_saying)
if cover_message:
    report_html_cover = report_html_cover.replace(r'<!--COVER MESSAGE-->', cover_message)
with open('articles.txt') as f_articles:
    articles = f_articles.read().splitlines()
    for idx in reversed(range(len(articles))):
        if not articles[idx]:
            del articles[idx]
    if articles:
        articles_html = '<p>Artigos frutos do trabalho:<ul>'
        for article in articles:
            if article:
                url = get_link_from_string(article)
                if url:
                    article = article.replace(url, '<a href="{url}">{url}</a>'.format(url=url))
                articles_html += '<li>' + article + '</li>'
        articles_html += '</ul></p>'
        report_html_cover = report_html_cover.replace(r'<!--ARTICLES TO CITE-->', articles_html)

print(' textos adicionados.')

# Get the HTML raw string of the report content if I had used
# `BeautifulSoup` package to help the page assembly.
if 'bs' in globals() and isinstance(report_html, bs):
    report_html = str(report_html.prettify())
report_html = report_html_cover.replace(r'<!--REPORT CONTENT-->', report_html)
if 'bs' in globals() and isinstance(report_html_summary, bs):
    report_html_summary = str(report_html_summary.prettify())
report_html = report_html.replace(r'<!--REPORT SUMMARY-->', report_html_summary)

print('\tCarregando motor de compilação PDF...')

# Compile the PDF.
base_path = os.path.dirname(__file__)
printer = weasyprint.HTML(string=report_html,
                          base_url=base_path)

print('\t\tCarregando estilo do documento e fontes...', end='')
if parse_version(weasyprint.__version__) >= parse_version('53.0'):
    font_config = weasyprint.text.fonts.FontConfiguration()
else:
    font_config = weasyprint.fonts.FontConfiguration()
report_css = [
    weasyprint.CSS(filename=resource_path_join('report.css'), font_config=font_config),
    # weasyprint.CSS(filename=resource_path_join('report.sass'), font_config=font_config),
    # weasyprint.CSS(filename=resource_path_join('report.css.map'), font_config=font_config),
]
print(' carregado.')

# Generate additional information need for HTML->PDF compilation.
print('\t\tCarregando arquivos extras...', end='')
extra_files = []
# extra_files += glob.glob(path_temporary + os.sep + '*')
print(' carregado.')

# report_page = weasyprint.document.pages

# print('\tSalvando relatório HTML...', end='')
# if 'bs' in globals() and isinstance(report_html, bs):
#     report_html = str(report_html.prettify())
# if 'bs' not in globals():
#     try:
#         from bs4 import BeautifulSoup as bs
#         report_html = bs(markup=report_html)
#         report_html = str(report_html.prettify())
#     except ImportError:
#         pass
# report_html = re.sub(r'<!--((.|\s)*?)-->', '', report_html)
# with open(os.path.splitext(file_name_report)[0] + '.html', 'w+') as html_file_handle:
#     html_file_handle.write(report_html)
# print(' salvo!')

print('\tGerando arquivo PDF...', end='')
if parse_version(weasyprint.__version__) >= parse_version('60.0'):
    printer.write_pdf(file_name_report,
                      font_config=font_config,
                      stylesheets=report_css,
                      optimize_images=True,
                      presentational_hints=True)
elif parse_version(weasyprint.__version__) >= parse_version('53.0'):
    printer.write_pdf(file_name_report,
                      font_config=font_config,
                      stylesheets=report_css,
                      optimize_size=('fonts', 'images',),
                      presentational_hints=True)
else:
    printer.write_pdf(file_name_report,
                      font_config=font_config,
                      stylesheets=report_css,
                      # attachments=extra_files,
                      optimize_images=True,  # Optimize the image size (resolution into the PDF?).
                      presentational_hints=True)  # Follow the image aspects given into the HTML.

print(' gerado!')

print('Compilação de dados terminada.')
