#!python
# This file is covered tby GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide specific definitions for searh and scrape
# routers/switches/acess-points ("rodeadores e WiFi").
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import re

# Required but not default libraries.
# import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
from items_general_info import *
from analysis_chart import *
from general_definitions import UNIDENTIFIED_TYPE, _, print


# %%
# Functions to get data from the original databases.

def get_router_type(item_idx: int,
                    category: str,
                    row: pd.DataFrame,
                    found_statistics: dict[str, Any] = {},
                    column_search: str = 'Descrição') -> str:
    '''Get the router type.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled in 'Modelo'.

    Return the router type (str).
    '''

    def is_match_type(pattern: str) -> bool:
        '''Get the type on the description.

        Tip: Always check the most specific category before the
        most generic one, because the first one may have the
        second word/noun as part of it description.

        Parameter pattern (str).

        Return (bool).'''
        match = re.search(pattern, row[column_search], re.IGNORECASE)
        return match

    # Capture the number of ports if it exist
    num_ports = is_match_type(r'((\d+[\ \+]+)*\d+)'
                             r'[\ \-]*(g(iga(bit)*)*)*[\ \-]*'
                             r'('
                             r'por\-*t(as*)|p\.*|g(iga)*'  # |x( *\d+)
                             r'(\.|\s|$)'
                             r')')
    num_ports = num_ports or is_match_type(r'(\d+)[\.\s]*$')
    if num_ports:
        try:
            num_ports = int(num_ports[1])
        except ValueError:
            # Possible still a string inside as '\d + \d'.
            ports = re.findall(r'\d+', num_ports[1])
            num_ports = sum([int(p) for p in ports])
    else:
        num_ports = None

    # Order for search here is important, it must check first
    # the less generic words because the groups formed by
    # such words may contain the generic words.
    if is_match_type(r'(acc*ess* +point|ponto +de +acesso'
                    r'|anten+a|wirel+ess*|sem +fio|band)'):
        return _('access point')
    if is_match_type(r'(vitualiza[cç\?][aã\?]o'
                    r'|ser(vidor|v*swi(tch|cth|cht))'
                    r'|armazenamento)'):
        return _('serviços')
    if is_match_type(r'(wifi)'):
        return _('comum')
    if is_match_type(r'(slots|gerenci[aá\?]vel|protocolo'
                    r'|camada|cisco|chass*is|desempenho)'):
        return _('estrutura')
    if num_ports and num_ports > 8:
        return _('estrutura')
    return _('comum')

    print(text_warning_missing(row, 'Tipo', category))
    return UNIDENTIFIED_TYPE


# %%
# References to functions for scrape data from the original databases
# and statistics and chart analysis.

# Scrape description for the property description item.
# The main key is 'category name (sub category name as optional)'.
_category_name = 'roteadores de rede'
items_definitions.update({
    _category_name: {
        # This search partners will be saved as columns at the
        # final spreadsheet file. The order specified here may
        # not affect the columns order of the spreadsheet, it
        # must be set on the save routine.
        'desc_match': r'(?:roteador|swi(?:tch|cth|cht)|acc*ess* +point|ponto +de +acesso)',
        'desc_not_match': r'(?:v[ií\?]deo|monitor|bandeja|sensores|medidor|modulador'
                          r'|[rh]ack|mesa|controlador +de +acesso|condicionador(?: +de +ar)*'
                          r'|notebook|teclado|c[aâ\?]mera'
                          r'|estabilizador|fonte +de +alimenta[cç\?][aã\?]o'
                          r'|gravador|equalizador|amplificador|microfone|som|refletor|interruptor)',
        # List of groups that may belongs to this category. They
        # are used for double match and report inconsistences
        # in the original database procedure (as already reported).
        'groups_check': [86],
        # References to functions of scrap data.
        'search': {
            'Tipo': get_router_type,
            'Data de compra': get_purchase_data,
            'Localização': get_location,
        },
        # References to functions for statistic and chart analysis.
        'analysis': {
            # "Tipo" must be the first analysis because it result may be used into other charts.
            'Tipo': analyze_percentage_chart,
            'Data de compra': analyze_discrete_chart,
            # 'Saldo Contábil': analyze_scatter_chart,
        }
    }
})


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    from items_testcase import test_all
    test_all(__file__, items_definitions)
