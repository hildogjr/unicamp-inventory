#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide specific definitions for searh and scrape
# fridges, freezers, ... ("geladeiras e freezers").
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import re

# Required but not default libraries.
# import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# from numba import jit

# Internal libraries.
from items_general_info import *
from analysis_chart import *
from general_definitions import UNIDENTIFIED_TYPE, _, print


# %%
# Functions to get data from the original databases.

def get_fridge_type(item_idx: int,
                    category: str,
                    row: pd.DataFrame,
                    found_statistics: dict[str, Any] = {},
                    column_search: str = 'Descrição') -> str:
    '''Get the fridge type.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled in 'Modelo'.

    Return the fridge type (str): geladeira, frigobar, freezer...
    '''

    def is_match_type(pattern: str) -> bool:
        '''Get the type on the description.

        Tip: Always check the most specific category before the
        most generic one, because the first one may have the
        second word/noun as part of it description.

        Parameter pattern (str).

        Return (bool).'''
        match = re.search(pattern, row[column_search], re.IGNORECASE)
        return match

    RE_FROST_FREE = r'(degelo|fr*ost*[\ \-]*free|defrost)'
    # Order for search here is important, it must check first
    # the less generic words because the groups formed by
    # such words may contain the generic words.
    if is_match_type(r'(purificador( +(de +)*[aá\?]gua)*)'):
        return _('bebedouro')
    if is_match_type(r'(m[aá\?]quina +de +)gelo'):
        return _('máquina de gelo')
    # "frigobar" before "geladeira" because "geladeira tipo
    # frigobar" and other descriptions of this kind.
    if is_match_type(r'frigo[\.\-]*bar'):
        return _('frigobar')
    if is_match_type(r'([rs]efrigerador|geladeira)'):
        if is_match_type(RE_FROST_FREE):
            return _('geladeira frost free')
        return _('geladeira')
    if is_match_type(r'(free*zer|congeladora*)'):
        if is_match_type(r'(ultra)'):
            return _('ultra congelador')
        if is_match_type(RE_FROST_FREE):
            return _('congelador frost free')
        return _('congelador')
    if is_match_type(r'frigor[íi\?]fic[oa]'):
        return _('frigorífico')
    if is_match_type(r'(bebedouro|[aá\?]gua)'):
        return _('bebedouro')

    print(text_warning_missing(row, 'Tipo', category))
    return UNIDENTIFIED_TYPE


# %%
# References to functions for scrape data from the original databases
# and statistics and chart analysis.

# Base to ignore.
base_ignore = (
    r'(?:sistema|unidade|solda|laser|espa[cç\?]o'
    r'|[cg]riorefrigerador|descongelador|transformador'
    r'|controlador|monitor|centr[ií\?]fuga|nitrog[eê\?]nio'
    r'|circulador|evaporador|banho|chiller|compressor'
    r'|m[oó\?]vel|bancada|arm[áa\?]rio|dep[oó\?]sito'
    r')'
)

# Scrape description for the property description item.
# The main key is 'category name (sub category name as optional)'.
_category_name = 'refrigeradores'
items_definitions.update({
    _category_name + ' (geral)': {
        # This search partners will be saved as columns at the
        # final spreadsheet file. The order specified here may
        # not affect the columns order of the spreadsheet, it
        # must be set on the save routine.
        'desc_match': r'(?:[rs]efrigerador|geladeira|frigobar'
                      r'|free*zer|congeladora*|frigor[íi\?]fic[oa]'
                      r'|(?:m[aá\?]quina +de +)gelo)',  # |resfriador)',
        'desc_not_match': base_ignore,
        # List of groups that may belongs to this category. They
        # are used for double match and report inconsistences
        # in the original database procedure (as already reported).
        'groups_check': [41, 46, 65, 71, 73],
        # References to functions of scrap data.
        'search': {
            'Tipo': get_fridge_type,
            'Data de compra': get_purchase_data,
            'Selo PROCEL': get_procel_stamp,
            'Localização': get_location,
            'Aquisição': get_incorporation_method,
        },
        # References to functions for statistic and chart analysis.
        'analysis': {
            # "Tipo" must be the first analysis because it result may be used into other charts.
            'Tipo': analyze_percentage_chart,
            'Data de compra': analyze_discrete_chart,
            'Selo PROCEL': analyze_percentage_chart,
            'Aquisição': analyze_percentage_chart,
            # 'Saldo Contábil': analyze_scatter_chart,
        }
    }
})

# Some especific rules to "Bebedouros".
items_definitions[_category_name + ' (bebedouros)'] = \
    items_definitions[_category_name + ' (geral)'].copy()
items_definitions[_category_name + ' (bebedouros)']['desc_match'] = (
    r'(?:[aá\?]gua refrigerad[oa]|bebedouro'
    r'|[aá\?]gua +refrigerad[oa]|purificador +de +[aá\?]gua)'
)
# Use all above rules plus some specific to not consider "bebedouro para animais".
items_definitions[_category_name + ' (bebedouros)']['desc_not_match'] = (
    items_definitions[_category_name + ' (geral)']['desc_not_match'][:-1]
    + r'|anima(?:l|is)|camundongos*|ratos*|gatil|esp[eé\?]cie'
    + r'|an[áa\?]li[sz]e)'
)

# Update the "geladeiras/refrigeradores" to not match with "bebedouros".
items_definitions[_category_name + ' (geral)']['desc_not_match'] = (
    items_definitions[_category_name + ' (geral)']['desc_not_match'][:-1]
    + r'|' + items_definitions[_category_name + ' (bebedouros)']['desc_match'][3:]
)


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    from items_testcase import test_all
    test_all(__file__, items_definitions)
