#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide specific definitions for search and scrape
# uninterrupted power system ("no-breaks").
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import re

# Required but not default libraries.
# import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# from numba import jit

# Internal libraries.
from items_general_info import *
from analysis_chart import *
from general_definitions import UNIDENTIFIED_TYPE, _, print


# %%
# Functions to get data from the original databases.

def get_ups_type(item_idx: int,
                 category: str,
                 row: pd.DataFrame,
                 found_statistics: dict[str, Any] = {},
                 column_search: str = 'Descrição') -> int:
    '''Get the UPS type/kind.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled in 'Modelo'.

    Return (str) UPT type: senoidal, dupla conversão, online,
        offline, line interactive.
    '''

    def is_match_type(pattern: str) -> bool:
        '''Get the type on the description.

        Tip: Always check the most specific category before the
        most generic one, because the first one may have the
        second word/noun as part of it description.

        Parameter pattern (str).

        Return (bool).'''
        match = re.search(pattern, row[column_search], re.IGNORECASE)
        return match

    # Order for search here is important, it must check first
    # the less generic words because the groups formed by
    # such words may contain the generic words.

    # The identified "estabilizador" should always return this.
    if row.get('Tipo') == 'estabilizador':
        return _('estabilizador')

    # Line interactive topology are always senoidal type,
    # so it must be match-tested before.
    if is_match_type(r'(?:(?:line|linha)[\s\-]*interac*t *iv[ea])'):
        return _('line interactive')
    if is_match_type(r'(?:dupl[ao] (?:convers[aã\?]o)|(?:\s|^)on[\ \-]*line(?![a-z]))'):
        if is_match_type(r'(?:senoidal|sin(\ |$|\.))'):
            return _('senoidal online')
        return _('online')
    if is_match_type(r'(?:senoidal|sin(\ |$|\.))'):
        return _('senoidal')
    # if is_match_type(r'(on[\ \-]*line)'):
    #    return _('online')
    if is_match_type(r'(?:estabilizador)'):
        return _('estabilizador')
    return _('offline')

    print(text_warning_missing(row, 'Tipo', category))
    return UNIDENTIFIED_TYPE


# %%
# References to functions for scrape data from the original databases
# and statistics and chart analysis.

# Scrape description for the property description item.
# The main key is 'category name (sub category name as optional)'.
_category_name = 'nobreaks'
items_definitions.update({
    _category_name + ' (geral)': {
        # This search partners will be saved as columns at the
        # final spreadsheet file. The order specified here may
        # not affect the columns order of the spreadsheet, it
        # must be set on the save routine.
        'desc_match': r'(?:ups|no[-\ ]*break(?![a-z])'
                      r'|conversor.+est[aá\?]tico.+(?:micro)*computador)',
        'desc_not_match': r'(?:cancela|espectr[oô]metro|[oó\?]tic[oa]|rel[oó]gio'
                          r'|c[aâ\?]mera|foto|filmadora|anal[oó\?]gic[oa]'
                          r'|mobili(?:a|[aá\?]rio)|[rh]ack|bancada|estante|suporte|gabinete|mesa|carrinho'
                          r'|arm[aá\?]rio|escrivaninha|p[eéê\?]s +|nivelador(?:a|es)*'
                          r'|an[aá\?]li[sz]e|rastreador|receptor|gps|m[aá\?]quina'
                          r'|pneum[aá]tico|esteira|calibrador)',
        # List of groups that may belongs to this category. They
        # are used for double match and report inconsistences
        # in the original database procedure (as already reported).
        'groups_check': [86, 61, 59],
        # References to functions of scrap data.
        'search': {
            'Tipo': get_ups_type,
            'Data de compra': get_purchase_data,
            'Potência elétrica (VA)': get_electric_power,
            'Localização': get_location,
        },
        # References to functions for statistic and chart analysis.
        'analysis': {
            # "Tipo" must be the first analysis because it result may be used into other charts.
            'Tipo': analyze_percentage_chart,
            'Data de compra': analyze_discrete_chart,
            'Potência elétrica (VA)': analyze_continuous_chart,
            # 'Saldo Contábil': analyze_scatter_chart,
        }
    }
})

# Specific sub-category of computer and other small autotransformer stabilizers.
items_definitions[_category_name + ' (estabilizadores)'] = \
    items_definitions[_category_name + ' (geral)'].copy()
# Without the termination could match with "estabilizadora ...".
items_definitions[_category_name + ' (estabilizadores)']['desc_match'] = r'(?:estabilizador(?![a-z]))'
items_definitions[_category_name + ' (estabilizadores)']['desc_not_match'] = (
    items_definitions[_category_name + ' (geral)']['desc_not_match'][:-1]
    # Suffixes used to exclude the item from this subcategory.
    + r'|' + items_definitions[_category_name + ' (estabilizadores)']['desc_match'] + r' +(?:para|p\/).*(?:quadr[oi])'
    # Void "No-break com estabilização de ?voltagem?..."
    + r'|ups|no[-\ ]*break(?![a-z])'
    + r')'
)


# Main entry, used just for debug.
if __name__ == "__main__":
    from items_testcase import test_all
    test_all(__file__, items_definitions)
