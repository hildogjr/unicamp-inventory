#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide the functions for search data in the used databases
# and merge utilities.
# This was the first file written in the project with
# `search_fun_by_unit` as the first function to test the idea
# feasibility. At that time, some employee of UNICAMP was
# passing me each institute data, she didn't known to use the
# system web site to export data without put a filter.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['search_fnc_by_unit', 'search_fnc', 'merge_units']

# Base libraries.
import os
import re
from datetime import datetime
# import itertools
from typing import Any
import warnings
from multiprocessing import Pool
from functools import partial

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
from general_definitions import (configs, GENERAL_SEP,
                                 get_units_group, COLUMN_UNIT_NAME,
                                 COLUMN_NAME_MATCH, COLUMN_NAME_SEARCHER,
                                 MULTIPROCESSING_CORE_USAGE_QTY, print)
from format_output import evaluate_variables, join_list_str
from items_definitions import items_definitions, items_category

date_now = datetime.now()


# Ignore the error "This pattern is interpreted as a regular expression, and has match groups.
# To actually get the groups, use str.extract." caused by `pandas.DataFrame.str.contains`
# execution which wants to capture not need RegEx groups.
warnings.filterwarnings('ignore', r'This pattern is interpreted as a regular expression[\w\s\.\,]*')


# %%
# Define the merge functions.

def merge_units(database: dict[str, pd.DataFrame],
                group_unit_name: str) -> pd.DataFrame:
    '''Merge the units/institutes itens (or by other rules: build location /
    administrative sector) into a unique `pd.DataFrame`.

    Parameters:
        Dictionary with name of units as keys and DataFrame as values.
        Group name expressed on the `configurations.yml` definitions.

    Return the merged database.
    '''
    # Get the units/institutes that belong to the current in analysis group.
    units_belongs_group = get_units_group(database.keys(), group_unit_name)
    if not units_belongs_group:
        # If no units/institutes belong to the group (may be not read
        # the needed data base) or the group group are not the first
        # that list the same units/institutes into a group, keep it
        # will generate duplicated (and not useful) data.
        # print('\nGroup "%s" list missing units (origin database), '
        #      'or it have a content duplicated of the first groups.' %
        #      group_unit_name)
        print(f'\nGrupo "{group_unit_name}" com unidades faltantes no banco de dados, '
              'ou com conteúdo duplicado dos grupos anteriores.')
        return pd.DataFrame()  # Empty DataFrame.

    # Create the complete database of this current group.
    grouped_database = pd.DataFrame()
    for unit_name in units_belongs_group:
        # Add a column for identify the origin of the data.
        db = database[unit_name]
        if COLUMN_UNIT_NAME in db.keys():
            db[COLUMN_UNIT_NAME] = unit_name
        else:
            db.insert(0, value=unit_name, column=COLUMN_UNIT_NAME)
        # Equivalente result of above but keep warning because of memory implementation.
        # data[COLUMN_UNIT_NAME] = unit_name  # Doesn't need to do `[unit_name] * len(db)`.
        # Append/create the `grouped_database` for bellow analysis.
        if grouped_database.empty:
            grouped_database = db
        else:
            # grouped_database = grouped_database.append(db)
            grouped_database = pd.concat([grouped_database, db], axis=0, ignore_index=True)
    if len(grouped_database) == 0:
        print(' NENHUM item.')
        return pd.DataFrame()  # Empty DataFrame.

    return grouped_database


# %%
# Define the search function.

def __search_fnc_by_unit(unit: str, database: pd.DataFrame) -> tuple[pd.DataFrame, dict]:
    # Iterate through each unit identified to keep compatibility between
    # the two kinds of exported spreadsheets and provide better report.
    print(f'Analisando unidade "{unit}"...')
    database_unit = database[database['Área de Patrimônio'] == unit]
    print('\n\n')
    return search_fnc(database_unit)


def search_fnc_by_unit(database: pd.DataFrame,
                       units: str | list | set | None = None) -> tuple[dict, dict]:
    '''Execute the `search_func` search by each unit first. This is useful
    for create the log file and track results.

    Parameters:
        Database to search.
        List of units (optional: used sorted list of all present).

    Return: dicts with the database of found items and statistic information.
    '''
    if not units:
        units = sorted(set(database['Área de Patrimônio']))
    items_found_db = {n: {} for n in items_definitions.keys()}
    items_found_statistic = {n: {} for n in items_definitions.keys()}

    if MULTIPROCESSING_CORE_USAGE_QTY == 1:
        for unit in units:
            # if unit != 'CECOM':
            #    continue
            unit_found, unit_statistic = __search_fnc_by_unit(unit, database)

            for items_name in items_definitions.keys():
                items_found_db[items_name][unit] = unit_found[items_name]
                items_found_statistic[items_name][unit] = unit_statistic[items_name]
    else:
        with Pool(processes=MULTIPROCESSING_CORE_USAGE_QTY) as cpu_processing:
            results = cpu_processing.map(partial(__search_fnc_by_unit, database=database), units)

        for idx, (unit_found, unit_statistic) in enumerate(results):
            for items_name in items_definitions.keys():
                items_found_db[items_name][units[idx]] = unit_found[items_name]
                items_found_statistic[items_name][units[idx]] = unit_statistic[items_name]

    return items_found_db, items_found_statistic


def search_fnc(database: pd.DataFrame) -> tuple[pd.DataFrame, dict]:
    ''' Function to search and extract parameters from items.

    Parameter: Database to search.

    Return: dicts with the database of found items and statistic information.
    '''
    items_found_db: dict[str, pd.DataFrame] = {}
    items_found_statistic: dict[str, Any] = {}
    item_name: str
    item_pattern: dict[str, str]
    for item_name, item_pattern in items_definitions.items():
        # Analyses for each item pattern.
        items_found_db[item_name] = {}
        items_found_statistic[item_name] = {}
        item_found_statistic = items_found_statistic[item_name]
        print('Procurando por "%s"...' % item_name, end='')
        # usando padrão "%s" e não "%s"' % (item_name, item_pattern['desc_match'], item_pattern['desc_not_match']))

        # Filter by the description match pattern.
        # = filter(item_pattern['desc_match'].match, database['Descrição'])
        found = database['Descrição'].str.contains(item_pattern['desc_match'], regex=True, flags=re.I)
        database_found = database.loc[found]
        # Exclude the description by the `not_match` pattern, if it exists.
        if item_pattern.get('desc_not_match'):
            found_exclude = database_found['Descrição'].str.contains(item_pattern['desc_not_match'], regex=True, flags=re.I)
            # Take only the rows that didn't match with the above pattern for exclusion.
            if sum(found_exclude) > 0:
                database_found = database_found[~found_exclude]
                # database_found = pd.merge(database_found,
                #                          database_found.loc[found_exclude], how='left')
        found_qty_initial = database_found.shape[0]
        if found_qty_initial == 0:
            print('SEM REGISTRO')
            # continue
        else:
            print('')

        # Remove the not active items by selecting only the ones that
        # does not have any description of sell/end of concession
        # or date/process of this procedure.
        # database_found_antigo = database_found
        # pd.concat([database_found, database_found_antigo]).drop_duplicates(keep=False)
        # database_found = database_found.query('`Fim de Comodato`.isnull() and '
        #                                      '`Fim de Cessão`.isnull() and '
        #                                      '`Motivo Baixa`.isnull() and '
        #                                      '`Data Baixa`.isnull() and '
        #                                      '`Processo de Baixa`.isnull()')
        #
        # Second Meire Terezinha Carioca Aoki <meire.aoki@dga.unicamp.br> , the reasons of "baixa" can be:
        # - A inservibilidade ocasionada por quebra, desgaste, avaria ou morte;
        # - A alienação por doação, venda ou permuta;
        # - O extravio;
        # - O sinistro;
        # - A transformação;
        # - A dispensa de patrimoniamento;
        # - A incorporação indevida;
        # - A devolução do bem ao proprietário;
        # - O fim de cessão quando bens de terceiros.
        #
        # Check the data for validate and exclude instead of just exclude
        # the existent ones (when the field were filled as before, code above).
        items_to_remove = []
        check_not_empty = [n.lower() for n in configs['identify']['check'].get('not empty', [])]
        for item_id, item_data in database_found.iterrows():
            # Not empty (not optional / mandatory) data value presence,
            # including some special case of date such.
            missing_columns = []

            # itertools.compress(check_not_empty, item_data.apply(check_empty))
            for data_name, data in item_data.items():
                if data_name.lower() in check_not_empty and pd.isnull(data):
                    missing_columns.append(data_name)
            if missing_columns:
                print('\tItem %i falta: %s.' % (item_data['Identificador'],
                                              join_list_str(missing_columns, pre_mark='"', post_mark='"')))
            # Validate some data (most of them date and non-empty case).
            for data_name, data in item_data.items():
                # Specific validation for data values.
                if 'data' in data_name.lower():
                    if not pd.isnull(data):
                        date = pd.to_datetime(data, errors='coerce')  # , format='%d/%m/%Y')
                        if pd.isnull(date):
                            print(f'\tItem {item_id} com erro na "Data" ({data}).')
                        else:
                            for data_column_sub in configs['identify']['check'].get('date consistence', []):
                                if data_column_sub.lower() in data_name.lower():
                                    # Other that can be greater:
                                    # ['Data Garantia', 'Fim de Cessão', 'Fim de Comodato', 'Data Baixa', ]
                                    if date > date_now:
                                        print('\tItem %i com erro na "Data de %s" (%s).' %
                                              (item_id, data_column_sub, data))

            # Remove second rule of dates.
            def check_item_removal(column_check: str | list[str]) -> tuple[str, str]:
                ''' Check item to remove from analysis by the data on an specific column.

                Parameter: Column name.

                Return: Removal reason and additional information.
                '''
                if isinstance(column_check, list):
                    # If it is a list of columns, use the recursive process.
                    reason_remove = []
                    additional_info = []
                    for c in column_check:
                        r, a = check_item_removal(c)
                        if r:
                            reason_remove.append(r)
                            additional_info.append(a)
                    return GENERAL_SEP.join(reason_remove), GENERAL_SEP.join(additional_info)
                elif isinstance(column_check, str) and column_check in item_data.keys():
                    if pd.notnull(item_data[column_check]):
                        try:
                            # Try to check if it is a data checking.
                            date = datetime.strptime(item_data[column_check], r'%d/%m/%Y')
                            if date > date_now:
                                return '', ''  # Item not excluded for the current date.
                        except ValueError:
                            # The value exist and the item will be excluded.
                            pass
                        additional_info = item_data[column_check]
                        return column_check, additional_info
                    return '', ''
                else:
                    return '', ''

            reason_remove = []
            additional_info = []
            for column in configs['identify']['check'].get('check to exclude', []):
                r, a = check_item_removal(column)
                if r:
                    reason_remove.append(r)
                    additional_info.append(a)
            reason_remove = GENERAL_SEP.join(reason_remove)
            additional_info = GENERAL_SEP.join(additional_info)
            if reason_remove:
                # database_found.loc[item_id,:]
                # database_found.drop(index=item_id)
                items_to_remove.append(item_id)
                print('\tItem %i desconsiderado: "%s" %s.' %
                      (item_data['Identificador'], reason_remove, additional_info))
        database_found = database_found.drop(items_to_remove)

        item_found_statistic['collected'] = found_qty_initial - database_found.shape[0]
        if found_qty_initial > database_found.shape[0]:
            print('Não contabilizado %i "%s" por constarem com não ativos.' %
                  (item_found_statistic['collected'], item_name))
        item_found_statistic['qty'] = database_found.shape[0]
        if item_found_statistic['qty'] == 0 and found_qty_initial > 0:
            print('\tTodos "%s" foram DESCONSIDERADOS.' % item_name)
            # continue

        # Allocate the output variables by identifying the information
        # that will be stored (configured on the file `configurations.yml`).
        columns_info = list(item_pattern['search'].keys())
        columns_name = []
        variables = dict(globals())
        variables.update(dict(locals()))
        for column_name in configs['identify']['info to save']:
            column_name = evaluate_variables(variables, column_name)
            if isinstance(column_name, str):
                columns_name.append(column_name)
            elif isinstance(column_name, list):
                columns_name += column_name
        del variables
        items_found_db[item_name] = pd.DataFrame([], columns=columns_name)

        # Get only the reference where was found the items and iterate
        # through all found items scraping the asked informations.
        for _, db_row in database_found.iterrows():
            if isinstance(item_pattern['desc_match'], re.Pattern):
                item_pattern_re = item_pattern['desc_match']
            else:
                item_pattern_re = re.compile(item_pattern['desc_match'], re.IGNORECASE)
            category_answer: str = item_pattern_re.findall(db_row['Descrição'])[0]
            if isinstance(category_answer, tuple):
                # If the regular expression match has sub-matches
                # of internal groups, use the first group/word to
                # identify the category match.
                category_answer = category_answer[0]
            category_answer = category_answer.strip()

            _item_found_db: dict[str, Any] = {}
            for column_name in items_found_db[item_name].keys():

                # Add the algorithm match information for developer track and debug.
                if column_name == COLUMN_NAME_SEARCHER:
                    _item_found_db.update({COLUMN_NAME_SEARCHER: [item_name.lower()]})
                elif column_name == COLUMN_NAME_MATCH:
                    # Information of item match on my algorithm to track and debug.
                    _item_found_db.update({COLUMN_NAME_MATCH: [category_answer.lower()]})

                # Add some statistic about the item. Most of them scraped in the
                # description information ("Descrição") or model ("Modelo").
                elif column_name in item_pattern['search'].keys():
                    search_fun = item_pattern['search'][column_name]
                    try:
                        value = search_fun(db_row['Identificador'], category_answer, db_row, _item_found_db)
                    except TypeError:
                        print('\tSem "%s" para %i (%s).' %
                              (column_name, db_row['Identificador'], category_answer))
                        value = None
                    _item_found_db.update({column_name: [value]})

                # Add some original column information. For safety, strip the
                # beginning and ending spaces, also replaces all double/triple/...
                # spaces in the middle of the sentence for single ones.
                else:
                    value = db_row[column_name]
                    if isinstance(value, str):
                        value = re.sub(r'\s+', ' ', value.strip())
                        if any(name in column_name.lower() for name in ('status', 'situação')):
                            value = value.lower()
                        if value.lower() == 'não consta':
                            # This is used to draw attention to this value on the column.
                            value = f'--{value.lower()}--'
                    _item_found_db.update({column_name: [value]})

            if configs['identify'].get('check_group_material', True):
                # Make some group check of the internal database information.
                check_category(db_row, 'Grupo', item_pattern.get('groups_check'))
                # Make some material check of the internal database information.
                check_category(db_row, 'Material', item_pattern.get('material_check'))

            _item_found_db = pd.DataFrame(_item_found_db)
            if items_found_db[item_name].empty:
                items_found_db[item_name] = _item_found_db
            if _item_found_db.empty:
                pass
            else:
                # items_found_db[item_name] = items_found_db[item_name].append(_item_found_db, ignore_index=True)
                items_found_db[item_name] = pd.concat([items_found_db[item_name], pd.DataFrame(_item_found_db)],
                                                    axis=0, ignore_index=True)

    return items_found_db, items_found_statistic


def check_category(db_row: pd.Series, column_name: str, category_list: list[int] | int):
    '''Print a message if the item is not on the expected category. This
    message is skipped if the group/material is empty and this was already
    warned before, but became mandatory in the absence of the above mentioned
    message (checking by the existence of the first check, by empty data).
    '''
    if not category_list:
        return  # The definition may not be expressed to the current item category.
    if isinstance(category_list, int):
        category_list = [category_list]
    item_category = db_row[column_name]
    if not pd.isnull(item_category) or column_name not in configs['identify'].get('check not empty', []):
        if get_category_number(item_category) not in category_list:
            print(f'\t{column_name} incorreto "{db_row[column_name]}" para'
                  f' {db_row["Identificador"]}: {db_row["Descrição"]}')


def get_category_number(value: str) -> int | None:
    '''Depending of the UNICAMP system used to export the database the
    'group' may become <int> of <str> object. In the case of string it
    may be include the text description of the group but always keeping
    the group number as the first characters.
    '''
    if isinstance(value, str):
        if value := re.findall(r'^\s*\d+', value):
            return int(value[0])
        else:
            return None
    return value


# %%
# Auxiliary functions.

def merge_subcategories(original_db: dict[str, pd.DataFrame],
                        column_sort: str | None = 'Identificador'):
    '''Merge all the items sub-category originated by one
    unit/institute database into a single pandas.DataFrame.

    Parameters:
        Dictionary (subcategories as key) of (dict of unit
            of) Pandas tables to be merged.
        Column name to sort the final table.

    Return same pattern of object as the input with less
    dictionary keys on the first level.
    '''

    output_db: dict[str, dict[str, pd.DataFrame]] = {}
    for equip_category, equip_names in items_category.items():
        output_db[equip_category] = {}
        for equip_name in equip_names:
            for unit in original_db[equip_name].keys():
                if unit not in output_db[equip_category].keys():
                    output_db[equip_category][unit] = pd.DataFrame()

                # Concat it the last result of the for.
                output_db[equip_category][unit] = pd.concat([output_db[equip_category][unit], original_db[equip_name][unit]],
                                                            axis=0, ignore_index=True)
            # del original_db[equip_names]

        if column_sort:
            for unit in output_db[equip_category].keys():
                output_db[equip_category][unit] = output_db[equip_category][unit].sort_values(by=column_sort)

    return output_db


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    # Debug of on small database.
    # from analysis_definitions import items_definitions

    db_name = 'CCUEC'  # Smallest database.
    # db_name = 'FEA'
    # db_name = 'FEM'
    # db_name = 'IB'
    db_file = './Databases/' + db_name + '.xlsx'
    db_file = os.path.join(os.path.dirname(__file__), db_file)
    database = pd.read_excel(db_file)
    # database.set_index('Identificador', inplace=True)
    search_fnc(database)
