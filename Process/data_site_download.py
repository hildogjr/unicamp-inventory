#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas NOT classified
# information, it is covered by Federal Brazilian Law 12527
# from 18/Nov./2011 (public data access):
# http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm.
#
# Extract the original databases from the UNICAMP patrimony system.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import os
from datetime import datetime
# from time import sleep

# Required but not default libraries.
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import TimeoutException

# Internal libraries.
from general_definitions import path_db  # , configs
# import search_on_dbs  # This is not used but the importation creates a better image on `make meta`.
from webbrowser_selenium import SeleniumBrowser

print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Extraindo banco de dados em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)


# Check the application variables and fill the login form.
if not os.getenv('EMAIL_ADDRESS') or not os.getenv('EMAIL_PASSWORD'):
    try:
        from dotenv import load_dotenv
        print('Trying to force environment load (used on debug mode)...', end='')
        load_dotenv()
        print(' loaded.')
    except ImportError:
        pass
    if not os.getenv('EMAIL_ADDRESS') or not os.getenv('EMAIL_PASSWORD'):
        exit('Missed user and password registry on application.')


URL_LOAD_TIMEOUT = 60 * 2
DOWNLOAD_PATH = os.path.abspath(path_db)


# %%
# Functions to browse into the UNICAMP SIAD.

UNICAMP_SIAD = 'https://www.siad.unicamp.br/dadosger/'


def login_siad(browser, user: str | None = None, password: str | None = None) -> None:
    '''Login into the UNICAMP SIAD services.'''
    if browser.driver.current_url != UNICAMP_SIAD:
        print(f'Loading UNICAMP SIAD services website "{UNICAMP_SIAD}"...', end='')
        browser.driver.get(UNICAMP_SIAD)
        print(' done.')
    try:
        WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.presence_of_element_located((By.ID, 'kc-login')))
    except TimeoutException:
        print(f'Page redirected to "{browser.driver.current_url}"...')
        exit('Page not correspond to expected.')

    # For safety, wait until load the click button.
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.element_to_be_clickable((By.ID, 'kc-login')))
    print('Logging in into UNICAMP SIAD services...', end='')
    browser.driver.find_element(By.ID, 'username').send_keys(user or os.getenv('SIAD_ADDRESS'))
    browser.driver.find_element(By.ID, 'password').send_keys(password or os.getenv('SIAD_PASSWORD'))
    browser.driver.find_element(By.ID, 'kc-login').click()
    print(' logged-in.')


def get_analysis_siad(browser) -> list[WebElement]:
    '''Get list of possible analysis listed enrolled in the SIAD site.'''
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.invisibility_of_element_located((By.CLASS_NAME, 'fa-spinner')))
    # WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until_not(EC.presence_of_element_located((By.CLASS_NAME, 'fa-spinner')))
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.presence_of_element_located((By.CLASS_NAME, 'link-analise')))
    return browser.driver.find_elements(By.CLASS_NAME, 'link-analise')


def load_analysis_siad(browser, available_analysis: list[WebElement], name_in: str) -> None:
    '''Load a specific analysis possible in the SIAD system.'''
    # Available data bases:
    # - Bem Móvel - Projeto Elétrica
    # - PAT - Áreas de Patrimônio
    # - PAT-Imóveis por Situação/Tipo
    print('\tAccessing the page intended of show the data...', end='')
    element: WebElement
    for element in available_analysis:
        # if element.text.lower() == 'Bem Móvel - Projeto Elétrica'.lower():
        if name_in.lower() in element.text.lower():
            element.click()
            break
    try:
        WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, 'fa-spinner')))
    except TimeoutException:
        print(' error.')
        exit('Timeout on analysis loading.')
    # Check if occurred some authorization erro.
    # if warnings := browser.driver.find_elements(By.CLASS_NAME, "ui-messages-warn-summary"):
    #     print(' failed.')
    #     warning_msg = '\n'.join(w.text for w in warnings)
    #     self.close()
    #     exit(warning_msg)
    print(' done.')


def execute_filter_siad(browser) -> None:
    '''Execute the database system SIAD filter.'''
    print('\tServer is executing the "no-filter" filter...', end='')
    # WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(
    #     EC.presence_of_element_located((By.CLASS_NAME, 'ui-messages-warn-summary')))
    # WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.presence_of_element_located((By.CLASS_NAME, 'ui-button-text')))
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.invisibility_of_element_located((By.CLASS_NAME, 'fa-spinner')))
    buttons = browser.driver.find_elements(By.CLASS_NAME, 'ui-button-text')
    element: WebElement
    for element in buttons:
        if element.text.lower() == 'pesquisar':
            element.click()
            break
    try:
        WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.invisibility_of_element_located((By.CLASS_NAME, 'fa-spinner')))
    except TimeoutException:
        print(' error.')
        exit('Timeout on filter execution.')
    print(' done.')


def download_db_siad(browser) -> None:
    '''Make the download of the spreadsheet from the SIAD webpage.'''
    print('\tServer is creating the XLSX file...', end='')
    try:
        WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(
            EC.element_to_be_clickable((By.CLASS_NAME, 'fa-file-excel-o')))
    except TimeoutException:
        print(' error.')
        exit('Timeout on spreadsheet download.')
    browser.driver.find_element(By.CLASS_NAME, 'fa-file-excel-o').click()
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT * 2).until(
        EC.invisibility_of_element_located((By.ID, 'executarAnaliseForm:dlgProgresso_title')))
    print(' done.')


# %%
# Functions to browse into the UNICAMP Sistema A.

UNICAMP_SISTEMAA = 'https://www.dga.unicamp.br/exterlogin/A_Sistema.asp'


def login_sistemaa(browser, user: str | None = None, password: str | None = None):
    '''Login into the UNICAMP SistemaA services.'''
    if browser.driver.current_url != UNICAMP_SIAD:
        print(f'Loading UNICAMP SistemaA services website "{UNICAMP_SISTEMAA}"...', end='')
        browser.driver.get(UNICAMP_SISTEMAA)
        print(' done.')

    print('Logging in into UNICAMP SistemaA services...', end='')
    browser.driver.find_element(By.XPATH, '//input[@name="login"]').send_keys(user or os.getenv('SISTEMAA_USER'))
    browser.driver.find_element(By.XPATH, '//input[@name="senha"]').send_keys(password or os.getenv('SISTEMAA_PASSWORD'))
    browser.driver.find_element(By.XPATH, '//input[@name="senha"]').send_keys(Keys.ENTER)
    # browser.driver.find_element(By.XPATH, '//input[@name=""imageField"]').click()
    print(' logged-in.')


def get_analysis_sistemaa(browser):
    '''Get the UNICAMP Sistemaa available analysis.'''
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(EC.element_to_be_clickable((By.NAME, 'sistema')))
    return browser.driver.find_element(By.NAME, 'sistema').find_elements(By.XPATH, '//option')


def load_analysis_sistemaa(browser, available_analysis: list[WebElement], name_in: str):
    '''Access the UNICAMP Sisteama download page of specific analysis.'''
    print('\tAccessing the page intended of show the data...', end='')
    element: WebElement
    for element in available_analysis:
        # if element.text.lower() == 'Bem Móvel - Projeto Elétrica'.lower():
        if name_in.lower() in element.text.lower():
            element.click()
            break
    browser.driver.find_element(By.XPATH, '//input[@class="servicos"]').click()
    browser.driver.switch_to.window(browser.driver.window_handles[-1])
    print(' done.')


def download_db_sistemaa(browser):
    '''Download the data from the specific analysis.'''
    WebDriverWait(browser.driver, URL_LOAD_TIMEOUT).until(
        EC.element_to_be_clickable((By.XPATH, '//a[contains(text(), "Download")]')))
    browser.driver.find_element(By.XPATH, '//a[contains(text(), "Download")]').click()


def main() -> None:
    '''Browsing logic: From all possible analysis in the current login, access the
    reserved to FEEC use. Starting download the inventory, that is the huge database,
    and after start the process of download the build and areas lists.'''
    print('Initializing Firefox web browser...', end='')
    with SeleniumBrowser(DOWNLOAD_PATH) as browser:
        print(' done.')

        # Extract the data from the SistemaA Unicamp system.
        login_sistemaa(browser)
        load_analysis_sistemaa(browser, get_analysis_sistemaa(browser), 'Scheduler')
        download_db_sistemaa(browser)

        # Extract the data from the SIAD Unicamp system.
        login_siad(browser)

        # At enf of May/2022 occurred a Unicamp DB migration to PostgreSQL and this
        # query at the site start to use long time to execute. I knew that this
        # download method as just a workaround the need for a direct connection
        # to the DB, which was available at 31/Jan/2023.
        # print('Getting inventory...')
        # browser.load_analysis(browser.get_analysis(), 'Bem Móvel')
        # browser.execute_filter()  # Filter is necessary on inventory.
        # browser.download_db()

        print('Getting build list...')
        browser.new_tab(UNICAMP_SIAD)
        load_analysis_siad(browser, get_analysis_siad(browser), 'Imóveis')
        execute_filter_siad(browser)  # Filter is necessary for build list.
        download_db_siad(browser)

        print('Getting sectors list...')
        browser.new_tab(UNICAMP_SIAD)
        load_analysis_siad(browser, get_analysis_siad(browser), 'Áreas')
        download_db_siad(browser)

        # Wait until alls downloads finish.
        print(f'Waiting to finish downloads of DB files to "{DOWNLOAD_PATH}"...', end='')
        browser.wait_downloads(URL_LOAD_TIMEOUT * 40)
        print(' done.')


# %%
# Main entry.
if __name__ == "__main__":
    main()
