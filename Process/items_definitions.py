#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide all the search and scrape definitions.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
import re
# import glob
# import os
# import importlib
import warnings
try:
    from numba.core.errors import NumbaDeprecationWarning
except ImportError:
    pass

# Internal libraries.
# Load the specific scrape/search definitions for each item type. They
# are defined into files named as `items_*.py` (excluding some special
# ones). The `items_definitions`is are inside each bellow submodule.
# items_definitions = {}
from items_airconditioners import *
from items_ups import *
from items_computers import *
from items_routers import *
from items_fridges import *
from items_transformers import *
'''
items_module_names = glob.glob('items_*.py')
for item_module_name in items_module_names:
    # Do not import the test, low level and current file.
    if item_module_name in ('items_general_info.py', 'items_testcase.py', 'items_definitions.py'):
        continue
    # Import all the equipment items definitions.
    item_module_name = os.path.splitext(item_module_name)[0]
    item_module = importlib.import_module(item_module_name)
    # The bellow code was borrow from:
    # https://stackoverflow.com/questions/21221358/python-how-to-import-all-methods-and-attributes-from-a-module-dynamically
    module_dict = item_module.__dict__
    try:
        to_import = item_module.__all__
    except AttributeError:
        to_import = [name for name in module_dict if not name.startswith('_')]
    globals().update({name: module_dict[name] for name in to_import})
'''

# Ignore the "Fall-back from the nopython compilation path to the object mode compilation path
# has been detected, this is deprecated behaviour" `numba` warning from all the itens definitions
# module files.
if 'NumbaDeprecationWarning' in locals():
    warnings.simplefilter('ignore', category=NumbaDeprecationWarning)

# Understand the main categories in the items to be found.
# Into each of the importation above, the equipment categories
# can have sub-categories just to help better code programming/
# data understanding. It is necessary to group them as the main
# categorias for the data output: spreadsheet and report.
# Example: "main category name (sub category name)".
items_category: dict = {}
for item_name in items_definitions.keys():
    match_name = re.findall(r'([\w\s]*\w)\s*(\(([\w\s]*\w)\))*',
                            item_name, re.IGNORECASE)
    main_name = match_name[0][0]
    if main_name not in items_category.keys():
        items_category[main_name] = []
    items_category[main_name].append(item_name)

# Compile the regular expressions, checking if the equipment
# definition group was already compiled before. It is necessary
# because of the multiple equipment types import.
for item_search_name in items_definitions.values():
    # if not isinstance(item_search_name['desc_match'], re.Pattern):
    #     item_search_name['desc_match'] = re.compile(item_search_name['desc_match'], re.IGNORECASE)
    #     if item_search_name.get('desc_not_match'):
    #         item_search_name['desc_not_match'] = re.compile(item_search_name['desc_not_match'], re.IGNORECASE)
    for item_search in item_search_name['search'].values():
        if isinstance(item_search, str):
            item_search = lambda t: re.search(item_search, t, re.IGNORECASE)[1]


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    # Compile the regular expressions, checking if the equipment
    # definition group was already compiled before. It is necessary
    # because of the multiple equipment types import.
    for item_name, item_search_name in items_definitions.items():
        print(f'Compiling "{item_name}" regular expressions...')
        if not isinstance(item_search_name['desc_match'], re.Pattern):
            print(re.compile(item_search_name['desc_match'], re.IGNORECASE).pattern)
        if item_search_name.get('desc_not_match') and not isinstance(item_search_name['desc_not_match'], re.Pattern):
            print(re.compile(item_search_name['desc_not_match'], re.IGNORECASE).pattern)
