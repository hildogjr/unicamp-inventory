#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Copyright definitions, used for attach information of my
# authorship to some specific file extensions.
# Uses the python-pillow library.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['imagefile_add_watermark', 'image_add_watermark']

# Base libraries.
import os

# Required but not default libraries.
import numpy as np
from PIL import Image, ImageSequence
try:
    from numba import jit
except ImportError:
    from debug_fncs import empty_decorator as jit

# Internal libraries.
# from debug_fncs import time_it


# %%

@jit
def image_add_watermark(img: np.array,
                        watermark_img: np.array,
                        opacity: float | None = 0.2,
                        position: tuple[int, int] | None = None,
                        relative_max_size: float | None = 0.65,
                        watermark_rotation: float | None = 20.0) -> np.array:
    '''Add watermark to the center of the image.

    Parameters:
        Image matrix to add the watermark.
        Image matrix of the watermark.
        Opacity of the watermark.
        Maximum relative size.
        Watermark rotation in degrees.

    Return the image watermarked.
    '''
    h_logo, w_logo = watermark_img.size
    h_img, w_img = img.size

    # Get the dimensions to insert the watermark in the center of the main image.
    watermark_scale = min([relative_max_size * h_img / h_logo, relative_max_size * w_img / w_logo])

    # Manipule the watermark image: resize and rotate.
    if watermark_rotation:
        watermark_img = watermark_img.rotate(watermark_rotation, expand=True)
    h_img, w_img = img.size  # Size of background Image.
    h_logo, w_logo = watermark_img.size
    watermark_img = watermark_img.resize((int(h_logo * watermark_scale),
                                          int(w_logo * watermark_scale)))
    h_logo, w_logo = watermark_img.size  # Size of foreground.

    # Position of foreground/overlay image.
    if position:
        x, y = position[0], position[1]
    else:
        x, y = (int(h_img / 2) - int(h_logo / 2), int(w_img / 2) - int(w_logo / 2))
    # Loop over all pixels and apply the blending equation.
    for i in range(h_logo):
        for j in range(w_logo):
            if x + i >= h_img or y + j >= w_img:
                continue
            watermark_pixel = watermark_img.getpixel((i, j))
            alpha_logo = watermark_pixel[3] / 0xff * opacity  # Read the alpha channel.
            alpha_img = 1 - alpha_logo
            img_pixel = list(img.getpixel((x + i, y + j)))
            # Manipulate the RGB and keep the A (transparancy) information (4th element).
            img_pixel[0] = int(alpha_img * img_pixel[0] + alpha_logo * watermark_pixel[0])
            img_pixel[1] = int(alpha_img * img_pixel[1] + alpha_logo * watermark_pixel[1])
            img_pixel[2] = int(alpha_img * img_pixel[2] + alpha_logo * watermark_pixel[2])

            # Replace pixel.
            img.putpixel((x + i, y + j), tuple(img_pixel))
    return img


watermark_img = None  # Initiaize the global (really used just as static) variable.


def imagefile_add_watermark(file_name: str,
                            file_output_name: str | None = None,
                            logo_file_name: str | None = None,
                            opacity: float | None = 0.2,
                            relative_max_size: float | None = 0.65,
                            watermark_rotation: float | None = 20.0):
    '''Add my logo as watermark to the center of the `file_name` image file.

    Parameters:
        File to add the watermark.
        Path to save, if not given it will overwrite the original image.
        Logo path file name.
        Opacity of the watermark.
        Maximum relative size.
        Watermark rotation in degrees.
    '''
    # I didn't use OpenCV since 2008, so I need to borrow some
    # reference code from https://www.youtube.com/watch?v=xfgtcAw-VIA.
    # Some future reference for project: https://www.youtube.com/watch?v=MkcUgPhOlP8.
    global watermark_img  # Make it local to save reload procedure.
    if not os.path.isfile(file_name):
        return  # File not created.
    # Load the watermark image just once to speed-up the code.
    if not isinstance(watermark_img, np.ndarray):
        if not logo_file_name:
            logo_file_name = os.path.join('Resources', 'logo_HGJ_big.png')  # 'logo_HGJ.png'
        watermark_img = Image.open(logo_file_name)
        # The watermark must be white on a black background, my
        # logo is a black symbol with a transparent background.
        # It is necessary to convert it.
        # watermark_img = 255 - watermark_img
    img = Image.open(file_name)  # Load the source file.
    file_extension = file_name.split('.')[-1].lower()

    # Get the dimensions to insert the watermark in the center of the main image.
    if file_extension == 'gif':
        resultant_gif = []
        for frame in ImageSequence.Iterator(img):
            resultant_gif.append(
                image_add_watermark(frame.convert(), watermark_img,
                                    opacity=opacity,
                                    relative_max_size=relative_max_size,
                                    watermark_rotation=watermark_rotation)
            )
        img = resultant_gif
    else:
        img = image_add_watermark(img, watermark_img,
                                  opacity=opacity,
                                  relative_max_size=relative_max_size,
                                  watermark_rotation=watermark_rotation)

    if not file_output_name:
        file_output_name = file_name  # Overwrite the original file.
    if file_extension == 'gif':
        # Pillow allows to manipulate gif image files, it must be saved with
        # the first image as file and the others appended.
        if len(img) > 1:
            img[0].save(file_output_name, save_all=True, append_images=img[1:])
        else:
            img[0].save(file_output_name)
    elif file_extension == 'png':
        # Higher level compression for the PNG image format.
        img.save(file_output_name, optimize=True, compression=9)
    elif file_extension == 'jpg' or file_extension == 'jpeg':
        img.convert('RGB').save(file_output_name, optimize=True, quality=70)
    else:
        img.save(file_output_name, optimize=True)


# %%
# Main entry, used for debug. In this case also used to prompt use the watermark function.
if __name__ == "__main__":
    from debug_fncs import time_it

    # Time it to check the small time on after first execution due
    # the @jit decorator used on core routines (do not print this
    # time debug on normal call executions).
    @time_it
    def watermark_img_fnc(path: str) -> None:
        imagefile_add_watermark(path)

    # Test image output.
    print('Using the debugging tests...')
    import shutil
    base_image_path = os.path.join('Test_data', 'chart_image.png')
    image_path1 = os.path.join('public', 'image1-pillow.png')
    shutil.copyfile(base_image_path, image_path1)
    watermark_img_fnc(image_path1)
    assert os.path.isfile(image_path1), f'Failed {image_path1}'
    # Test a second time, because the test of logo already loaded.
    image_path2 = os.path.join('public', 'image2-pillow.png')
    shutil.copyfile(base_image_path, image_path2)
    watermark_img_fnc(image_path2)
    assert os.path.isfile(image_path2), f'Failed {image_path2}'
    # Test a third time with other file extension.
    image_path3 = os.path.join('public', 'image3-pillow.jpg')
    shutil.copyfile(base_image_path, image_path3)
    watermark_img_fnc(image_path3)
    assert os.path.isfile(image_path3), f'Failed {image_path3}'
