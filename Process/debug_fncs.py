#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas classified information.
#
# Functions to help debug my own code.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['time_it', 'empty_decorator']

# Base libraries.
from time import time

# Required but not default libraries.

# Internal libraries.

# Definitions of shortcut methods.


# %%
#

def time_it(func: callable):
    '''This function shows the execution time of the function object passed.'''
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'Function {func.__name__!r} executed in {(t2 - t1):.4f}s')
        return result
    return wrap_func


def empty_decorator(func: callable):
    '''Empty decorator.'''
    def wrap_func(*args, **kwargs):
        return func(*args, **kwargs)
    return wrap_func
