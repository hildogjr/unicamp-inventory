#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Create simple HTML page with the last report, items
# identification and other relevant information.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from datetime import datetime
from os import path, getenv
import re
import glob
from fnmatch import fnmatch

# Required but not default libraries.

# Application libraries.
from general_definitions import (path_output, path_resources, path_db,
                                 file_name_report, file_name_analysis,
                                 FILE_NAME_INVENTORY_DB, FILE_NAME_BUILDS_DB,
                                 FILE_NAME_SECTORS_DB)
from items_definitions import items_category
from format_output import evaluate_variables, get_link_from_string

print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Criando página web em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)

# %%

if not path.isfile(file_name_report):
    exit('Nenhum relatório previamente gerado, a página não será gerada.')

# Load the base HTML page.
print('Carregando template the página...', end='')
with open(path.join(path_resources, 'page.html'), 'r') as fh:
    html_page = fh.read()
print(' carregado.')

# Get the analytics track code.
# I tried to embedded from other file through HTML tag directive.
# But the behavior was not the expected inside <head> tag.
print('Adicionando código de rastreamento...', end='')
try:
    with open(path.join(path_resources, 'track_code.html'), 'r') as fh:
        track_code = children = fh.read()
    html_page = html_page.replace(r'<!--TRACK CODE-->', track_code)
    print(' adicionado.')
except (FileExistsError, FileNotFoundError):
    print(' falha no arquivo (ignorado código).')

# Replace some values on the template message be software variable values.
print('Substituindo valores no template...', end='')
# database_file_name = path.relpath(glob.glob(path.join(path_db, '*.xlsx'))[0])
items_categories = [n.capitalize() for n in sorted(items_category.keys())]
path_full_report = glob.glob(path.join(path_output, 'Relatório.*'))
if path_full_report:
    path_full_report = path.relpath(path_full_report[0], path_output)
else:
    exit('Arquivo de relatório não encontrado.')
path_log_file = glob.glob(path.join(path_output, 'Identifica*o.*'))
if path_log_file:
    path_log_file = path.relpath(path_log_file[0], path_output)
else:
    exit('Log de identificação não encontrado.')
path_inventory_db = glob.glob(path.join(path_output, FILE_NAME_INVENTORY_DB))
if path_inventory_db:
    path_inventory_db = path.relpath(path_inventory_db[0], path_output)
else:
    exit('DB com inventário original não encontrada.')
path_builds_db = glob.glob(path.join(path_output, FILE_NAME_BUILDS_DB))
if path_builds_db:
    path_builds_db = path.relpath(path_builds_db[0], path_output)
else:
    exit('DB com listagem tos prédios não encontrada.')
path_build_sectors_db = glob.glob(path.join(path_output, FILE_NAME_SECTORS_DB))
if path_build_sectors_db:
    path_build_sectors_db = path.relpath(path_build_sectors_db[0], path_output)
else:
    exit('DB com listagem dos setores não encontrada.')
path_report_statistics_pattern = path.splitext(file_name_analysis)[0] + '.*'
path_report_statistics = glob.glob(path_report_statistics_pattern)
if path_report_statistics:
    path_report_statistics = path.relpath(path_report_statistics[0], path_output)
else:
    exit('DB com estatísticas tabuladas não encontrada.')
year = date_now.year
html_page = evaluate_variables(dict(globals()), html_page, convert_to_str=True)
print(' valores substituidos.')

# Add the parcial results and spreadsheets with the identified items.
# Search by the prefix "Relatório*" instead the file extension "*.pdf"
# and each category name at begging of the file name instead the
# extension ".xlsx" because I may decide to compress all those output
# file of my software due the 100MB default limit of Gitlab artifacts
# to allow page generation.

print('Buscando por lista de itens identificados e relatórios parciais...', end='')

# identified_items = glob.glob(path.join(path_output, '*.xlsx'))
identified_items = []
for category in items_categories:
    identified_items += glob.glob(path.join(path_output, category + '.*'))
for idx in reversed(range(len(identified_items))):
    if fnmatch(path_report_statistics_pattern, identified_items[idx]):
        del identified_items[idx]
for idx, file_path in enumerate(identified_items):
    identified_items[idx] = path.relpath(file_path, path_output)

partial_reports = glob.glob(path.join(path_output, 'Relatório*'))
if file_name_report in partial_reports:
    partial_reports.remove(file_name_report)
for idx, file_path in enumerate(partial_reports):
    partial_reports[idx] = path.relpath(file_path, path_output)

print(' encontrado %i categorias e %i relatórios parciais.' %
      (len(identified_items), len(partial_reports)))

# Force all elements to be sorted to match the three lists.
items_categories = sorted(items_categories)
identified_items = sorted(identified_items)
partial_reports = sorted(partial_reports)

# Create a table with the file links.
print('Criando tabela HTML para resultados parciais...', end='')
html_by_item_table = ''
for idx, (item_name, spreadsheet) in enumerate(zip(items_categories, identified_items)):
    # If present add also the partial report.
    if partial_reports:
        report = partial_reports[idx]
        report_html_page = '''
        <td style="text-align:center">
            <a href="{partial}">
                <span class="icon">
                    <img src="page_files/pdf.svg" alt="Report" class="img-icon-small"
                    title="Baixar relatório parcial de {name}">
                </span>
            </a>
        </td>
        '''.format(partial=report, name=item_name)
    else:
        report_html_page = ''  # '&nbsp;'
    html_by_item_table += '''
    <tr>
        <td><p>{name}</p></td>
        <td style="text-align:center">
            <a href="{items}">
                <span class="icon">
                    <img src="page_files/excel.svg" alt="List of specific itens" 
                    class="img-icon-small" title="Baixar identificação de {name}">
                </span>
            </a>
        </td>
        {report}
    </tr>
    '''.format(name=item_name, items=spreadsheet, report=report_html_page)  # noqa
html_page = html_page.replace('<!--IDENTIFIED ITENS TABLE-->', html_by_item_table)
print(' criada.')

# Add some information to the template page.
print('Adicionando referências...', end='')
with open('articles.txt') as fh:
    articles = fh.read().splitlines()
    for idx, a in enumerate(articles):
        if not a:
            del articles[idx]
if articles:
    articles_html = '<p>Referências:<ul>'
    for article in articles:
        if article:
            url = get_link_from_string(article)
            if url:
                article = article.replace(url, '<a href="{url}">{url}</a>'.format(url=url))
            articles_html += '<li>' + article + '</li>'
    articles_html += '</ul></p>'
    html_page = html_page.replace(r'<!--ARTICLES TO CITE-->', articles_html)
print(' adicionado %i referências.' % len(articles))


html_page = re.sub(r'{{PROJECT_URL}}', getenv('CI_PROJECT_URL', 'https://gitlab.com/hildogjr/unicamp-inventory'), html_page)
html_page = re.sub(r'{{CI_PAGES_URL}}', getenv('PAGE_URL', 'https://hildogjr.gitlab.io/unicamp-inventory/'), html_page)


# Save the page and copy the required files.
print('Salvando página HTML...', end='')
html_page = re.sub(r'<!--((.|\s)*?)-->', '', html_page)
with open(path.join(path_output, 'index.html'), 'w') as fh:
    fh.write(html_page)
print(' salva.')
