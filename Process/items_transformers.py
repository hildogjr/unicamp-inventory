#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide specific definitions for search and scrape
# transformers from the distribution system.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Any
import re

# Required but not default libraries.
# import numpy as np
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
# from numba import jit

# Internal libraries.
from items_general_info import *
from analysis_chart import *
from general_definitions import UNIDENTIFIED_TYPE, _, print


# %%
# Functions to get data from the original databases.

def get_transformer_type(item_idx: int,
                         category: str,
                         row: pd.DataFrame,
                         found_statistics: dict[str, Any] = {},
                         column_search: str = 'Descrição') -> str:
    '''Get the router type.

    Parameters:
        Identification number.
        Item description matched.
        Data row.
        Statistics already evaluated.
        Column name to search the information. The default is
            'Descrição', some user filled in 'Modelo'.

    Return the router type (str).
    '''

    def is_match_type(pattern: str) -> bool:
        '''Get the type on the description.

        Tip: Always check the most specific category before the
        most generic one, because the first one may have the
        second word/noun as part of it description.

        Parameter pattern (str).

        Return (bool).'''
        match = re.search(pattern, row[column_search], re.IGNORECASE)
        return match

    # Order for search here is important, it must check first
    # the less generic words because the groups formed by
    # such words may contain the generic words.
    if is_match_type(r'(auto[\.\-\s]*(transformador|trafo))'):
        return _('autotranformador')  # Auto transformers are usually dry.
    if is_match_type(r'(seco|resina)'):
        return _('a seco')
    if is_match_type(r'([oó\?]leo)'):
        return _('a óleo')

    print(text_warning_missing(row, 'Tipo', category))
    return UNIDENTIFIED_TYPE


def get_transformer_electric_power(*args, **kwargs) -> float | None:
    '''Specific function to get the transformer electrical power.'''
    return get_electric_power(*args, **kwargs,
                              units_re=['VA', 'W', r'watt*s', 'WA'],
                              re_remove=r'(?:(?:Q|QT|QTE)-*\d+|(?:trafo|transformador) +\d{1,2} +(?!k *)(?!(W|V)))')


# %%
# References to functions for scrape data from the original databases
# and statistics and chart analysis.

# Scrape description for the property description item.
# The main key is 'category name (sub category name as optional)'.
_category_name = 'transformadores de distribuição'
items_definitions.update({
    _category_name: {
        # This search partners will be saved as columns at the
        # final spreadsheet file. The order specified here may
        # not affect the columns order of the spreadsheet, it
        # must be set on the save routine.
        'desc_match': r'(?:(?:auto[\.\-\s]|\s|^)*(?:transformador|trafo))',
        'desc_not_match': r'(?:ups|no[-\ ]*break|solda|estabilizador'
                          r'|retroprojetor|refletor|eletr[oô\?]nic[oa]'
                          r'|desmont[aá\?]vel|d[eé\?]cada|m[óo\?]dulo|conjunto'
                          r'|transformadores'  # On plural are kit and modules.
                          # Remove the single-phase (here there isn't such type in the distribution system).
                          r'|(?:mono|bi)[\s\-]*(?:f[aá\?]sico|(?![a-z]))'
                          r'|vari[aá\?](?:vel|dor)|ajust[áa\?]vel|bivolt|regul(?:ador|[áa\?]vel)'
                          r'|caixa|maquete|mal(?:a|eta)|c[aâ]m[ea]ra|fonte|sistema|sinal|led|neon'
                          r'|painel|retificador|isola(?:dor*|mento)|de +isola[çc\?][ãa\?]o|acoplamento'
                          r'|aerofotogr[aá\?]fico|vari(?:ac|ked)|de +(?:corrente|g[aá\?]s)'
                          r')',
                          # r'|(?:auto[\s\-]*)*transformador.+'
                          # r'(?:(?:110|127)[\s\-\/]+220|220[\s\-\/]+(?:110|127))' # Can't be used.
        # List of groups that may belongs to this category. They
        # are used for double match and report inconsistences
        # in the original database procedure (as already reported).
        'groups_check': [59, 61],
        # References to functions of scrap data.
        'search': {
            'Tipo': get_transformer_type,
            'Data de compra': get_purchase_data,
            'Potência elétrica (VA)': get_transformer_electric_power,
            'Localização': get_location,
        },
        # References to functions for statistic and chart analysis.
        'analysis': {
            # "Tipo" must be the first analysis because it result may be used into other charts.
            'Tipo': analyze_percentage_chart,
            'Data de compra': analyze_discrete_chart,
            'Potência elétrica (VA)': analyze_continuous_chart,
            # 'Saldo Contábil': analyze_scatter_chart,
        }
    }
})
# Exclude transformers accessories and when transformer is the accessory.
items_definitions[_category_name]['desc_not_match'] = (
    items_definitions[_category_name]['desc_not_match'][:-1]
    # Prefixes used to exclude the item from this subcategory.
    + r'|(?:para|p\/|sem|em|como*|c\/|contendo|e|d[oae]|um|\d{1,2}) *'
    + items_definitions[_category_name]['desc_match']
    # Suffixes used to exclude the item from this subcategory.
    + r'|' + items_definitions[_category_name]['desc_match']
    + ' +(?:interno|integrado)'
    + r')'
)


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    from items_testcase import test_all
    test_all(__file__, items_definitions)
