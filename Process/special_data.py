#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide some special data definitions not default on Python.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['UniqueElemList']

from typing import Any


# %%

class UniqueElemList(list):
    def __init__(self, v: Any | None = None, accept_none: bool = True):
        self._accept_none = accept_none
        if v:
            if isinstance(v, str):
                v = list(v)
            if isinstance(v, list):
                # Remove the duplicated values.
                for vv in v:
                    self.__add__(vv)
            else:
                super.__init__(v)

    def add(self, v):
        if v not in self and (v is not None or self._accept_none):
            self.append(v)

    def discard(self, v):
        while v in self:
            self.pop(v)


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    a = UniqueElemList()
    a.add(1)
    a.add(2)
    a.add(3)
    a.add(2)
    a.add(None)
    a.remove(2)
    assert a == [1, 3, None]
    print(a)

    a = UniqueElemList(accept_none=False)
    a.add(1)
    a.add(2)
    a.add(3)
    a.add(2)
    a.add(None)
    a.remove(2)
    assert a == [1, 3]
    print(a)
