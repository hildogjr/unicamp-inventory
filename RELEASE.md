This document have the releases comments add to at each automatic e-mail.

Add the comments as items in a bi-annual release format with month-year section title. The most recent section must be just into this file.

# 12-2023
- Uso do runners compartilhados do Gitlab devido problemas enfrentados na estrutura computacional do LabREI.
- Artigo com os resultados contemplado como melhor paper da conferência: Guillardi Júnior, H. (2024). Analysis of Electrical Equipment at UNICAMP: Insights from the Inventory Database. In: Jørgensen, B.N., da Silva, L.C.P., Ma, Z. (eds) Energy Informatics. EI.A 2023. Lecture Notes in Computer Science, vol 14468. Springer, Cham. https://doi.org/10.1007/978-3-031-48652-4_14

# 03-2023
- Reestabelecido o acesso aos dados da Unicamp em 02/2023, problema ocorrido na migração do servidor em maio/2022.

# 01-2022
- Adicionado função para obter a potência frigorífica a partir do modelo do ar condicionado;
- Incorporado os dados dos prédios e órgãos no site;
- Corrigida compatibilidade com a exportação "HEMO" / "HEMOCENTRO" pelo sistema de patrimônio.

# 09-2021
- Download do banco de dados a partir de automação do browser, não dependendo de link direto disponibilizado pela UNICAMP;
- Criado site https://hildogjr.gitlab.io/unicamp-inventory/ com relatório e planilhas com discriminação dos equipamentos;
- Usa funções pré-compiladas para acelerar o tempo de execução de código das etapas de identificação e análise.

# 06-2021
- Adicionada a busca por transformadores de distribuição;
- Realizada a contabilização estimada de quantidade de sistemas de ar condicionados (no caso do registro de unidades evaporadoras e condensadoras separadamente);
- Melhora os textos e hyperlinks do relatório.

# 03-2021
Agradecimentos a/o(s):
- GT5 do Campus Sustentável onde o trabalho se iniciou;
- Estudantes do grupo de apoio do GT1 do Campus Sustentável que estiveram trabalhando na conferência destes dados nos últimos dias;
- Everaldo e pessoal da DGA pela dicas adicionais sobre o sistema de patrimônio;
- Prof. Luiz por me permitiu trabalhar sem restrições neste projeto;
- Prof. Antenor por permitir o uso de seus servidores para processamento desta versão final;
- Jefferson e pessoal da CCEUC por realizar as correções necessárias no servidor de e-mail da universidade para todos receberem relatório.

.End of file
