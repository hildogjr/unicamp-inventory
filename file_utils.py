#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Provide functions to deal with the data and file conversions
# during the tests.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

# Base libraries.
from __future__ import annotations
import os
import glob
from xlsxwriter.workbook import Workbook
import csv

# Required but not default libraries.
from tqdm import tqdm


# %%
#

def csv_to_xlsx(file_input: str,
                file_output: str | None = None,
                **kwargs) -> None:
    '''Convert a CSV file into a XLSX file.

    Parameters:
        Path to the CSV file.
        (Optional) path to the XLSX output file.
        Others CSV dialect parameters.
    '''
    file_output = file_output or os.path.splitext(file_input)[0] + '.xlsx'
    with open(file_input, 'rt', encoding='utf8') as fh:
        reader = csv.reader(fh, **kwargs)
        num_rows = sum(1 for _ in reader)
        fh.seek(0)  # Return to first character after counting.
        with Workbook(file_output) as workbook, \
                tqdm(desc='Converting...', total=num_rows) as progress:
            worksheet = workbook.add_worksheet()
            for idx_row, row in enumerate(reader):
                worksheet.write_row(idx_row, 0, [v.strip() for v in row])
                # for idx_col, cell_value in enumerate(row):
                #     worksheet.write(idx_row, idx_col, cell_value.strip())
                progress.update(1)


# %%
# Main entry.
if __name__ == "__main__":
    import sys
    if len(sys.argv) == 3:
        csv_to_xlsx(sys.argv[1], sys.argv[2], delimiter=';')
    elif len(sys.argv) == 1:
        from pathlib import Path
        files = glob.glob(os.path.join(Path.home(), 'Downloads', '*.csv'))
        for file in files:
            csv_to_xlsx(file, delimiter=';')
    else:
        exit(f'Not recognized parameters {sys.argv}')
