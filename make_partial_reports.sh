#!bash
# This file is covered tby GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Create the partial reports.
# Written by Hildo Guillardi Júnior (GUILLARDI JR., H.).

# Use the file names (without extension or folder name) as parameters to call the report generation.
#echo "Criando relatórios parciais por unidades..."
#units=$(find Databases/*.xlsx -type f)
#units=$(echo "${units}" | while read f; do echo "${f#*/}"; done)
#units=$(echo "${units}" | while read f; do echo "${f%.*}"; done)
#echo "${units}" | xargs -i python generate_report.py -u {} -o "Relatório - {}.pdf"

echo "Criando relatórios parciais por equipamentos..."
items=$(find public/*.xlsx -type f -not -name 'Análise geral.xlsx')
items=$(echo "${items}" | while read f; do echo "${f#*/}"; done)
items=$(echo "${items}" | while read f; do echo "${f%.*}"; done)
echo "${items}" | xargs -i python Process/generate_report.py -i {} -o "Relatório - {}.pdf"
