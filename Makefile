#!make
# This file is covered tby GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Recipe of the software.
# Written by Hildo Guillardi Júnior (GUILLARDI JR., H.).

#include .env  # Uncomment this for use the ARTICLE configurations and run out of Gitlab CI/CD.

.PHONY: help
help:
	@echo "Allowed commands:"
	@echo "install \t Install all the requirements for the application."
	@echo "tests \t\t Execute all the code tests and validations."
	@echo "get_data \t Extract the XLSX files databases from UNICAMP server."
	@echo "identify \t Identify all items classes."
	@echo "analyze \t Create a report with the identified data."
	@echo "report \t\t Generete the report."
	@echo "vpn \t\t Connect to Unicamp VPN."
	@echo "email \t\t Send the global report to a email."
	@echo "site \t\t Build the web site HTML page."
	@echo "meta \t\t Run some metadata analysis."

# ---------------------------------------
# Base executions.
.PHONY: get_data identify analyze report email site get_processed

# Download the databases from the University patrimony system.
get_data:
	# Cleanup the downloaded databases.
	rm public/AGI*.xlsx -f
	mkdir public -p
	# Download the last version of geckodriver for Mozilla Firefox. 
	rm geckodriver.log -f
	#$(eval gecko_driver:=$(shell bash -c 'curl -s https://api.github.com/repos/mozilla/geckodriver/releases/latest | grep 'browser_' | cut -d\" -f4 | grep linux64.tar.gz$$'))
	#wget $(gecko_driver) -O geckodriver.tar.gz
	#tar -xf gecko*.tar*
	#rm *.tar.gz
	# Download the most recent inventory database.
	python Process/data_site_download.py
	#cat geckodriver.log
	#rm geckodriver -f

# Identify the items from the units/schools databases.
identify:
	mkdir public -p
	python Process/identify_items.py > public/Identificação.log

# Analyse creating statistics of each item identified.
analyze:
	rm Temporary/* -f
	mkdir Temporary -p
	python Process/performe_analysis.py

# Create the complete report.
report:
	python Process/generate_report.py

email:
	python Process/send_email.py

site:
	# Compress some result files to allow the page generation.
	ls public/ -l
	- cd public && 7z a Identificação.7z Identificação.log && rm Identificação.log
	#- cd public && 7z a Relatório.7z Relatório.pdf && rm Relatório.pdf
	- libreoffice7.2 --headless --convert-to ods public/*.xlsx --outdir public || libreoffice --headless --convert-to ods public/*.xlsx --outdir public && rm public/*.xlsx -f
	#- cd public && find . -name 'Relatório - *.pdf' -print0 | xargs -0 -n1 -I {} 7z a "{}".7z {} && rm 'Relatório - *.pdf'
	#- cd public && find . -type f \( -name '*.xlsx' -o -name '*.xls' \) -print0 | xargs -0 -n1 -I {} 7z a "{}".7z {} && rm *.xlsx *.xls
	#- cd public && find . -type f -name '*.ods' -print0 | xargs -0 -n1 -I {} 7z a "{}".7z {} && rm *.ods
	- cd public && find . -type f -name '*.csv' -print0 | xargs -0 -n1 -I {} 7z a "{}".7z {} && rm *.csv
	# Create the page with all results.
	cp Resources/page_files public -r
	ls public/ -l
	python Process/create_page.py

# Macro for download all the information from the website to my personal folder.
get_processed:
	# Delete the older files.
	- cd public && rm *.xls *.xlsx *.ods *.7z *.log *.pdf
	mkdir public/ -p
	# Get the available data from the website.
	wget -qO- https://hildogjr.gitlab.io/unicamp-inventory/ \
		| grep -oiP '<a +href=\".+\.(ods|xlsx*|pdf|log|7z|)\" *>' \
		| sed -n "s|<a * href *= *\"\([^\"\'>]*\)\">|\1|p" \
		| xargs -n1 -I {} wget -P ./public/ https://hildogjr.gitlab.io/unicamp-inventory/{}
	- cd public/ && find . -type f -name '*.7z' -exec file-roller -h {} +
	- rm public/*.7z


# ---------------------------------------
# Release procedures.

# Execute all above functions but the email.
.PHONY: all install vpn me translate
all: load identify analyze report

install:
	sudo apt install graphviz libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 python3 python3-pip p7zip-full gettext
	sudo -H pip install -r Process/requirements.txt
	@echo "Este programa realiza a análise de patrimônio da UNICAMP com foco em identificação de consumo."
	@echo "Software completamente desenvolvido por Dr. Hildo Guillardi Júnior."
	@echo "Para citação use: 'GUILLARDI JR, H. Software para análise de consumo elétrico do patrimônio registrado da UNICAMP. Linguagem Python. Grupo de trabalho 05 para Uso Final da Energia do Campus Sustentável. 2020.'

vpn:
	@echo "Starting VPN under Campus Sustentável account."
	xterm -T "UNICAMP VPN connection" -geometry 55x4 -e "sudo openvpn --config VPN_client_ovpn --auth-user-pass VPN_user_pass" &

meta:
	@# Need https://github.com/PyCQA/pylint/issues/2763 fix to remove the `__init.py__` file.
	-cd Process/ && pyreverse -o png . && rm classes*.png -f && mv packages.png ..
	@#find . -name '*.py' | xargs wc -l
	find . -type f \( -name '*.py' -o -name '*.yaml' -o -name '*.yml' -o -name 'Makefile' -o -name '*.sh' -o -name '*.html' -o -name '*.css' -o -name '.env' -o -name '.gitignore' -o -name '*.md' -o -name '*.csv' -o -name '*.txt' -o -name '*.pot' -o -name '*.po' \) -print0 | xargs -0 wc -l

translate:
	# Generate the base translation file.
	xgettext -o Process/Languages/base.pot *.py -L Python --from-code=UTF-8
	# Merge with the already translated file.
	msgmerge -vU Process/Languages/en_US/LC_MESSAGES/messages.po Process/Languages/base.pot
	# Compile the present language catalogs.
	msgfmt Process/Languages/en_US/LC_MESSAGES/messages.po -o Process/Languages/en_US/LC_MESSAGES/messages.mo -v
	rm Process/Languages/en_US/LC_MESSAGES/messages.po~ -f
	codium Process/Languages/en_US/LC_MESSAGES/messages.po

# ---------------------------------------
# Macros to help the author development.

.PHONY: tests up_all up_software down_charts down_result down_results down_report down_reports
tests:
	python Process/general_definitions.py
	python Process/format_output.py
	# Test the copyright on images.
	mkdir public -p
	python Process/special_data.py
	python Process/copyright.py
	- python Process/copyright_opencv.py
	- python Process/copyright_pillow.py
	rm public/image*.*
	# Run all items module in test mode for validate them functions.
	python Process/items_*.py
	python Process/*_dig.py
	#find Process -name 'items_*.py' | xargs -t -n1 -I @ python "@"
	#find Process -name '*_dig.py' | xargs -t -n1 -I @ python "@"
up_all:
	scp -o "LogLevel=error" -rp -P 7531 * hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5
up_software:
	#scp -o "LogLevel=error" -rp -P 7531 *.* Resources/ Languages/ Test_data/ Makefile LICENSE .env hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5
	scp -o "LogLevel=error" -rp -P 7531 *.* Resources/ Languages/ Test_data/ Process/ LICENSE hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5
down_charts:
	scp -o "LogLevel=error" -rp -P 7531 hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5/Temporary/ .
down_result:
	mkdir public -p
	scp -o "LogLevel=error" -rp -P 7531 hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5/public/\{*.xlsx,*.log,Relatório.pdf\} public/
down_results:
	scp -o "LogLevel=error" -rp -P 7531 hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5/public/ .
down_report:
	scp -o "LogLevel=error" -rp -P 7531 hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5/public/Relatório.pdf public/
down_reports:
	scp -o "LogLevel=error" -rp -P 7531 hildogjr@labrei.dsce.fee.unicamp.br:CampusSustentável_GT5/public/*.pdf public/
