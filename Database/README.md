Dabase manipulationfor the University of Campinas inventory analysis focusing on electric equipment characteristics.


Author: Hildo Guillardi Júnior (GUILLARDI JR., 2020).

To undersand the software, check the [list of articles](../articles.txt) and the evolution between realeases in [`RELEASE.md`](../RELEASE.md) file.

# Licenses

1. All the code in this repository is written by Hildo Guillardi Júnior (GUILLARDI JR., HILDO), when/if released, are covered by GNU-GPL v3+ license, if you does not received a LICENSE file copy on download, please access https://www.gnu.org/licenses/gpl-3.0.en.html;

1. The databases used by this code are University of Campinas classified information and covered by the government Brazilian law number 12527 from 18/Nov./2011.

It expected the author citation/mention on future works: (GUILLARDI JR., 2020). The use of the data provide by this work must by followed by the saying:

- This paper contains patrimony data from UNICAMP processed by PhD. Hildo Guillardi Júnior.

- *Esse trabalho usa os dados de patrimônios da UNICAMP processados por Dr. Hildo Guillardi Júnior.*
