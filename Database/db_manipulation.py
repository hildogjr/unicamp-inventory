#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas classified information.
#
# Script for database manipulation: create, delete and load the
# equipment items data to the DB storage that will be used for
# the dashboard application.
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from typing import Callable, Any
import argparse

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd

# Internal libraries.
# from format_equip_data import get_equipments_db_file_list, get_equipment_db, merge_db_pages, COLUMN_UNIT_NAME


# %%
# Main function.

def main():
    args = argparse.ArgumentParser(description='Database connection and manipulation.')
    args_actions = args.add_mutually_exclusive_group()
    args_actions.add_argument('--delete', type=str, nargs='*', help='Delete the DB.')
    args_actions.add_argument('--clean', type=str, nargs='*', help='Clean the DB.')
    args_actions.add_argument('--create', type=str, nargs='*', help='Create the DB.')
    args_actions.add_argument('--load', type=str, nargs='*', help='Load the DB with all recognized equipment items data.')

    arguments = args.parse_args()
    print(arguments)


# %%
# Entry point.
if __name__ == '__main__':
    main()
