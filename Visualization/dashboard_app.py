#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
#
# Create a dashboard with the previous precessed data to
# users interact and filter their own information.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations
__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'

# Base libraries.
from datetime import datetime
from os import path
from fnmatch import fnmatch

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
import dash
from dash import html, dcc
from dash.dependencies import Input, Output, State
import plotly.express as px

# Application libraries.
from format_equip_data import (get_equipments_db_file_list, get_equipment_db, merge_db_pages, unify_airconditioners_systems,
                               COLUMN_UNIT_NAME)

print('=' * 55)
print('Estudo estatísticos de patrimônios da UNICAMP identificados como grandes consumidores de energia.')
print('Escrito por %s' % __author__)
date_now = datetime.now()
print('Criando web dashboard interativo em %s às %s' % (date_now.strftime(r'%d/%m/%Y'), date_now.strftime(r'%H:%M:%S')))
print('=' * 55)


# %%
# Read the generated equipment lists (filters applied by the other software steps).

# Find all files with the before items identification.
equipment_type = get_equipments_db_file_list()


# %%
# Define the dashboard layout.

app = dash.Dash(__name__)
# app = dash.Dash('Patrimônios elétricos da UNICAMP')
app.title = 'Patrimônios elétricos da UNICAMP'
# app._favicon = path.join(path.realpath(''), 'page_icon.png')

# Get the analytics track code.
try:
    with open(path.join('Resources', 'page_files', 'track_code.html'), 'r') as fh:
        track_code = children = fh.read()
except (FileExistsError, FileNotFoundError):
    track_code = ''

# <link rel = "stylesheet" href = "page_files/page.css">
# <link rel = "stylesheet" href = "page_files/dark-control.css">
# <link rel = "icon" href = "page_files/page_icon.png">
# <script src = "page_files/dark-control.js" defer > </script>
app.layout = html.Div([
    dcc.Store(id='store-equipment-name', storage_type='session'),
    dcc.Store(id='store-db-raw', storage_type='session'),
    dcc.Store(id='storoe-filters'),
    dcc.Store(id='store-db-filtered'),
    # html.Header(track_code),
    #     html.Header('''
    # <label class="container" title="Ativar modo noturno">
    #     <input type="checkbox" id="toggle-dark">
    #     <span class="checkmark"></span>
    # </label>
    # '''),
    html.H1('Patrimônios elétricos da UNICAMP'),
    html.Div(style={'width': '2%', 'display': 'inline-block'}),  # Place holder.

    html.Div([html.P('Categorias de patrimônios:'),
              dcc.RadioItems(id='equipment-type', labelStyle={'display': 'inline-block'},
                             options=[{'label': n, 'value': n} for n in equipment_type])
              ]),
    html.Div([html.Button('Remover filtros', id='remove-filters')]),
    html.Div([dcc.DatePickerSingle(id='date-min'),
              dcc.RangeSlider(id='date-year'),
              dcc.DatePickerSingle(id='date-max')
              ],
             style={'display': 'inline-block'}),

    dcc.Dropdown(id='unities-selector', multi=True, searchable=True, clearable=False),

    html.Div([
        dcc.Graph(id='unit-build'),
        dcc.Graph(id='purchase-date'),
    ], id='indicators-base', style={'display': 'inline-block', 'width': '55%'}),

    html.Div([
        dcc.Graph(id='subtypes'),
        dcc.Graph(id='acquisition-method'),
    ], id='indicators-classification', style={'display': 'inline-block', 'width': '55%'}),

    html.Div([
        dcc.Graph(id='refrigeration-power'),
        dcc.Graph(id='procel'),
    ], id='indicators-extra', style={'display': 'inline-block'}),
])


# %%
# Load data.

@app.callback(Output('equipment-type', 'value'), Input('equipment-type', 'options'))
def update_equipments_selector_value(values):
    '''Update the equipments selector values as the first one but prioritizing air conditioners.'''
    set_value = None
    for v in values:
        if 'ar condicionado' in v['value'].lower():
            set_value = v['value']
            break
    if set_value is None:
        set_value = values['value'][0]
    return set_value


@app.callback([Output('store-equipment-name', 'data'), Output('store-db-raw', 'data')],
              Input('equipment-type', 'value'),
              [State('store-equipment-name', 'data'), State('store-db-raw', 'data')])
def load_db(equipment_name: str, equipment_name_db: str, data):
    '''Load the specific database of equipments from my personal website.'''
    if equipment_name != equipment_name_db or data is None:  # Or keep the older data.
        print(f'Loading "{equipment_name}"...')
        # if equipment_name.lower() == 'ar condicionados':
        #    treatment_fnc = unify_airconditioners_systems
        # else:
        #    treatment_fnc = None
        treatment_fnc = None
        data = merge_db_pages(get_equipment_db(equipment_name), filter_fnc=treatment_fnc)
        # Insert a year column as just the year part of date.
        data.insert(0, value=data['Data de compra'].dt.year, column='year')
        print(f'Loaded {len(data)} items.')
        data = data.to_dict()
    return equipment_name, data


@app.callback(Output('unities-selector', 'options'),
              Input('store-db-raw', 'data'))
def update_unities_selector_list(data):
    '''Update the available unities list.'''
    if isinstance(data, dict):
        data = pd.DataFrame(data)
    # Add the macros as the initial vavlues in the list of the combocheckerbox.
    values = [{'label': '-> Selecionar todos', 'value': 'all_values'},
              {'label': '-> Limpar tudo', 'value': 'all_clean'},
              {'label': '-> Inverter seleção', 'value': 'all_inverted'}]
    # Add the unities names listed in the database.
    values += [{'label': v, 'value': v} for v in sorted(set(data[COLUMN_UNIT_NAME]))]
    return values


@app.callback(Output('unities-selector', 'value'),
              Input('unities-selector', 'options'))
def update_unities_selector_value(data):
    '''Update the available unities value on change of the available values.'''
    return 'all_values'


# @app.callback([Output('date-min', 'min_date_allowed'), Output('date-min', 'max_date_allowed'),
#               Output('date-min', 'min_date_allowed'), Output('date-min', 'max_date_allowed'),
#               Output('date-year', 'min_date_allowed'), Output('date-year', 'max_date_allowed')],
#               Input('store-db-raw', 'data'))
# def update_date_limites(data):
#     '''Update the data range components.'''
#     # Update one single component limit that will call the execution to update the three
#     # (minimum and maximum dcc.DataPickerSingle and the dcc.RangerSlider used to year)
#     # limits and actual value.
#     minimum = min(data['Data de compra'])
#     maximum = max(data['Data de compra'])
#     return minimum, maximum, minimum, maximum, minimum.dt.year, maximum.dt.year


# %%
# Components user experience expansion.

# @app.callback(Output('unities-selector', 'value'),
#               Input('unities-selector', 'value'),
#               State('store-equipment-name', 'data'))
# def update_unities(value, data):
#     '''Update the unities check-combobox with the logic for all/none/inverted.'''
#     units = list(set(data[COLUMN_UNIT_NAME]))
#     if isinstance(value, str):
#         value = [value]
#     if 'all_values' in value:
#         set_values = units
#     elif 'all_clean' in value:
#         set_values = []
#     elif 'all_inverted' in value:
#         set_values = units
#         value.remove('all_inverted')
#         for v in value:
#             set_values.remove(v)
#     else:
#         # Accept "*" for generic match.
#         set_values = []
#         for v in reversed(value):
#             if '*' in v:
#                 value.pop(v)
#                 set_values += [u for u in units if fnmatch(u, v)]
#         set_values += value
#         set_values = list(set(set_values))
#     return sorted(set_values)


# @app.callback([Output('date-min', 'date'), Output('date-max', 'date'), Output('date-year', 'value')],
#               [Input('date-min', 'date'), Input('date-max', 'date'), Input('date-year', 'value'),
#               Input('store-db-raw', 'data')])
# def update_date(value_min, value_max, value_year, data):
#     '''Update the years for filter.'''
#     if getattr(update_date, 'year', None) is None:
#         update_date.year = value_year
#     value_year = [int(value_year[0]), int(value_year[1])]
#     value_min = pd.to_datetime(value_min)
#     value_max = pd.to_datetime(value_max)

#     if update_date.year[0] != value_year[0]:
#         # The modification was in the range slider.
#         value_year[0] = max(int(min(data['Data de compra'].dt.year)), value_year[0])
#         value_min = pd.to_datetime(f'{value_year[0]}-{value_min.month}-{value_min.day}')
#     else:
#         # The modification was in the calendar picker date.
#         value_min = max(min(data['Data de compra']), value_min)
#         value_year[0] = value_min.year

#     if update_date.year[1] != value_year[1]:
#         value_year[1] = min(int(max(data['Data de compra'].dt.year)), value_year[1])
#         value_max = pd.to_datetime(f'{value_year[1]}-{value_min.month}-{value_min.day}')
#     else:
#         value_max = min(max(data['Data de compra']), value_max)
#         value_year[1] = value_max.year

#     # Mark the update the last modification.
#     update_date.year = value_year
#     return value_min, value_max, value_year


# @app.callback(Output('date-min', 'date'), Input('date-min', 'min_date_allowed'))
# def update_date_picker_min_value(value):
#     '''Update the data picker used for minimum date.'''
#     return value


# @app.callback(Output('date-max', 'date'), Input('date-max', 'max_date_allowed'))
# def update_date_picker_max_value(value):
#     '''Update the data picker used for maximum date.'''
#     return value


# @app.callback(Output('date-year', 'value'),
#               [Input('date-year', 'min'), Input('date-year', 'max')])
# def update_date_range_value(value_min, value_max):
#     '''Update the data slider used for years.'''
#     return [value_min, value_max]


# %%
# Filter execution logic.

# @app.callback([Output('store-db-raw', 'data'), Output('store-db-filtered', 'data'), Output('store-filters', 'data')],
#               [Input('button', 'n_clicks'), Input('store-db-raw', 'data')])
# def update_remove_filters(value, data):
#     '''Remove all the filters.'''
#     return data, {}, data


# %%
# Update charts logic.

# @app.callback(Output('unit-build', 'figure'), Input('store-db-filtered', 'data'))
# def update_bar_unity(data):
#     '''Update the unities bar chart.'''
#     filtered_df = data.groupby([COLUMN_UNIT_NAME]).size()
#     fig = px.bar(x=filtered_df.keys(), y=filtered_df.values,
#                  labels={'x': 'Unidade', 'y': 'Quantidade'},
#                  title='Quantidade por unidade')
#     fig.update_layout(transition_duration=500)
#     return fig


# @app.callback(Output('purchase-date', 'figure'), Input('store-db-filtered', 'data'))
# def update_bar_purchase_date(data):
#     '''Update the purchase date chart.'''
#     filtered_df = data.groupby(['year']).size()
#     fig = px.bar(x=filtered_df.keys(), y=filtered_df.values,
#                  labels={'x': 'Ano', 'y': 'Quantidade'},
#                  title='Quantidade por ano')
#     # filtered_df = data.groupby(['year']).count()['Aquisição']
#     # fig = px.bar(x=filtered_df.keys(), y=filtered_df.values,
#     #              labels={'x': 'Ano', 'y': 'Quantidade'}
#     #              title='Registro por ano e modo de aquisição')
#     fig.update_layout(transition_duration=500)
#     return fig


# @app.callback(Output('subtypes', 'figure'), Input('store-db-filtered', 'data'))
# def update_pie_subtypes(data):
#     '''Update the subtypes pie chart.'''
#     filtered_df = data.groupby(['Tipo']).size()
#     fig = px.pie(values=filtered_df.values, labels=filtered_df.keys(),
#                  title='Subtipo da categoria')
#     fig.update_traces(textposition='inside', textinfo='percent+label', hoverinfo='label+percent+name')
#     fig.update_layout(transition_duration=500)
#     return fig


# @app.callback(Output('acquisition-method', 'figure'), Input('store-db-filtered', 'data'))
# def update_pie_acquisition_method(data):
#     # Update the acquisition method pie chart.
#     filtered_df = data.groupby(['Aquisição']).size()
#     fig = px.pie(values=filtered_df.values, labels=filtered_df.keys(),
#                  title='Modo de aquisição da categoria')
#     fig.update_traces(textposition='inside', textinfo='percent+label', hoverinfo='label+percent+name')
#     fig.update_layout(transition_duration=500)
#     return fig


# %%
# Load the dashboard webpage service.
if __name__ == '__main__':
    app.run_server(debug=False)
    # Open it on http://127.0.0.1:8050
