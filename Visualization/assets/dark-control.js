/* Slide dark mode button control borrow from https://vidadeprogramador.com.br/ */

// Pega o checkbox
const botaoDark = document.getElementById('toggle-dark');

// Verifica se tem no localStorage seleção do dark theme
if (localStorage.getItem('data-theme') == 'dark') {
    botaoDark.checked = true;
}

// Liga a função ao checkbox
botaoDark.addEventListener('change', () => {
    let theme = localStorage.getItem('data-theme'); // Retrieve saved them from local storage
    if (!botaoDark.checked) {
        changeThemeToLight()
    } else {
        changeThemeToDark()
    }
});

// Segunda parte, removida do HTML principal.
const changeThemeToDark = () => {
    document.documentElement.setAttribute("data-theme", "dark");
    localStorage.setItem("data-theme", "dark");
}
const changeThemeToLight = () => {
    document.documentElement.setAttribute("data-theme", "light");
    localStorage.setItem("data-theme", 'light');
}
//let theme = localStorage.getItem('data-theme');
//if (theme == 'dark') changeThemeToDark();


// Load the default operational system preference by capturing the them of the window which opened the page.
const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: dark)');
if (prefersDarkScheme.matches) {

    changeThemeToDark();
    botaoDark.checked = true;
} else {
    changeThemeToLight();
    botaoDark.checked = false;
}