#!python
# This file is covered by GNU-GPL v3+ license, if you does not
# received a LICENSE file copy on download, please access
# https://www.gnu.org/licenses/gpl-3.0.en.html.
# The databases are University of Campinas classified information.
#
# Format definitions for read files and other inputs or equipment
# processed database into spreadsheet format. This file is shared
# between multiple projects.
#
# Written by Hildo Guillardi Júnior (GUILLARDI JR, H.).

from __future__ import annotations

__author__ = 'Hildo Guillardi Júnior (GUILLARDI JR, H.)'
__all__ = ['get_equipment_db', 'merge_db_pages',
           'get_equipments_db_file_list',
           'unify_airconditioners_systems', 'COLUMN_UNIT_NAME']

# Base libraries.
from typing import Callable, Any
from os import path
from glob import glob, glob1
import re

# Required but not default libraries.
try:
    import modin.pandas as pd
except ImportError:
    import pandas as pd
import numpy as np

# Internal libraries.
try:
    # Try import from general definition (only works on main project).
    from general_definitions import (COLUMN_UNIT_NAME, SPREADSHEET_PAGE_INFO_NAME, FILE_NAME_DB, _)
except ImportError:
    try:
        from ..Process.general_definitions import (COLUMN_UNIT_NAME, SPREADSHEET_PAGE_INFO_NAME, FILE_NAME_DB, _)
    except ImportError:
        COLUMN_UNIT_NAME = 'Área de Patrimônio'
        SPREADSHEET_PAGE_INFO_NAME = 'Informações gerais'
        FILE_NAME_DB = 'AGI*.*'  # Filter for general DB file of the university.

        def _(x):
            # Bypass the translation function.
            return x

WEB_PAGE = 'https://hildogjr.gitlab.io/unicamp-inventory/'  # My webpage as data repository.


# %%
# Functions to deal with the input data.

def get_equipments_db_file_list(force_webpage: bool = False) -> list[str]:
    '''Get the list of equipment databases available
    on the computer or in the repository webpage.

    Parameters: Force get from webpage.
    '''
    from fnmatch import fnmatch

    def remove_not_valid(db_list: list[str]) -> list[str]:
        '''Remove the not valid names.'''
        for idx in reversed(range(len(db_list))):
            if fnmatch(db_list[idx], FILE_NAME_DB) or fnmatch(db_list[idx].lower(), '*análise*'):
                del db_list[idx]
        return db_list

    if not force_webpage:
        # Try to get the information from the default path on my computer.
        db_list = []
        db_list += glob('*.xlsx')
        db_list += glob('*.xls')
        db_list += glob('*.ods')
        relative_path = path.join('..', 'Uso final da energia [GT5]', 'public')
        # Despite `get_equipment_db(foo)`, it doesn't include the path on the list.
        db_list += glob1(relative_path, '*.xlsx')
        db_list += glob1(relative_path, '*.xls')
        db_list += glob1(relative_path, '*.ods')
        db_list = remove_not_valid(db_list)

    # If it didn't get any information from computer repository,
    # force get from the web page.
    db_list = []
    if not db_list:
        import urllib.parse
        import urllib.request

        # Get the possible files that match with.
        with urllib.request.urlopen(WEB_PAGE) as page:
            if page.getcode() != 200:
                page.close()
                raise FileExistsError(f'"{WEB_PAGE}" not found.')
            html = page.read().decode('utf8')
            db_list = re.findall(r'[\'"](.*\.(xlsx*|ods))[\'"]', html)
            if db_list:
                db_list = [n[0] for n in db_list]
            db_list = remove_not_valid(db_list)

    return [path.splitext(n)[0] for n in db_list]


def get_equipment_db(file_pattern: str) -> pd.DataFrame:
    '''Get the air conditioner data.

    Parameter: the file name or pattern to search into the default
    paths / the repository website.

    Raise a `FileExistsError` if file or webpage not found.
    '''
    if not path.splitext(file_pattern)[-1]:
        file_pattern += '.*'

    # Try to get the information from the default path on my computer.
    possible_db_file = []
    possible_db_file += glob(file_pattern)

    def get_from_subfoflder(relative_path: str) -> list[str]:
        return [path.join(relative_path, f) for f in glob1(relative_path, file_pattern)]

    possible_db_file += get_from_subfoflder(path.join('..', 'Uso final da energia [GT5]', 'public'))
    possible_db_file += get_from_subfoflder('public')

    db = pd.DataFrame()
    for f in reversed(possible_db_file):
        # Read the first valid file.
        db, _ = read_equipment_database(f)
        return db

    # If not found the file, try to download it from my webpage.
    if len(db) == 0:
        import urllib.parse
        import urllib.request
        from fnmatch import translate as pattern_to_re

        # Get the possible files that match with.
        with urllib.request.urlopen(WEB_PAGE) as page:
            if page.getcode() != 200:
                page.close()
                raise FileExistsError(f'"{WEB_PAGE}" not found.')
            html = page.read().decode('utf8')
            file_pattern = pattern_to_re(file_pattern)[4:-3]
            file_name = re.findall('[\'"](%s)[\'"]' % file_pattern, html)[0]
            if not file_name:
                raise ValueError(f'File pattern "{file_pattern}" not found at {WEB_PAGE}.')

        # link = urllib.parse.urljoin(WEB_PAGE, 'public/')
        link = urllib.parse.urljoin(WEB_PAGE, urllib.parse.quote(file_name))

        # Downlaod the specific file.
        with urllib.request.urlopen(link) as page:
            if page.getcode() != 200:
                page.close()
                raise FileExistsError(f'"{WEB_PAGE}" not found.')
            with open(file_name, 'wb') as fh:
                fh.write(page.read())
            db, _ = read_equipment_database(file_name)
        return db

    # Not file found or could be downloaded.
    raise FileExistsError('No ar conditioner database file found.')


def read_equipment_database(file_name: str) -> tuple[pd.DataFrame | dict[pd.DataFrame], dict[str, Any]]:
    '''Read a equipment database generated by the step of identification.
    '''
    database = pd.read_excel(file_name, sheet_name=None)
    database_meta = dict()
    # If not defined the `sheet_name` nas `None` it will read just
    # the first one. This is interesting for speed-up the debug.
    # database = pd.read_excel(db_file)

    # Delete additional sheet pages created by file readers (not by the
    # `identify_item.py` script). By the importation, all the unit/school
    # names (valid identified data) are uppercase. If a correct sheet was
    # imported, set the "Identificador" as index.
    # Sort the units to better appear on `file_name_analysis` spreadsheet
    # file rows. This is not mandatory, the `generate_report.py` will sort
    # the report section and possible the units came already sorted by
    # `identify_items.py` execution.
    page_name: str
    for page_name in list(database.keys()):

        # Use the bellow bypass to run the code with less
        # databases. This is used for better quick debug.
        # if page_name not in ['AEPLAN', 'CIPA', ]:
        #    del database[page_name]
        #    continue

        if 'análise' in page_name.lower():
            del database[page_name]
        elif page_name.lower() == SPREADSHEET_PAGE_INFO_NAME.lower():
            temp = database[page_name].transpose().to_dict()
            del database[page_name]

            # Interpreter the forced conversion.
            row_value_keys = list(temp.values())
            row_idx = 0
            while row_idx < len(row_value_keys):

                def get_row_key(level_count: int = 0, row_count: int = row_idx):
                    # Safety return.
                    if row_count >= len(row_value_keys):
                        return np.nan

                    row_value = row_value_keys[row_count]
                    info_name = row_value[list(row_value.keys())[level_count]]
                    if isinstance(info_name, str):
                        info_name = info_name.strip()
                        if info_name[-1] == ':':
                            info_name = info_name[:-1].strip()
                    return info_name

                def extract_row_data(level_count: int = 0):
                    nonlocal row_idx
                    # Safety return.
                    if row_idx >= len(row_value_keys):
                        return np.nan, np.nan

                    # Get the base key and value to test.
                    # The key (fist valid columns) is get by special `get_row_key`
                    # function. The value is replace by `np.nan` if the right end
                    # of the DB is reached.
                    row_value = row_value_keys[row_idx]
                    info_name = get_row_key(level_count, row_idx)
                    info_value = (row_value[list(row_value.keys())[level_count + 1]]
                                  if level_count + 1 < len(row_value) else np.nan)

                    # Validate logic and iteration.
                    if isinstance(info_value, str):
                        info_value = info_value.strip()
                    elif info_value is np.nan:
                        if info_name is np.nan and level_count == 0:
                            return np.nan, np.nan  # No data in this line.

                        # This is used for recursive extract lists.
                        info_value: list = []
                        while True:
                            # Break the loop if the next line is empty (check at least
                            # one column more tha used on above read) or if if found
                            # a key or minor level (major priority).
                            if (all(get_row_key(column_idx, row_idx + 1) is np.nan for column_idx in range(level_count + 2))
                                    or get_row_key(level_count, row_idx + 1) is not np.nan):
                                if level_count > 0:
                                    return info_name, np.nan
                                else:
                                    # In the root of the graph, just break the iterations.
                                    break

                            row_idx += 1  # Go next row on the main graph.
                            info_name_, info_value_ = extract_row_data(level_count + 1)
                            if info_value_ is not np.nan:
                                info_value.append({info_name_: info_value_})
                            else:
                                info_value.append(info_name_)

                        # Return from iteration.
                        if len(info_value) == 1:
                            info_value = info_value[0]
                        info_value = {info_name: info_value}
                    return info_name, info_value

                info_name, info_value = extract_row_data()
                if info_name is not np.nan and info_value is not np.nan:
                    database_meta.update({info_name: info_value})
                row_idx += 1  # Go next row on the main graph.

        else:
            # The index is not used on the code but it keep in this
            # way for better organization and desambiguization.
            database[page_name].set_index('Identificador')

            def convert_check_date(database: pd.Series, date_column_name: str) -> list[pd.DatetimeTZDtype]:
                # Convert the date to datetime object.
                # NOTE: Check if it is a valid data replacing the invalid
                # by `NaT`, it happened that found a invalid (possible
                # type error) that cause a invalid/to old data.
                new_dates = pd.to_datetime(database[date_column_name], errors='coerce', format='%d/%m/%Y')
                new_dates_NaT = new_dates.isnull()
                if any(new_dates_NaT):
                    for _, row in database[new_dates_NaT].iterrows():
                        if not pd.isnull(row[date_column_name]):
                            print('\t%s (%s) não reconhecida para %i em %s.' %
                                  (date_column_name, row[date_column_name], row['Identificador'], page_name))
                return new_dates

            def convert_to_int(x: str) -> int | str:
                try:
                    return int(x.replace('.', ''))
                except Exception:
                    return x

            def remove_spaces(x: str) -> str:
                try:
                    return x.replace(' ', '')
                except Exception:
                    return x

            # Example at 12/02/1015 the item 677662 of FEM (code index 173).
            database[page_name]['Data Situação'] = convert_check_date(database[page_name], 'Data Situação')
            database[page_name]['Data de compra'] = convert_check_date(database[page_name], 'Data de compra')
            database[page_name]['Imóvel'] = database[page_name]['Imóvel'].apply(convert_to_int)
            database[page_name]['Órgão'] = database[page_name]['Órgão'].apply(remove_spaces)

    return database, database_meta


def merge_db_pages(db: dict[str, pd.DataFrame],
                   column_origin_name: str = COLUMN_UNIT_NAME,
                   filter_fnc: Callable[[dict[str, pd.DataFrame]], pd.DataFrame] | None = None) -> pd.DataFrame:
    '''Create the complete database merging all pages (dictionary entries).

    Parameters:
        Dictionary of pages with DataFrame.
        Some specific function to execute on the data of each page before merge it.

    Return a merged DataFrame with a additional column to indicate the page origin.
    '''
    if column_origin_name in db.keys():
        raise KeyError(f'"{column_origin_name}" column name already exists on the database.')

    print('Merging pages: ', end='')
    grouped_database = pd.DataFrame()
    for page_name, data in db.items():
        print(page_name, end=' ')
        if filter_fnc:
            data = filter_fnc(data)
        data.insert(0, value=page_name, column=column_origin_name)
        # Equivalente result of above but keep warning because of memory implementation.
        # data[column_origin_name] = page_name
        # Append/create the `grouped_database` for bellow analysis.
        if grouped_database.empty:
            grouped_database = data
        else:
            # grouped_database = grouped_database.append(data)
            grouped_database = pd.concat([data, grouped_database], axis=0, ignore_index=True)
    print('')
    return grouped_database


def unify_airconditioners_systems(data: pd.DataFrame) -> pd.DataFrame:
    '''Sometimes the split air conditioners equipments appear on the inventory
    as two different parts, "evaporator"+"condenser" or "condenser"+"split" unit,
    sometimes just as "split".  Those should be count as an unit equipment system.
    Usually the "condenser" are poorly described and doesn't have "Selo PROCEL"
    or refrigeration power in the description text.
    Considering those two different equipment listed items as independent could
    propagate erros to the mean and missing values analysis of the refrigeration
    power of each institute/unit, preventing a reliable purchase between them.

    This function remove the "condenser" data of this duplicated registry case.

    Parameters and returns: the original pd.DataFrame and it unified.
    '''

    def clean_itens(data: pd.DataFrame) -> pd.DataFrame:
        '''Function logic to unify the air conditioners systems by removing
        itens that compose subparts of a big system (and the information to
        to be analyzed are already in the first).
        '''
        kind_equipments = set(data['Tipo'])

        def have_item(kind: str) -> bool:
            '''Check if have some item of the category.'''
            return any(kind in k for k in kind_equipments)

        # Check if is present "condensador" and "evaporador" at same time to remove the
        # "condensador", usually the BTU/h and other information were registered on the
        # "evaporador" part.  Do the check under loop form to check by the main type
        # and not sub type (inverter word, for example).
        if have_item(_('split')) or (have_item(_('condensador')) and have_item(_('evaporador'))):
            data = data[~data['Tipo'].str.contains(_('condensador'))]
        # Do same check for the pair "chiller" - "fancoil".
        if have_item(_('chiller')) and have_item(_('fancoil')):
            data = data[~data['Tipo'].str.contains(_('fancoil'))]
        return data

    units = set(data.get(COLUMN_UNIT_NAME, []))
    if len(units) <= 1:
        # When there is only one unit (empty is assumed as unique unit).
        data_total = clean_itens(data)
    else:
        # When presented a DataFrame of more than one unit (grouped units
        # DataFrame), use the logic of unification by each unit. It is deal
        # in this way because the split items part, as explained in  the
        # title, occur when a person(s) of one unit fill the inventory in
        # that way (the itens have not correlation between units).
        data_total = pd.DataFrame()
        for unit_name in units:
            data_unit_clean = clean_itens(data[data[COLUMN_UNIT_NAME] == unit_name])
            data_total = pd.concat([data_total, data_unit_clean], axis=0, ignore_index=True)

    return data_total


# %%
# Main entry, used just for debug.
if __name__ == "__main__":
    print('List of equipments DB:', get_equipments_db_file_list())

    print('Reading "air conditioners"...')
    db = merge_db_pages(get_equipment_db('Ar condicionados.*'), filter_fnc=unify_airconditioners_systems)
    print(db)
