This folder contains all the test cases for the identification modules of this sofware.

The files have to be named as `test_*.csv` for the affirmative match items case and `test_not_*.csv` for the negative match items case of the same module `*`.

The not match (`test_not_*.csv`) cases MUST have the columns:
- Área de Patrimônio
- Identificador
- Descrição

A insteresting additional column is "Match" that is used the internal software match, not useful for final users of the identified data but useful for software bug track.

The do match (`test_*.csv`) cases CAN have the columns above and addition of:
- Match
- Marca
- Modelo
- Data de compra
- Tipo

Until now "Tipo" are amost preset for all module of identification.

Additional columns can be added depending of the identified characteristics of each identification module: "Selo PROCEL", "Potência frigorífica (BTU/h)", "Potência elétrica (VA)", "Potência elétrica (W)" ...

The identification module (for replace `*` in the file names above) installed are:
- airconditioners
- computers
- fridges
- routers
- ups
