Do the University of Campinas inventory analysis focusing on electric equipment characteristics with constant electrical consumption releasing to the e-mail group and update a web page.

Author: [Hildo Guillardi Júnior (GUILLARDI JR., 2020)](http://lattes.cnpq.br/2763526214348012).

If this open software or process data were useful to your work/development, consider to buy me a coffee.

<a href="https://www.buymeacoffee.com/hildogjr" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" ></a>

# Published results

Webpage with results of last execution: https://hildogjr.gitlab.io/unicamp-inventory/

Article(s):

- [Guillardi Júnior, H. (2024). Analysis of Electrical Equipment at UNICAMP: Insights from the Inventory Database. In: Jørgensen, B.N., da Silva, L.C.P., Ma, Z. (eds) Energy Informatics. EI.A 2023. Lecture Notes in Computer Science, vol 14468. Springer, Cham. https://doi.org/10.1007/978-3-031-48652-4_14](https://doi.org/10.1007/978-3-031-48652-4_14)


# Processing steps

The process is split on the bellow steps:

1. [Extraction of the database](Process/download_data.py) -> `make get_data`;

1. [Identification of the items](Process/identify_items.py) -> `make identify`;

1. [Performance of the analysis](Process/performe_analysis.py) -> `make analyze`;

1. [Generation of a PDF report](Process/generate_report.py) using the above results `make report`;

1. Publishing the results by [e-mail](Process/send_email.py) and [page creation](Process/create_page.py) -> `make email` and `make site`.

To understand the software, check the [list of articles](articles.txt) and the evolution between releases in [`RELEASE.md`](RELEASE.md) file.


# Licenses

1. All the code in this repository is written by Hildo Guillardi Júnior (GUILLARDI JR., HILDO), when/if released, are covered by GNU-GPL v3+ license, if you does not received a LICENSE file copy on download, please access https://www.gnu.org/licenses/gpl-3.0.en.html;

1. The databases used by this code are University of Campinas classified information and covered by the government Brazilian law number 12527 from 18/Nov./2011.

It expected the author citation/mention on future works: (GUILLARDI JR., 2020). The use of the data provide by this work must by followed by the saying:

- This paper contains patrimony data from UNICAMP processed by PhD. Hildo Guillardi Júnior's software and available at https://hildogjr.gitlab.io/unicamp-inventory/.

- *Esse trabalho usa os dados de patrimônios da UNICAMP processados por software desenvolvido por Dr. Hildo Guillardi Júnior e disponível em https://hildogjr.gitlab.io/unicamp-inventory/.*
